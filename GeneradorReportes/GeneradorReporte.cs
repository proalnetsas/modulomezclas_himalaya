﻿using SipModuloManualModelo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneradorReportes
{
    public partial class GeneradorReporte : Form
    {
        private ContextEntities db = new ContextEntities();
        private CrearArchivo crea = new CrearArchivo();

        public GeneradorReporte()
        {
            InitializeComponent();
        }

        void generarReportes()
        {
            List<GENERADORREPORTES> gene = db.GENERADORREPORTES.Where(c => c.GENERADO == false).ToList();

            

            foreach (GENERADORREPORTES gen in gene)
            {
                try
                { 
                    if (gen.REPORTE == "PPR027")
                    {
                    
                        crea.GenerarPPR027(gen.RUTA);
                        gen.GENERADO = true;
                        db.SaveChanges();
                    }

                    if (gen.REPORTE == "TOCO1")
                    {
                        crea.GenerarToc01(gen.RUTA, gen.PARAMETRO);
                        gen.GENERADO = true;
                        db.SaveChanges();
                    }

                    if (gen.REPORTE == "TOC02")
                    {
                        crea.GenerarToc02(gen.RUTA, Convert.ToInt32(gen.PARAMETRO));
                        gen.GENERADO = true;
                        db.SaveChanges();
                    }

                    if (gen.REPORTE == "PPR019")
                    {
                        crea.GenerarPpr019(gen.RUTA, Convert.ToInt32(gen.PARAMETRO));
                        gen.GENERADO = true;
                        db.SaveChanges();
                    }

                    if (gen.REPORTE == "PPR01")
                    {
                        crea.GenerarPPr01(gen.RUTA, Convert.ToInt32( gen.PARAMETRO));
                        gen.GENERADO = true;
                        db.SaveChanges();
                    }
                }
                catch(Exception ex)
                {

                }
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

        }

        object updateLock = new object();

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            
                if (Monitor.TryEnter(updateLock))
                {
                    try
                    {
                        generarReportes();
                    }
                    finally
                    {
                        Monitor.Exit(updateLock);
                    }
                }
                else
                {
                    // previous timer tick took too long.
                    // so do nothing this time through.
                }
         }
    }
}

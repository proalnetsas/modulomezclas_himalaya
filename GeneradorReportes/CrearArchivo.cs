﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SipModuloManualModelo.Model;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GeneradorReportes
{
    public  class CrearArchivo
    {

        private ContextEntities db = new ContextEntities();

        public void GenerarPPR027(string rutaarchivo)
        {

            int col = 1;

            var a = db.FORMATOPPR.AsQueryable().OrderBy(c => c.CODIGOORDENPRODUCCION).Select(c => new
            {
                c.CODIGOORDENPRODUCCION,
                c.PRODUCTOS.NOMBREPRODUCTO,
                c.ANALISIS,
                c.MATERIALES.NOMBREMATERIAL,
                c.LOTEMATERIAL,
                c.CODIGOPREMEZCLA,
                APROBADO = c.APROBADO == true ? "SI" : "NO",
                IMPRESO = c.IMPRESO == true ? "SI" : "NO",
                OPERARIOENTREGA = c.USUARIOS.PERSONAL.NOMBRES + " " + c.USUARIOS.PERSONAL.APELLIDOS,
                OPERARIORECIBE = c.USUARIOS1.PERSONAL.NOMBRES + " " + c.USUARIOS1.PERSONAL.APELLIDOS,
                c.FECHAENTREGA,
                c.FECHARECIBE,
                c.FECHAPESAJE,
                TARA = c.TARA + " KG",
                PESOTOTAL = c.PESOTOTAL + " KG",
                PESOBRUTO = c.PESOBRUTO + " KG",
                c.OBSERVACIONES,

            }).ToList();

            //Creamos el objecto excel
            Microsoft.Office.Interop.Excel.Application aplicacion;
            Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
            Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;


            aplicacion = new Microsoft.Office.Interop.Excel.Application();


            libros_trabajo = aplicacion.Workbooks.Add();
            hoja_trabajo =
            (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

            //COLUMNAS DEL EMCABEZADO
            hoja_trabajo.Cells[1, 1] = "AZUL K S.A.";
            hoja_trabajo.Cells[2, 1] = "PRODUCCION";
            hoja_trabajo.Cells[3, 1] = "PP-R-027";
            hoja_trabajo.Cells[4, 1] = "OBJETIVO: Identificar la materia prima y el material de empaque para evitar confusiones y alteraciones en el proceso";
            hoja_trabajo.Cells[6, 1] = "Area: Tocador III";

            hoja_trabajo.Cells[1, 2] = "IDENTIFICACION DE MATERIA PRIMA Y MATERIAL DE EMPAQUE";

            hoja_trabajo.Cells[1, 3] = "Fecha Emision: 15-09-16";
            hoja_trabajo.Cells[2, 3] = "Version: 06";
            hoja_trabajo.Cells[3, 3] = "Pagina 1/1";


            //escribimos en la hoja de excel los nombres de los encabezados
            foreach (var column in a)
            {
                foreach (var prop in column.GetType().GetProperties())
                {
                    hoja_trabajo.Cells[6, col] = prop.Name;
                    hoja_trabajo.Cells[6, col].font.color = Color.White;//Microsoft.Office.Interop.Excel.XlRgbColor.rgbWhite;
                    hoja_trabajo.Cells[6, col].interior.color = Color.Blue;//Microsoft.Office.Interop.Excel.XlRgbColor.rgbSkyBlue;
                    col += 1;
                }

                break;
            }


            //Obtiene las propiedades del primer item de la lista
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties((a.First()));



            ////Recorremos el DataGridView rellenando la hoja de trabajo
            for (int i = 0; i < a.Count; i++)
                for (int j = 0; j < properties.Count; j++)
                    hoja_trabajo.Cells[i + 7, j + 1].Value = properties[j].GetValue(a[i]);



            //combino y centro para el emcabezado
            hoja_trabajo.get_Range("B1", "B3").Merge();
            hoja_trabajo.get_Range("A4", "D5").Merge();

            hoja_trabajo.get_Range("B1", "B3").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            hoja_trabajo.get_Range("B1", "B3").Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;


            //Combinamos celdas para mostrar el texto largo
            var r = hoja_trabajo.get_Range("A4", "D5");
            r.Select();
            r.WrapText = true;
            r.Style.Font.Bold = true;
            r.Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            r.Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

            ////Tamaño automatico de las columnas
            hoja_trabajo.Columns.AutoFit();

            
            libros_trabajo.SaveAs(rutaarchivo,
                    Microsoft.Office.Interop.Excel.XlFileFormat.xlAddIn);
            libros_trabajo.Close();
            //libros_trabajo.Close(true);
            aplicacion.Quit();

        }


        public void GenerarToc01(string rutaarchivo, string  parametro)
        {
            try
            {
                int col = 1;

                FORMATOTOC formato = db.FORMATOTOC.Where(c => c.CODIGOORDENPRODUCCION == parametro).FirstOrDefault();


                DataTable dt = new DataTable();

                SqlCommand cmd = new SqlCommand("SP_DynamicReportTOCExcel", (SqlConnection)db.Database.Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CODIGOPRODUCTOS ", formato.CODIGOPRODUCTOS);
                cmd.Parameters.AddWithValue("@CODIGOORDEN ", formato.CODIGOORDENPRODUCCION);
                cmd.Parameters.AddWithValue("@CODIGOPUESTO ", formato.CODIGOPUESTO);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                Microsoft.Office.Interop.Excel.Application aplicacion;
                Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
                Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;

                aplicacion = new Microsoft.Office.Interop.Excel.Application();

                libros_trabajo = aplicacion.Workbooks.Add();
                hoja_trabajo =
                    (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

                //NOMBRE DEL ENCABEZADO
                hoja_trabajo.Cells[1, 1] = "AZUL K S.A. ";
                hoja_trabajo.Cells[2, 1] = "PRODUCCION";
                hoja_trabajo.Cells[3, 1] = "PP-TOC-R-001";
                hoja_trabajo.Cells[4, 1] = "OBJETIVO: Controlar y garantizar las cantidades de materia prima con un tiempo preestablecido para fabricar productos de Tocador bajo estandares de calidad";
                hoja_trabajo.Cells[6, 1] = "Area: Tocador III";

                hoja_trabajo.Cells[1, 2] = "CONTROL DE PRODUCTO ETAPA DE FABRICACION";


                hoja_trabajo.Cells[1, 4] = "Fecha Emision: 16-05-18";
                hoja_trabajo.Cells[2, 4] = "Version: 08";
                hoja_trabajo.Cells[3, 4] = "Pagina 1/1";


                hoja_trabajo.Cells[1, 6] = "FECHA: " + formato.FECHA.ToShortDateString();
                hoja_trabajo.Cells[2, 6] = "CODIGO ORDEN: " + formato.CODIGOORDENPRODUCCION;
                hoja_trabajo.Cells[3, 6] = "PRODUCTO: " + formato.PRODUCTOS.NOMBREPRODUCTO;
                //hoja_trabajo.Cells[4, 6] = "TURNO :" + formato.CODIGOTURNO;
                //hoja_trabajo.Cells[5, 6] = "OPERARIO: " + formato.USUARIOS.PERSONAL.NOMBRES + ' ' + formato.USUARIOS.PERSONAL.APELLIDOS;




                foreach (DataColumn column in dt.Columns)
                {
                    hoja_trabajo.Cells[6, col] = column.ColumnName.ToString();
                    hoja_trabajo.Cells[6, col].Font.Color = Color.White;
                    hoja_trabajo.Cells[6, col].Interior.Color = Color.Blue;
                    col += 1;
                }


                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j + 1 > 10)
                        {
                            hoja_trabajo.Cells[i + 7, j + 1] = dt.Rows[i][j].ToString() + " G";
                        }
                        else
                            hoja_trabajo.Cells[i + 7, j + 1] = dt.Rows[i][j].ToString();
                    }
                }

                //combino y centro para el emcabezado
                hoja_trabajo.get_Range("B1", "B3").Merge();
                hoja_trabajo.get_Range("A4", "D5").Merge();
                hoja_trabajo.get_Range("B1", "B3").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                hoja_trabajo.get_Range("B1", "B3").Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //ponemos formatos de negrita y centramos celdas para mostrar el texto largo
                Microsoft.Office.Interop.Excel.Range r = hoja_trabajo.get_Range("A4", "D5");
                r.Select();
                r.Style.Font.Bold = true;
                r.WrapText = true;
                r.Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                r.Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;


                //Tamaño automatico de las columnas
                hoja_trabajo.Columns.AutoFit();


                //string rutaarchivo = "D:\\" + nombrearchivo;




                libros_trabajo.SaveAs(rutaarchivo,
                        Microsoft.Office.Interop.Excel.XlFileFormat.xlAddIn);
                //libros_trabajo.Close(true);
                aplicacion.Quit();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GenerarToc02(string rutaarchivo, int parametro)
        {
            int col = 1;

            FORMATOTOC02 control = db.FORMATOTOC02.Where(c => c.ID == (parametro)).FirstOrDefault();

            if (control != null) { 
                var a = db.FORMATOTOC02DETALLE.Where(c => c.IDTOC == (parametro)).OrderBy(c => c.ESPECIFICACION).Select(c => new {
                    c.CONDICION,
                    c.ESPECIFICACION,
                    c.IEP,
                    c.FORMATOTOC02.FECHA,
                    OPERARIO= c.FORMATOTOC02.USUARIOS.PERSONAL.NOMBRES + " "+ c.FORMATOTOC02.USUARIOS.PERSONAL.APELLIDOS,
                    c.FORMATOTOC02.CODIGOTURNO,
                    c.HORA1,
                    c.HORA2,
                    c.HORA3,
                    c.HORA4,
                    c.HORA5,
                    c.HORA6,
                    c.HORA7,
                    c.HORA8,
                    c.OBSERVACIONES
                }).Distinct().ToList();



                //Creamos el objecto excel
                Microsoft.Office.Interop.Excel.Application aplicacion;
                Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
                Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;

                //Creamos una nueva instancia de la hoja de excel, libro de trabajo y  hoja de trabajo
                aplicacion = new Microsoft.Office.Interop.Excel.Application();
                libros_trabajo = aplicacion.Workbooks.Add();
                hoja_trabajo =
                    (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

                //COLUMNAS DEL EMCABEZADO
                hoja_trabajo.Cells[1, 1] = "AZUL K S.A.";
                hoja_trabajo.Cells[2, 1] = "PRODUCCION";
                hoja_trabajo.Cells[3, 1] = "PP-TOC-R-002";
                hoja_trabajo.Cells[4, 1] = "OBJETIVO: Controlar las variables de operacion en la fabricacion de los jabones de tocador, garantizando productos bajo estandares de calidad";
                hoja_trabajo.Cells[6, 1] = "Area: Tocador III";

                hoja_trabajo.Cells[1, 2] = "CONTROL PROCESO TOCADOR II, III Y IV";

                hoja_trabajo.Cells[1, 3] = "Fecha Emision: 16-09-05";
                hoja_trabajo.Cells[2, 3] = "Version: 13";
                hoja_trabajo.Cells[3, 3] = "Pagina 1/1";



                hoja_trabajo.Cells[1, 5] = "Fecha: " + control.FECHA;
                hoja_trabajo.Cells[2, 5] = "Producto: " + control.PRODUCTOS.NOMBREPRODUCTO;
                hoja_trabajo.Cells[3, 5] = "Lote: " + control.LOTE;
                hoja_trabajo.Cells[4, 5] = "Pastas Rechazadas Ini: " + control.PASTARECHAZADAINICIO;
                hoja_trabajo.Cells[5, 5] = "Pastas Rechazadas Fin: " + control.PASTARECHAZADAFIN;
                hoja_trabajo.Cells[6, 5] = "% Rechazado: " + control.PORCENTAJERECHAZADO;


                //Escribimos en la hoja de excel los nombres de los encabezados
                foreach (var column in a)
                {
                    foreach (var prop in column.GetType().GetProperties())
                    {
                        hoja_trabajo.Cells[7, col] = prop.Name;
                        hoja_trabajo.Cells[7, col].Font.Color = Color.White;
                        hoja_trabajo.Cells[7, col].Interior.Color = Color.Blue;
                        col += 1;
                    }

                    break;
                }


                //Obtiene las propiedades del primer item de la lista
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties((a.First()));



                //Recorremos el DataGridView rellenando la hoja de trabajo
                for (int i = 0; i < a.Count; i++)
                    for (int j = 0; j < properties.Count; j++)
                        hoja_trabajo.Cells[i + 8, j + 1].Value = properties[j].GetValue(a[i]);



                //combino y centro para el emcabezado
                hoja_trabajo.get_Range("B1", "B3").Merge();
                hoja_trabajo.get_Range("A4", "D5").Merge();

                hoja_trabajo.get_Range("B1", "B3").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                hoja_trabajo.get_Range("B1", "B3").Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;


                //Combinamos celdas para mostrar el texto largo
                var r = hoja_trabajo.get_Range("A4", "D5");
                r.Select();
                r.WrapText = true;
                r.Style.Font.Bold = true;
                r.Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                r.Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //Tamaño automatico de las columnas
                hoja_trabajo.Columns.AutoFit();


          

                //libros_trabajo.SaveAs("C:\\Program Files (x86)\\PROALNET\\Cavidi\\Content\\Reportes\\FormatoToc.xls",
                libros_trabajo.SaveAs(rutaarchivo,
                        Microsoft.Office.Interop.Excel.XlFileFormat.xlAddIn);
                libros_trabajo.Close();
                //libros_trabajo.Close(true);
                
                aplicacion.Quit();

            }
        }


        public void GenerarPpr019(string rutaarchivo, int parametro)
        {
            int col = 1;

            FORMATOPPRCONTROL control = db.FORMATOPPRCONTROL.Where(c => c.ID == (parametro)).FirstOrDefault();

            //Sacamos la lista a exportar de excel
            var a = db.FORMATOPPRDESPEJELINEA.Where(c => c.IDFORMATOPPRCONTROL == (parametro)).OrderBy(c => c.CODIGOPREGUNTA).Select(c => new {
                c.CODIGOPREGUNTA,
                c.PREGUNTA,
                APLICA = c.APLICA == true ? "SI" : "NO",
                RESPUESTA = c.RESPUESTA == true ? "SI" : "NO",
                c.OBSERVACIONES, 
                NOMBRE_OPERARIO = control.USUARIOS.PERSONAL.NOMBRES + " " + control.USUARIOS.PERSONAL.APELLIDOS,
                TURNO= control.CODIGOTURNO
            }).Distinct().ToList();


            //Creamos el objecto excel
            Microsoft.Office.Interop.Excel.Application aplicacion;
            Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
            Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;

            //Creamos una nueva instancia de la hoja de excel, libro de trabajo y  hoja de trabajo
            aplicacion = new Microsoft.Office.Interop.Excel.Application();
            libros_trabajo = aplicacion.Workbooks.Add();
            hoja_trabajo =
                (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

            //COLUMNAS DEL EMCABEZADO
            hoja_trabajo.Cells[1, 1] = "AZUL K S.A.";
            hoja_trabajo.Cells[2, 1] = "PRODUCCION";
            hoja_trabajo.Cells[3, 1] = "PP-R-019";
            hoja_trabajo.Cells[4, 1] = "OBJETIVO: Garantizar las condiciones del proceso y funcionalidad de los equipos antes de iniciar una fabricacion, mediante la verificacion minimizar los riesgos de contaminacion y confusion";
            hoja_trabajo.Cells[6, 1] = "Area: Tocador III";

            hoja_trabajo.Cells[1, 2] = "CONTROLES DE PROCESO";
           




            hoja_trabajo.Cells[1, 3] = "Fecha Emision: 12-09-11";
            hoja_trabajo.Cells[2, 3] = "Version: 06";
            hoja_trabajo.Cells[3, 3] = "Pagina 1/1";


            hoja_trabajo.Cells[1, 5] = "FECHA: "  + control.FECHA.ToShortDateString();
            hoja_trabajo.Cells[2, 5] = "CODIGO ORDEN: "+ control.CODIGOORDENPRODUCCION;
            hoja_trabajo.Cells[3, 5] = "PRODUCTO ACTUAL: " + control.PRODUCTOS.NOMBREPRODUCTO;
            hoja_trabajo.Cells[3, 6] = "LOTE: " + control.LOTEPRODUCTO;


            hoja_trabajo.Cells[4, 5] = "PRODUCTO ANTERIOR: " + control.PRODUCTOS1.NOMBREPRODUCTO;
            hoja_trabajo.Cells[4, 6] = "LOTE: " + control.LOTEPRODUCTOANT;

            hoja_trabajo.Cells[5, 5] = "DESINFECTANTE: " + control.DESINFECTANTE;


        
            //Escribimos en la hoja de excel los nombres de los encabezados
            foreach (var column in a)
            {
                foreach (var prop in column.GetType().GetProperties())
                {
                    hoja_trabajo.Cells[6, col] = prop.Name;
                    hoja_trabajo.Cells[6, col].Font.Color = Color.White;
                    hoja_trabajo.Cells[6, col].Interior.Color = Color.Blue;
                    col += 1;
                }

                break;
            }


            //Obtiene las propiedades del primer item de la lista
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties((a.First()));
            
            

            //Recorremos el DataGridView rellenando la hoja de trabajo
            for (int i = 0; i < a.Count; i++)
                for (int j = 0; j < properties.Count; j++)
                    hoja_trabajo.Cells[i + 7, j + 1].Value = properties[j].GetValue(a[i]);



            //combino y centro para el emcabezado
            hoja_trabajo.get_Range("B1", "B3").Merge();
            hoja_trabajo.get_Range("A4", "D5").Merge();

            hoja_trabajo.get_Range("B1", "B3").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            hoja_trabajo.get_Range("B1", "B3").Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;


            //Combinamos celdas para mostrar el texto largo
            var r = hoja_trabajo.get_Range("A4", "D5");
            r.Select();
            r.WrapText = true;
            r.Style.Font.Bold = true;
            r.Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            r.Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

            //Tamaño automatico de las columnas
            hoja_trabajo.Columns.AutoFit();


           

            //libros_trabajo.SaveAs("C:\\Program Files (x86)\\PROALNET\\Cavidi\\Content\\Reportes\\FormatoToc.xls",
            libros_trabajo.SaveAs(rutaarchivo,
                    Microsoft.Office.Interop.Excel.XlFileFormat.xlAddIn);
            libros_trabajo.Close();
            
            //libros_trabajo.Close(true);
            aplicacion.Quit();

        }

        public void GenerarPPr01(string rutaarchivo, int parametro)
        {
            int col = 1;

            try
            {
                var d = db.FORMATOPPR01.Where(c => c.ID == (parametro)).FirstOrDefault();

                var b = db.FORMATOPPR01ATRIBUTOS.Where(c => c.IDFORMATOPPR01 == (parametro)).OrderBy(c => c.MUESTRA).Select(c => new
                {
                    c.MUESTRA,
                    c.HORA1,
                    c.HORA2,
                    c.HORA3,
                    c.HORA4,
                    c.HORA5,
                    c.HORA6,
                    c.HORA7,
                    c.HORA8,
                    c.HORA9,
                    c.HORA10,
                    c.HORA11,
                    c.HORA12,
                    c.HORA13,
                    c.HORA14,
                    c.HORA15,
                    c.HORA16,
                    c.HORA17,
                    c.HORA18,
                    c.HORA19,
                    c.HORA20,
                    c.HORA21,
                    c.HORA22,
                    c.HORA23,
                    c.HORA24
                }).Distinct().ToList();





                var a = db.FORMATOPPR01PESO.Where(c => c.IDFORMATOPPR01 == (parametro)).OrderBy(c => c.MUESTRA).Select(c => new
                {
                    c.MUESTRA,
                    c.HORA1,
                    c.HORA2,
                    c.HORA3,
                    c.HORA4,
                    c.HORA5,
                    c.HORA6,
                    c.HORA7,
                    c.HORA8,
                    c.HORA9,
                    c.HORA10,
                    c.HORA11,
                    c.HORA12,
                    c.HORA13,
                    c.HORA14,
                    c.HORA15,
                    c.HORA16,
                    c.HORA17,
                    c.HORA18,
                    c.HORA19,
                    c.HORA20,
                    c.HORA21,
                    c.HORA22,
                    c.HORA23,
                    c.HORA24
                }).Distinct().ToList();


                if (b == null || a == null)
                    return;


                //Creamos el objecto excel
                Microsoft.Office.Interop.Excel.Application aplicacion;
                Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
                Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;

                //Creamos una nueva instancia de la hoja de excel, libro de trabajo y  hoja de trabajo
                aplicacion = new Microsoft.Office.Interop.Excel.Application();
                libros_trabajo = aplicacion.Workbooks.Add();
                hoja_trabajo =
                    (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

                //COLUMNAS DEL EMCABEZADO
                hoja_trabajo.Cells[1, 1] = "AZUL K S.A.";
                hoja_trabajo.Cells[2, 1] = "PRODUCCION";
                hoja_trabajo.Cells[3, 1] = "PP-R-001";
                hoja_trabajo.Cells[4, 1] = "OBJETIVO: dar cumplimiento a requisitos legales y garantizar al cliente las especificaciones de peso y atributos del producto";
                hoja_trabajo.Cells[6, 1] = "Area: Tocador III";

                hoja_trabajo.Cells[1, 2] = "CONTROL DE PESOS Y ATRIBUTOS";

                hoja_trabajo.Cells[1, 3] = "Fecha Emision: 16-07-05";
                hoja_trabajo.Cells[2, 3] = "Version: 04";
                hoja_trabajo.Cells[3, 3] = "Pagina 1/1";

                if(d.USUARIOS != null)
                    hoja_trabajo.Cells[1, 5] = "Operario: " + d.USUARIOS.PERSONAL.NOMBRES + ' ' + d.USUARIOS.PERSONAL.APELLIDOS;

                hoja_trabajo.Cells[2, 5] = "Turno: "+ d.TURNO;

                //Escribimos en la hoja de excel los nombres de los encabezados
                foreach (var column in a)
                {
                    foreach (var prop in column.GetType().GetProperties())
                    {
                        hoja_trabajo.Cells[7, col] = prop.Name;
                        hoja_trabajo.Cells[7, col].Font.Color = Color.White;
                        hoja_trabajo.Cells[7, col].Interior.Color = Color.Blue;
                        col += 1;
                    }

                    break;
                }


                //Obtiene las propiedades del primer item de la lista
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties((a.First()));

                col = 1;

                //Recorremos el DataGridView rellenando la hoja de trabajo
                //for (int i = 0; i < a.Count; i++)
                //    for (int j = 0; j < properties.Count; j++)
                //        hoja_trabajo.Cells[i + 8, j + 1].Value = properties[j].GetValue(a[i]);

                double promediototal = 0;
                int cantidadpromedio = 0;

                for (int i = 0; i < properties.Count; i++)
                {

                    double promedio = 0;
                    for (int j = 0; j < a.Count; j++)
                    {
                        if (i > 0 && j != 0)
                            promedio = promedio + Convert.ToDouble(properties[i].GetValue(a[j]));
                        hoja_trabajo.Cells[j + 8, i + 1].Value = properties[i].GetValue(a[j]);

                        if (j == a.Count - 1 && i > 0)
                        {
                            promedio = promedio / j;

                            if (promedio > 0)
                            {
                                promediototal = promediototal + promedio;
                                cantidadpromedio = cantidadpromedio + 1;
                            }


                            hoja_trabajo.Cells[j + 9, i + 1].Value = promedio;
                        }
                    }

                    if(i == properties.Count - 1)
                        hoja_trabajo.Cells[14, 26].Value = promediototal /  cantidadpromedio;
                } 


                properties = TypeDescriptor.GetProperties((b.First()));

                //Escribimos en la hoja de excel los nombres de los encabezados
                foreach (var column in b)
                {
                    foreach (var prop in column.GetType().GetProperties())
                    {
                        hoja_trabajo.Cells[17, col] = prop.Name;
                        hoja_trabajo.Cells[17, col].Font.Color = Color.White;
                        hoja_trabajo.Cells[17, col].Interior.Color = Color.Blue;
                        col += 1;
                    }

                    break;
                }
                //Recorremos el DataGridView rellenando la hoja de trabajo
                for (int i = 0; i < b.Count; i++)
                {

                    for (int j = 0; j < properties.Count; j++)
                    {
                        hoja_trabajo.Cells[i + 18, j + 1].value = properties[j].GetValue(b[i]);
                    }
                }

              


                //combino y centro para el emcabezado
                hoja_trabajo.get_Range("B1", "B3").Merge();
                hoja_trabajo.get_Range("A4", "D5").Merge();

                hoja_trabajo.get_Range("B1", "B3").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                hoja_trabajo.get_Range("B1", "B3").Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;


                //Combinamos celdas para mostrar el texto largo
                var r = hoja_trabajo.get_Range("A4", "D5");
                r.Select();
                r.WrapText = true;
                r.Style.Font.Bold = true;
                r.Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                r.Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //Tamaño automatico de las columnas
                hoja_trabajo.Columns.AutoFit();




                libros_trabajo.SaveAs(rutaarchivo,
                            Microsoft.Office.Interop.Excel.XlFileFormat.xlAddIn);

                
                libros_trabajo.Close();
                //libros_trabajo.Close(true);
                aplicacion.Quit();

            }
            catch (Exception ex) {

            }
        }
    }
}

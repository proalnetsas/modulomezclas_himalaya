﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SipModuloManualModelo.General
{
    public class SessionExpireFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
                HttpContext.Current.Response.Cache.SetNoStore();
             
                if (HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString() != "Index")
                {
                    if (HttpContext.Current.Cache["autenticated-user"] == null || Generales.SesionTrabajo.RolActual < 1)
                    {
                        if (Generales.SesionBloqueada)
                        {
                            if (HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString() != "Seguridad" &&
                                (HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString() != "ReactivarSesion" ||
                                HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString() != "CancelarSesion" ||
                                HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString() != "ActualizaTiempoSesion" ||
                                HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString() != "ValidaTiempoSesion"))
                            {
                                Generales.RedireccionarUrl("Home", "Home");
                                Generales.SesionTrabajo = new SesionDeTrabajoActual();
                            }
                        }
                        else
                        {
                            if (Generales.SesionBloqueada || Generales.SesionTrabajo == null || Generales.SesionTrabajo.CodigoUsuario == null)
                            {
                                Generales.RedireccionarUrl("Home", "Index");
                                Generales.SesionTrabajo = new SesionDeTrabajoActual();
                            }

                        }

                    }
                    else
                    {
                        String Action = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString();
                        String UrlActual = "/" + HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString() + (Action == String.Empty ? String.Empty : "/") + Action;
                        List<PermisosPorUsuario> opcionesPorUsuario = Generales.SesionTrabajo.OpcionesPorUsuario as List<PermisosPorUsuario>;

                        if (UrlActual != "/Inicio/Index" && UrlActual != "/AccesoDenegado")
                        {

                            PermisosPorUsuario perm = null;
                            if (opcionesPorUsuario != null)
                            {
                                perm = (from x in opcionesPorUsuario
                                        where x.Url.ToString().ToLower() == UrlActual.ToLower()
                                        select x).FirstOrDefault();
                            }
                            if (perm != null)
                            {
                                Generales.UltimoMovimientoSistema = DateTime.Now;
                                if (!perm.Estado)
                                    Generales.RedireccionarUrl("Home", "Index");
                            }
                        }

                    }
                }
                else if (HttpContext.Current.Cache["autenticated-user"] != null)
                {
                    if (Generales.SesionTrabajo != null && Generales.SesionTrabajo.RolActual > 0)
                        Generales.RedireccionarUrl("Home", "Index");
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using SipModuloManualModelo.Dto;
using SipModuloManualModelo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;

namespace SipModuloManual
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                       name: "DefaultApi",
                       routeTemplate: "api/{controller}/{id}",
                       defaults: new { id = RouteParameter.Optional }
                   );


            config.Routes.MapHttpRoute(
                name: "DefaultApiRouted",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { param = RouteParameter.Optional }
            );



            // Quite los comentarios de la siguiente línea de código para habilitar la compatibilidad de consultas para las acciones con un tipo de valor devuelto IQueryable o IQueryable<T>.
            // Para evitar el procesamiento de consultas inesperadas o malintencionadas, use la configuración de validación en QueryableAttribute para validar las consultas entrantes.
            // Para obtener más información, visite http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // Para deshabilitar el seguimiento en la aplicación, incluya un comentario o quite la siguiente línea de código
            // Para obtener más información, consulte: http://www.asp.net/web-api
            //config.EnableSystemDiagnosticsTracing();

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<ORDENES_DE_PRODUCCION>("ORDENES_DE_PRODUCCION");
            builder.EntitySet<PRODUCTOS>("PRODUCTOS");
            builder.EntitySet<PARTESXPRODUCTOXPROCESO>("PARTESXPRODUCTOXPROCESOes");
            builder.EntitySet<PROCESO>("PROCESOes");
            builder.EntitySet<MATERIALES>("MATERIALES");
            builder.EntitySet<MATERIALESPORPRODUCTO>("MATERIALESPORPRODUCTOes");
            builder.EntitySet<PRODUCTOPORPROCESO>("PRODUCTOPORPROCESOes");
            builder.EntitySet<MATERIALESPORPRODUCTO>("MATERIALESPORPRODUCTOes");
            builder.EntitySet<PUESTOTRABAJOSEGUNPRODUCTO>("PUESTOTRABAJOSEGUNPRODUCTOes");
            builder.EntitySet<ORDENPRODUCCIONXPRODUCTO>("ORDENPRODUCCIONXPRODUCTOes");
            builder.EntitySet<PERSONAL>("PERSONAL");
            builder.EntitySet<ROLES>("ROLES");
            builder.EntitySet<USUARIOS>("USUARIOS");
            builder.EntitySet<FORMATOTOC>("FORMATOTOCs");
            builder.EntitySet<USUARIOSXROL>("USUARIOSXROLs");

            //var model = builder.GetEdmModel();
            ValidarLogin(builder);
            DeletePersonal(builder);
            DeleteUsuarioXRol(builder);
            CrearOperacionTrabajo(builder);
            CrearOperacionTrabajoHimalaya(builder);
            crearReporteDinamycTOC(builder);
            crearReporteDinamycTOCHimalaya(builder);
            crearReporteHistoricoHimalaya(builder);
            crearReporteDinamycTOCExcel(builder);
            CrearUsuario(builder);
            CrearUsuarioXRol(builder);
            ActualizarUsuario(builder);
            ActualizarPersonal(builder);
            CrearPersonal(builder);
            CerrarBatch(builder);
            CrearNuevoBatch(builder);
            CargarLotesSAP(builder);
            GetOrdenProduccion(builder);
            GetListaMateriales(builder);
            GetListaMaterialesMezclado(builder);
            GetListaMaterialesMezclado2(builder);
            consultarDatosOperacion(builder);
            consultarDatosOperacionHimalaya(builder);
            consultarInformacionNuevoBatch(builder);
            ConsultarDatosOPIniciarFabricacion(builder);
            CantidadOperacionesHechas(builder);
            ConsultaPesoBatch(builder);
            ConsultarCantidadAPesar(builder);
            ConsultarOperacionBase(builder);
            GetListaProductos(builder);
            GetListaProductosYPm(builder);
            GetAdminUsuarios(builder);
            GetAdminPersonal(builder);
            ConsultarBatchCerrado(builder);
            ConsultarOrdenReporte(builder);
            ConsultarMezclaReporte(builder);
            CheckMezclaPesada(builder);
            crearReporteConsolidadoHimalaya(builder);
            ConsultarProgramacionSemana(builder);
            ConsultarLimitesMezclas(builder);

            mostrarPersonalparausuario(builder);
            MostrarRolesParausuario(builder);


            ReporteToc01(builder);
            CantidadPesadaOP(builder);

            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }

        static void ConsultarProgramacionSemana(ODataConventionModelBuilder builder)
        {
            var consultarProgramacionSemana = builder.Entity<ORDENPRODUCCIONXPRODUCTO>().Collection.Action("ConsultarProgramacionSemana");
            consultarProgramacionSemana.Returns<string>();
        }

        static void ConsultarLimitesMezclas(ODataConventionModelBuilder builder)
        {
            var consultarLimitesMezclas = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarLimitesMezclas");
            consultarLimitesMezclas.ReturnsCollection<string>();
        }

        static void ReporteToc01(ODataConventionModelBuilder builder)
        {
            var mostrarReporte = builder.Entity<FORMATOTOC>().Collection.Action("ReporteToc01Orden");
            mostrarReporte.Parameter<string>("CODIGOORDENPRODUCCION");
            mostrarReporte.Parameter<string>("CODIGOPRODUCTO");
            mostrarReporte.Parameter<string>("FECHADESDE");
            mostrarReporte.Parameter<string>("FECHAHASTA");

            mostrarReporte.Returns<string>();
        }

        static void GetAdminUsuarios(ODataConventionModelBuilder builder)
        {
            var getAdminUsuarios = builder.Entity<USUARIOS>().Collection.Action("GetAdminUsuarios");
            getAdminUsuarios.ReturnsCollectionFromEntitySet<AdminUsuariosDTO>("GetAdminUsuarios");
        }

        static void GetAdminPersonal(ODataConventionModelBuilder builder)
        {
            var getAdminPersonal = builder.Entity<USUARIOS>().Collection.Action("GetAdminPersonal");
            getAdminPersonal.ReturnsCollectionFromEntitySet<AdminPersonalDTO>("GetAdminPersonal");
        }
       

        static void mostrarPersonalparausuario(ODataConventionModelBuilder builder)
        {
            var mostrarpersonal = builder.Entity<USUARIOS>().Collection.Action("GetPersonalSinUsuario");
            mostrarpersonal.ReturnsCollectionFromEntitySet<PersonalDTO>("GetPersonalSinUsuario");
        }

        static void MostrarRolesParausuario(ODataConventionModelBuilder builder)
        {
            var mostrarRoles = builder.Entity<USUARIOS>().Collection.Action("GetRolesParaUsuario");
            mostrarRoles.ReturnsCollectionFromEntitySet<RolesDTO>("GetRolesParaUsuario");
        }

        static void CantidadOperacionesHechas(ODataConventionModelBuilder builder)
        {
            var cantidadOperacioneshechas = builder.Entity<FORMATOTOC>().Collection.Action("CantidadOperacionesHechas");
            cantidadOperacioneshechas.Parameter<string>("CODIGOPRODUCTO");
            cantidadOperacioneshechas.Parameter<string>("CODIGOPUESTO");
            cantidadOperacioneshechas.Parameter<string>("CODIGOORDENPRODUCCION");
            cantidadOperacioneshechas.Returns<int>();
        }

        static void ConsultaPesoBatch(ODataConventionModelBuilder builder)
        {
            var ConsultaPesoBatch = builder.Entity<FORMATOTOC>().Collection.Action("ConsultaPesoBatch");

            ConsultaPesoBatch.Parameter<string>("CODIGOPRODUCTO");
            ConsultaPesoBatch.Parameter<string>("CODIGOPUESTO");
            ConsultaPesoBatch.Parameter<string>("CODIGOORDENPRODUCCION");
            ConsultaPesoBatch.Returns<double>();

        }

        static void ConsultarCantidadAPesar(ODataConventionModelBuilder builder)
        {
            var ConsultarCantidadAPesar = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarCantidadAPesar");

            ConsultarCantidadAPesar.Parameter<string>("CODIGOPRODUCTO");
            ConsultarCantidadAPesar.Parameter<string>("CODIGOMATERIAL");
            ConsultarCantidadAPesar.Returns<double>();
        }

        static void CerrarBatch(ODataConventionModelBuilder builder)
        {
            var cerrarBatch = builder.Entity<FORMATOTOC>().Collection.Action("CerrarBatch");

            cerrarBatch.Parameter<string>("CODIGOORDENPRODUCCION");
            cerrarBatch.Parameter<string>("CODIGOPRODUCTO");
            cerrarBatch.Parameter<string>("NUMEROBATCH");



        }
        
        static void ConsultarOperacionBase(ODataConventionModelBuilder builder)
        {
            var consultarOperacionBase = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarOperacionBase");
            consultarOperacionBase.Parameter<string>("CODIGOPRODUCTO");
            consultarOperacionBase.Parameter<string>("CODIGOPUESTO");
            consultarOperacionBase.Parameter<string>("CODIGOORDENPRODUCCION");
            consultarOperacionBase.Returns<string>();
        }

        static void ConsultarBatchCerrado(ODataConventionModelBuilder builder)
        {
            var consultarBatchCerrado = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarBatchCerrado");

            consultarBatchCerrado.Parameter<string>("CODIGOORDENPRODUCCION");
            consultarBatchCerrado.Parameter<string>("CODIGOPRODUCTO");
            consultarBatchCerrado.Parameter<string>("NROOPERACION");
            consultarBatchCerrado.Returns<bool>();
        }
        
        static void crearReporteDinamycTOC(ODataConventionModelBuilder builder)
        {
            var creareporteDinamycTOC = builder.Entity<FORMATOTOC>().Collection.Action("CrearReporteDinamycTOC");
            creareporteDinamycTOC.Parameter<string>("CODIGOPRODUCTO");
            creareporteDinamycTOC.Parameter<string>("CODIGOPUESTO");
            creareporteDinamycTOC.Parameter<string>("CODIGOORDENPRODUCCION");
            creareporteDinamycTOC.Returns<string>();
        }

        static void crearReporteDinamycTOCHimalaya(ODataConventionModelBuilder builder)
        {
            var crearReporteDinamycTOCHimalaya = builder.Entity<FORMATOTOC>().Collection.Action("CrearReporteDinamycTOCHimalaya");
            crearReporteDinamycTOCHimalaya.Parameter<string>("CODIGOPRODUCTO");
            crearReporteDinamycTOCHimalaya.Parameter<string>("CODIGOPUESTO");
            crearReporteDinamycTOCHimalaya.Parameter<string>("CODIGOORDENPRODUCCION");
            crearReporteDinamycTOCHimalaya.Returns<string>();
        }

        static void crearReporteHistoricoHimalaya(ODataConventionModelBuilder builder)
        {
            var crearReporteHistoricoHimalaya = builder.Entity<FORMATOTOC>().Collection.Action("CrearReporteHistoricoHimalaya");
            crearReporteHistoricoHimalaya.Parameter<string>("CODIGOPRODUCTO");
            crearReporteHistoricoHimalaya.Parameter<string>("CODIGOPUESTO");
            crearReporteHistoricoHimalaya.Parameter<string>("CODIGOORDENPRODUCCION");
            crearReporteHistoricoHimalaya.Returns<string>();
        }

        static void crearReporteConsolidadoHimalaya(ODataConventionModelBuilder builder)
        {
            var crearReporteConsolidadoHimalaya = builder.Entity<FORMATOTOC>().Collection.Action("CrearReporteConsolidadoHimalaya");
            crearReporteConsolidadoHimalaya.Parameter<string>("CODIGOPRODUCTO");;
            crearReporteConsolidadoHimalaya.Parameter<string>("CODIGOORDENPRODUCCION");
            crearReporteConsolidadoHimalaya.Parameter<string>("FECHADESDE");
            crearReporteConsolidadoHimalaya.Parameter<string>("FECHAHASTA");
            crearReporteConsolidadoHimalaya.Returns<string>();
        }
        static void crearReporteDinamycTOCExcel(ODataConventionModelBuilder builder)
        {
            var creareporteDinamycTOC = builder.Entity<FORMATOTOC>().Collection.Action("CrearReporteDinamycTOCExcel");
            creareporteDinamycTOC.Parameter<string>("CODIGOPRODUCTO");
            creareporteDinamycTOC.Parameter<string>("CODIGOPUESTO");
            creareporteDinamycTOC.Parameter<string>("CODIGOORDENPRODUCCION");
            creareporteDinamycTOC.Returns<string>();
        }

        static void consultarDatosOperacion(ODataConventionModelBuilder builder) {
            var consultardatosOperacion = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarDatosOperacion");
            consultardatosOperacion.Parameter<string>("CODIGOPRODUCTO");
            consultardatosOperacion.Parameter<string>("CODIGOPUESTO");
            consultardatosOperacion.Parameter<string>("CODIGOORDENPRODUCCION");
            consultardatosOperacion.Parameter<int>("NROOPERACION");
            consultardatosOperacion.ReturnsFromEntitySet<MateriaPrimaDTO>("ConsultarDatosOperacion");
        }

        static void CantidadPesadaOP(ODataConventionModelBuilder builder)
        {
            var cantidadPesadaOP = builder.Entity<FORMATOTOC>().Collection.Action("CantidadPesadaOP");
            cantidadPesadaOP.Parameter<string>("CODIGOORDENPRODUCCION");
            cantidadPesadaOP.Returns<double>();

            var actualizarOPFormatoTocSAP = builder.Entity<FORMATOTOC>().Collection.Action("ActualizarOPFormatoTocSAP");
            actualizarOPFormatoTocSAP.Parameter<string>("CODIGOORDENPRODUCCION");

            var consultarRecetaOP = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarRecetaOP");
            consultarRecetaOP.Parameter<string>("CODIGOORDENPRODUCCION");
            consultarRecetaOP.ReturnsCollectionFromEntitySet<INT_SAP_CARGUE>("ConsultarRecetaOP");
        }

        static void consultarDatosOperacionHimalaya(ODataConventionModelBuilder builder)
        {
            var consultarDatosOperacionHimalaya = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarDatosOperacionHimalaya");
            consultarDatosOperacionHimalaya.Parameter<string>("CODIGOPRODUCTO");
            consultarDatosOperacionHimalaya.Parameter<string>("CODIGOORDENPRODUCCION");
            consultarDatosOperacionHimalaya.Parameter<int>("NROOPERACION");
            consultarDatosOperacionHimalaya.ReturnsFromEntitySet<MateriaPrimaDTO>("ConsultarDatosOperacionHimalaya");
        }

        static void consultarInformacionNuevoBatch(ODataConventionModelBuilder builder)
        {
            var consultarInformacionNuevoBatch = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarInformacionNuevoBatch");
            consultarInformacionNuevoBatch.Parameter<string>("CODIGOPRODUCTO");
            consultarInformacionNuevoBatch.Parameter<string>("CODIGOORDENPRODUCCION");
            consultarInformacionNuevoBatch.Parameter<int>("NROOPERACION");
            consultarInformacionNuevoBatch.Parameter<double>("PESOBATCH");
            consultarInformacionNuevoBatch.ReturnsFromEntitySet<NuevoBatchDTO>("ConsultarInformacionNuevoBatch");
        }

        static void CheckMezclaPesada(ODataConventionModelBuilder builder)
        {
            var checkMezclaPesada = builder.Entity<FORMATOTOC>().Collection.Action("CheckMezclaPesada");

            checkMezclaPesada.Parameter<string>("CODIGOORDENPRODUCCION");
            checkMezclaPesada.Parameter<string>("CODIGOPRODUCTO");
            checkMezclaPesada.ReturnsFromEntitySet<NuevoBatchDTO>("CheckMezclaPesada");

        }

        static void ConsultarOrdenReporte(ODataConventionModelBuilder builder)
        {
            var consultarOrdenReporte = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarOrdenReporte");
            consultarOrdenReporte.Parameter<string>("FECHAINICIO");
            consultarOrdenReporte.Parameter<string>("FECHAFIN");
            consultarOrdenReporte.Parameter<string>("TIPOCONSULTA");
            consultarOrdenReporte.ReturnsFromEntitySet<OrdenProduccionDTO>("ConsultarOrdenReporte");
        }

        static void ConsultarMezclaReporte(ODataConventionModelBuilder builder)
        {
            var consultarMezclaReporte = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarMezclaReporte");
            consultarMezclaReporte.Parameter<string>("FECHAINICIO");
            consultarMezclaReporte.Parameter<string>("FECHAFIN");
            consultarMezclaReporte.Parameter<string>("TIPOCONSULTA");
            consultarMezclaReporte.ReturnsFromEntitySet<ProductosDTO>("ConsultarMezclaReporte");
        }


        static void ConsultarDatosOPIniciarFabricacion(ODataConventionModelBuilder builder)
        {
            var consultarDatosOPIniciarFabricacion = builder.Entity<FORMATOTOC>().Collection.Action("ConsultarDatosOPIniciarFabricacion");
            consultarDatosOPIniciarFabricacion.Parameter<string>("CODIGOORDENPRODUCCION");
            consultarDatosOPIniciarFabricacion.ReturnsFromEntitySet<OrdenProduccionDTO>("ConsultarDatosOPIniciarFabricacion");
        }

        static void ValidarLogin(ODataConventionModelBuilder builder)
        {
            var login = builder.Entity<USUARIOS>().Collection.Action("Login");
            login.Parameter<string>("USUARIO");
            login.Parameter<string>("PASSWORD");
            login.Returns<bool>();
        }

        static void DeleteUsuarioXRol(ODataConventionModelBuilder builder)
        {
            var deleteUsuarioXRol  = builder.Entity<USUARIOS>().Collection.Action("DeleteUsuarioXRol");
            deleteUsuarioXRol.Parameter<string>("LOGIN");
            deleteUsuarioXRol.Parameter<int>("IDROL");
            deleteUsuarioXRol.Parameter<string>("ROL");
            deleteUsuarioXRol.Parameter<string>("PASSWORD");
            deleteUsuarioXRol.Parameter<string>("CODIGOPERSONAL");
        }

        static void DeletePersonal(ODataConventionModelBuilder builder)
        {
            var deletePersonal = builder.Entity<USUARIOS>().Collection.Action("DeletePersonal");

            deletePersonal.Parameter<string>("CODIGOPERSONAL");
            deletePersonal.Parameter<string>("CEDULA");
            deletePersonal.Parameter<string>("NOMBRES");
            deletePersonal.Parameter<string>("APELLIDOS");
            deletePersonal.Parameter<string>("SEXO");
            deletePersonal.Parameter<DateTime>("FECHANACIMIENTO");

        }


        static void ActualizarUsuario (ODataConventionModelBuilder builder)
        {
            var ActualizarUsuario = builder.Entity<USUARIOS>().Collection.Action("ActualizarUsuario");
           ActualizarUsuario.Parameter<string>("LOGIN");
           ActualizarUsuario.Parameter<int>("IDROL");
           ActualizarUsuario.Parameter<DateTime>("FECHACREACION");
           ActualizarUsuario.Parameter<string>("ROL");
           ActualizarUsuario.Parameter<string>("PASSWORD");
           ActualizarUsuario.Parameter<string>("CODIGOPERSONAL");

        }

        static void ActualizarPersonal(ODataConventionModelBuilder builder)
        {
            var actualizarPersonal = builder.Entity<USUARIOS>().Collection.Action("ActualizarPersonal");
            actualizarPersonal.Parameter<string>("CODIGOPERSONAL");
            actualizarPersonal.Parameter<string>("CEDULA");
            actualizarPersonal.Parameter<string>("NOMBRES");
            actualizarPersonal.Parameter<string>("APELLIDOS");
            actualizarPersonal.Parameter<string>("SEXO");
            actualizarPersonal.Parameter<DateTime>("FECHANACIMIENTO");


        }

        static void GetOrdenProduccion(ODataConventionModelBuilder builder)
        {
            //EntityTypeConfiguration<ORDENPRODUCCIONXPRODUCTO> OrdenConfiguration = builder.EntitySet<ORDENPRODUCCIONXPRODUCTO>("ORDENPRODUCCIONXPRODUCTOes").EntityType;
            var getOrdenProduccion = builder.Entity<ORDENPRODUCCIONXPRODUCTO>().Collection.Action("GetOrdenProduccion");
            getOrdenProduccion.ReturnsFromEntitySet<OrdenProduccionDTO>("GetOrdenProduccion");

         }

        static void GetListaMateriales(ODataConventionModelBuilder builder) {

            var getListaMateriales = builder.Entity<MATERIALESPORPRODUCTO>().Collection.Action("ListaMaterialesProductos");
            getListaMateriales.Parameter<string>("CODIGOPRODUCTO");
            getListaMateriales.Parameter<string>("CODIGOORDENPRODUCCION");
            getListaMateriales.ReturnsFromEntitySet<MaterialesDTO>("ListaMaterialesProductos");

        }

        static void GetListaProductos(ODataConventionModelBuilder buildder)
        {
            var getListaProductos = buildder.Entity<MATERIALESPORPRODUCTO>().Collection.Action("ListaProductos");
            getListaProductos.Parameter<string>("CODIGOPRODUCTO");
            getListaProductos.Parameter<string>("CODIGOORDENPRODUCCION");
            getListaProductos.ReturnsFromEntitySet<ProductosDTO>("ListaProductos");
        }

        static void GetListaProductosYPm(ODataConventionModelBuilder buildder)
        {
            var getListaProductosYPm = buildder.Entity<MATERIALESPORPRODUCTO>().Collection.Action("ListaProductosYPm");
            getListaProductosYPm.Parameter<string>("CODIGOPRODUCTO");
            getListaProductosYPm.Parameter<string>("CODIGOORDENPRODUCCION");
            getListaProductosYPm.ReturnsFromEntitySet<ProductosDTO>("ListaProductosYPm");
        }


        static void GetListaMaterialesMezclado(ODataConventionModelBuilder builder)
        {

            var getListaMateriales = builder.Entity<MATERIALESPORPRODUCTO>().Collection.Action("ListaMaterialesProductosMezclado");
            getListaMateriales.Parameter<string>("CODIGOPRODUCTO");
            getListaMateriales.Parameter<string>("CODIGOORDENPRODUCCION");
            getListaMateriales.Parameter<int>("NROOPERACION");
            getListaMateriales.ReturnsFromEntitySet<MaterialesDTO>("ListaMaterialesProductosMezclado");

        }

        static void GetListaMaterialesMezclado2(ODataConventionModelBuilder builder)
        {

            var getListaMateriales = builder.Entity<MATERIALESPORPRODUCTO>().Collection.Action("ListaMaterialesProductosMezclado2");
            getListaMateriales.Parameter<string>("CODIGOPRODUCTO");
            getListaMateriales.Parameter<string>("CODIGOORDENPRODUCCION");
            getListaMateriales.ReturnsFromEntitySet<MaterialesDTO>("ListaMaterialesProductosMezclado2");

        }

        static void CrearUsuarioXRol (ODataConventionModelBuilder builder)
        {
            var createUsuarioXRol = builder.Entity<USUARIOS>().Collection.Action("CrearUsuarioXRol");
            createUsuarioXRol.Parameter<string>("LOGIN");
            createUsuarioXRol.Parameter<int>("IDROL");
            createUsuarioXRol.Parameter<string>("ROL");
            createUsuarioXRol.Parameter<string>("PASSWORD");
            createUsuarioXRol.Parameter<string>("CODIGOPERSONAL");

        }

        static void CrearUsuario(ODataConventionModelBuilder builder)
        {
            var createUsuario = builder.Entity<USUARIOS>().Collection.Action("CrearUsuario");

            createUsuario.Parameter<string>("LOGIN");
            createUsuario.Parameter<string>("PASSWORD");
            createUsuario.Parameter<string>("CODIGOPERSONAL");

        }
       
        static void CrearPersonal(ODataConventionModelBuilder builder)
        {
            var crearPersonal = builder.Entity<USUARIOS>().Collection.Action("CrearPersonal");

            crearPersonal.Parameter<string>("CODIGOPERSONAL");
            crearPersonal.Parameter<string>("CEDULA");
            crearPersonal.Parameter<string>("NOMBRE");
            crearPersonal.Parameter<string>("APELLIDOS");
            crearPersonal.Parameter<string>("SEXO");
            crearPersonal.Parameter<string>("FECHANACIMIENTO");
        }



        static void CrearOperacionTrabajo(ODataConventionModelBuilder builder)
        {
            var createOperacionToc = builder.Entity<FORMATOTOC>().Collection.Action("CrearOperacionTrabajo");
            createOperacionToc.Parameter<string>("CODIGOPRODUCTOS");
            createOperacionToc.Parameter<string>("LOTE");
            createOperacionToc.Parameter<string>("CODIGOPUESTO");
            createOperacionToc.Parameter<string>("CODIGOORDENPRODUCCION");
            createOperacionToc.Parameter<string>("CODIGOTURNO");
            createOperacionToc.Parameter<string>("LOGIN");
            createOperacionToc.Parameter<int>("NROOPERACION");
            createOperacionToc.Parameter<string>("ORDENPRODUCCIOBASE");
            createOperacionToc.Parameter<double>("KILOGRAMOS");
            createOperacionToc.Parameter<int>("MINUTOSMEZCLA");
            createOperacionToc.Parameter<bool>("BASE_SILO");
            createOperacionToc.Parameter<string>("HORA");
            createOperacionToc.Parameter<int>("LAMINADAS");
            createOperacionToc.Parameter<string>("CODMATERIAL");
            createOperacionToc.Parameter<double>("CANTIDAD");
            createOperacionToc.Parameter<bool>("PESOMANUAL");
            createOperacionToc.Parameter<string>("FECHA");
            

        }

        static void CrearOperacionTrabajoHimalaya(ODataConventionModelBuilder builder)
        {
            var crearOperacionTrabajoHimalaya = builder.Entity<FORMATOTOC>().Collection.Action("CrearOperacionTrabajoHimalaya");
            crearOperacionTrabajoHimalaya.Parameter<string>("CODIGOPRODUCTOS");
            crearOperacionTrabajoHimalaya.Parameter<string>("LOTE");
            crearOperacionTrabajoHimalaya.Parameter<string>("CODIGOPUESTO");
            crearOperacionTrabajoHimalaya.Parameter<string>("CODIGOORDENPRODUCCION");
            crearOperacionTrabajoHimalaya.Parameter<string>("CODIGOTURNO");
            crearOperacionTrabajoHimalaya.Parameter<string>("LOGIN");
            crearOperacionTrabajoHimalaya.Parameter<int>("NROOPERACION");
            crearOperacionTrabajoHimalaya.Parameter<string>("ORDENPRODUCCIOBASE");
            crearOperacionTrabajoHimalaya.Parameter<double>("KILOGRAMOS");
            crearOperacionTrabajoHimalaya.Parameter<int>("MINUTOSMEZCLA");
            crearOperacionTrabajoHimalaya.Parameter<bool>("BASE_SILO");
            crearOperacionTrabajoHimalaya.Parameter<string>("HORA");
            crearOperacionTrabajoHimalaya.Parameter<int>("LAMINADAS");
            crearOperacionTrabajoHimalaya.Parameter<string>("CODMATERIAL");
            crearOperacionTrabajoHimalaya.Parameter<double>("CANTIDAD");
            crearOperacionTrabajoHimalaya.Parameter<bool>("PESOMANUAL");
            crearOperacionTrabajoHimalaya.Parameter<string>("FECHA");
            crearOperacionTrabajoHimalaya.Parameter<string>("CODIGOPREMEZCLA");
        }

        static void CrearNuevoBatch(ODataConventionModelBuilder builder)
        {
            var crearNuevoBatch = builder.Entity<FORMATOTOC>().Collection.Action("CrearNuevoBatch");
            crearNuevoBatch.Parameter<string>("CODIGOPRODUCTO");
            crearNuevoBatch.Parameter<string>("CODIGOORDENPRODUCCION");
            crearNuevoBatch.Parameter<string>("NROOPERACION");
            crearNuevoBatch.Parameter<string>("PESOBATCH");

        }

        static void CargarLotesSAP(ODataConventionModelBuilder builder)
        {
            var cargarlotessap = builder.Entity<FORMATOTOC>().Collection.Action("CargarLotesSAP");
            cargarlotessap.Parameter<string>("CODIGOITEM");
            cargarlotessap.ReturnsFromEntitySet<LoteDTO>("CargarLotesSAP");
        }
    }
}

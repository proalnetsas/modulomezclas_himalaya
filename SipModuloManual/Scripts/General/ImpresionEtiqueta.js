﻿

$(document).ready(function () {

    if (document.URL.indexOf('Etiqueta') != -1)
    {
        AsignarValoresEtiqueta();
        
    }


});
// JavaScript source code
function AsignarValoresEtiqueta()
{
    var etapa;


    if (window.opener.$('input[name="Status"]:checked').val() == 1)
        etapa = "1 - Entrega";

    if (window.opener.$('input[name="Status"]:checked').val() == 2)
        etapa = "2 - En Proceso";

    if (window.opener.$('input[name="Status"]:checked').val() == 3)
        etapa = "3 - Devolucion";


    var etiqueta =
    {
        etapa: etapa,
        material: window.opener.$("#NombreMaterial").val(),
        codmaterial: window.opener.$("#codMaterial").val(),
        producto: window.opener.$("#Producto").val(),
        fechapesaje: window.opener.$("#FechaPesaje").val(),
        tara: parseFloat(window.opener.$("#Tara").val()),
        pesobruto: parseFloat(window.opener.$("#PesoBruto").val()),
        pesoneto: parseFloat(window.opener.$("#PesoNeto").val()),
        operario: window.opener.$("#OperarioEntrega").val(),
        operariore: window.opener.$("#OperarioRecibe").val(),
        fecharecibe: window.opener.$("#FechaRecibido").val(),
        fechaentrega: window.opener.$("#FechaEntrega").val(),
        observaciones: window.opener.$("#Observaciones").val(),
        codigoOrden: window.opener.$("#ordenProduccion").val(),
        nomaterial: window.opener.$("#LoteMaterial").val(),
        noanalisis: window.opener.$("#NAnalisis").val(),
        norecipiente: window.opener.$("#NRecipiente").val(),
        norecipientede: window.opener.$("#Consecutivo").val(),
        noloteproducto: window.opener.$("#LoteProducto").val()
    }
    
    if (etiqueta != undefined) {
        AsignarDatosImpresion(etiqueta);
    }
}


function AsignarDatosImpresion(datos)
{
    if (typeof (datos) == "object") {
        var codbarramaterial = $("#codBarraMaterial");

        var etapa = $('#etapa');
        var material = $('#material');
        var producto = $('#producto');
        var fechapesaje = $('#fechapesaje');
        var tara = $('#tara');
        var pesobruto = $('#pesobruto');
        var pesoneto = $('#pesoneto');
        var operario = $('#operario');
        var fechaentrega = $('#fechaentrega');
        var observaciones = $('#observaciones');
        var nomaterial = $('#nomaterial');
        var noanalisis = $('#noanalisis');
        var norecipiente = $('#norecipiente');
        var norecipientede = $('#norecipientede');
        var noloteproducto = $('#noloteproducto');
        var operariorec = $('#operariore');
        var fecharec = $('#fecharecibe');
        
        codbarramaterial.text('*'+ datos.codmaterial + '*');
        etapa.text(datos.etapa);
        material.text(datos.material);
        producto.text(datos.producto);
        fechapesaje.text(datos.fechapesaje);
        tara.text(parseFloat(datos.tara).toFixed(3) + " KG");
        pesobruto.text(parseFloat(datos.pesobruto).toFixed(3) + " KG");
        pesoneto.text(parseFloat(datos.pesoneto).toFixed(3) + " KG");
        operario.text(datos.operario);
        fechaentrega.text(datos.fechaentrega);
        observaciones.text(observaciones.text() + datos.observaciones);
        nomaterial.text(datos.nomaterial);
        noanalisis.text(datos.noanalisis);
        norecipiente.text(datos.norecipiente);
        norecipientede.text(datos.norecipientede);
        noloteproducto.text(datos.noloteproducto);
        operariorec.text(datos.operariore);
        fecharec.text(datos.fecharecibe);

        PrintWindow();

    }
    else {
        console.log('Por favor asigne como parámetro un objeto válido.');
    }
}

function CheckWindowState() {
    
    if (document.readyState == "complete") {
        window.close();
    }
    else {

        setTimeout("CheckWindowState()", 2000)   
    }
}

function PrintWindow() {
        window.print();
        CheckWindowState();
}
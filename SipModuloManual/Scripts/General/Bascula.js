﻿var connection;
var LimiteInferiorMezclas;
var LimiteSuperiorMezclas;

$(document).ready(function () {
    ConsultarLimitesMezclas();
    Bascula();
});



function PararComunicacion()
{
    connection.stop();
}

function ReactivarComunicacion()
{
    connection.start();
    connection.start();
}


function Bascula()
{

    //Se crea la conexion al puerto donde se expone el valor del peso
    connection = $.hubConnection("http://localhost:9992");
    //Se crea el clienthub
    proxyBascula = connection.createHubProxy("clientHub");
    //Se inicia la conexion
    connection.start({ logging: true })

    //se colaca en modo automatico el clienhub
    proxyBascula.on('data', function (dato) {

        if (dato != null) {

            var bruto = dato.Valor;

            if(($("#Tara").val()  != undefined))
            {
                if ($("#Tara").val() != "")
                {
                    var tara = $("#Tara").val();
                    var neto = parseFloat(bruto).toFixed(3) - parseFloat(tara);
                    $("#PesoNeto").val(neto.toFixed(3));
                }
                
                $("#PesoBruto").val(bruto);
            }

            if($("#Peso").val() != undefined)
            {
                $("#Peso").val(bruto);
            }

            ///Validacion para pintar el campo peso

            var cantidadAPesar = parseFloat($("#CantidadAPesar").val());
            var pesoTiempoReal = parseFloat($("#Peso").val());
            var cantidadPesada = parseFloat($("#CantidadPesada").val());
            var tiempoRealPesada = (pesoTiempoReal + cantidadPesada).toFixed(3);

            let LimiteInferior = parseFloat((cantidadAPesar - LimiteInferiorMezclas).toFixed(3));
            let LimiteSuperior = parseFloat((cantidadAPesar + LimiteSuperiorMezclas).toFixed(3));

            if (parseFloat(tiempoRealPesada) == cantidadAPesar) {
               document.getElementById("Peso").style.backgroundColor = "Green";
               //$("#btnGuardarFormato").attr("disabled", false);
            }
            else if ((parseFloat(tiempoRealPesada) > LimiteSuperior) || (parseFloat(tiempoRealPesada) < LimiteInferior)) {

               document.getElementById("Peso").style.backgroundColor = "Red";
               //$("#btnGuardarFormato").attr("disabled", true);
            }
            else if ((parseFloat(tiempoRealPesada) >= LimiteInferior) && (parseFloat(tiempoRealPesada) <= LimiteSuperior)) {

               document.getElementById("Peso").style.backgroundColor = "Yellow";
               //$("#btnGuardarFormato").attr("disabled", false);
            }

            //if (parseFloat(tiempoRealPesada) == cantidadAPesar) {
            //    document.getElementById("Peso").style.backgroundColor = "Green";
            //    //$("#btnGuardarFormato").attr("disabled", false);
            //}
            //else if ((parseFloat(tiempoRealPesada) > cantidadAPesar) || (parseFloat(tiempoRealPesada) < (cantidadAPesar * 0.9))) {

            //    document.getElementById("Peso").style.backgroundColor = "Red";

            //}
            //else if ((parseFloat(tiempoRealPesada) >= cantidadAPesar * 0.9) && (parseFloat(tiempoRealPesada) < cantidadAPesar)) {

            //    document.getElementById("Peso").style.backgroundColor = "Yellow";

            //}



            //var cantidadAPesar = parseFloat($("#CantidadAPesar").val());
            //var pesoTiempoReal = parseFloat($("#Peso").val());


            //if (pesoTiempoReal == cantidadAPesar) {

            //    document.getElementById("Peso").style.backgroundColor = "Green";
            //    $("#btnGuardarFormato").attr("disabled", false);
            //}
            //else if ((pesoTiempoReal > (cantidadAPesar - 0.1) && pesoTiempoReal < cantidadAPesar) || ((pesoTiempoReal < (cantidadAPesar + 0.1)) && pesoTiempoReal > cantidadAPesar) || (pesoTiempoReal == (cantidadAPesar - 0.1)) || (pesoTiempoReal == (cantidadAPesar + 0.1))) {

            //    document.getElementById("Peso").style.backgroundColor = "Yellow";
            //    $("#btnGuardarFormato").attr("disabled", false);
            //}
            //else if (pesoTiempoReal < (cantidadAPesar - 0.1) || pesoTiempoReal > (cantidadAPesar + 0.1)) {
            //    document.getElementById("Peso").style.backgroundColor = "Red";

            //    $("#btnGuardarFormato").attr("disabled", true);
            //}

            
            //document.getElementById("PesoBruto").value = Bruto;
        }
    });
}

function ConsultarLimitesMezclas() {

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultarLimitesMezclas", function (data) {

        debugger;
        if (data.value != null) {

            LimiteInferiorMezclas = parseFloat(data.value[0])
            LimiteSuperiorMezclas = parseFloat(data.value[1])

        }

    }, errorHandler, JSON.stringify());
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ORDENES_DE_PRODUCCION>("ORDENES_DE_PRODUCCION");
    builder.EntitySet<ORDENPRODUCCIONXPRODUCTO>("ORDENPRODUCCIONXPRODUCTO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ORDENES_DE_PRODUCCIONController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/ORDENES_DE_PRODUCCION
        [EnableQuery]
        public IQueryable<ORDENES_DE_PRODUCCION> GetORDENES_DE_PRODUCCION()
        {
            return db.ORDENES_DE_PRODUCCION;
        }

        // GET: odata/ORDENES_DE_PRODUCCION(5)
        [EnableQuery]
        public SingleResult<ORDENES_DE_PRODUCCION> GetORDENES_DE_PRODUCCION([FromODataUri] string key)
        {
            return SingleResult.Create(db.ORDENES_DE_PRODUCCION.Where(oRDENES_DE_PRODUCCION => oRDENES_DE_PRODUCCION.CODIGOORDENPRODUCCION == key));
        }

        // PUT: odata/ORDENES_DE_PRODUCCION(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<ORDENES_DE_PRODUCCION> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ORDENES_DE_PRODUCCION oRDENES_DE_PRODUCCION = db.ORDENES_DE_PRODUCCION.Find(key);
            if (oRDENES_DE_PRODUCCION == null)
            {
                return NotFound();
            }

            patch.Put(oRDENES_DE_PRODUCCION);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDENES_DE_PRODUCCIONExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(oRDENES_DE_PRODUCCION);
        }

        // POST: odata/ORDENES_DE_PRODUCCION
        public IHttpActionResult Post(ORDENES_DE_PRODUCCION oRDENES_DE_PRODUCCION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ORDENES_DE_PRODUCCION.Add(oRDENES_DE_PRODUCCION);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ORDENES_DE_PRODUCCIONExists(oRDENES_DE_PRODUCCION.CODIGOORDENPRODUCCION))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(oRDENES_DE_PRODUCCION);
        }

        // PATCH: odata/ORDENES_DE_PRODUCCION(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<ORDENES_DE_PRODUCCION> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ORDENES_DE_PRODUCCION oRDENES_DE_PRODUCCION = db.ORDENES_DE_PRODUCCION.Find(key);
            if (oRDENES_DE_PRODUCCION == null)
            {
                return NotFound();
            }

            patch.Patch(oRDENES_DE_PRODUCCION);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDENES_DE_PRODUCCIONExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(oRDENES_DE_PRODUCCION);
        }

        // DELETE: odata/ORDENES_DE_PRODUCCION(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            ORDENES_DE_PRODUCCION oRDENES_DE_PRODUCCION = db.ORDENES_DE_PRODUCCION.Find(key);
            if (oRDENES_DE_PRODUCCION == null)
            {
                return NotFound();
            }

            db.ORDENES_DE_PRODUCCION.Remove(oRDENES_DE_PRODUCCION);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/ORDENES_DE_PRODUCCION(5)/ORDENPRODUCCIONXPRODUCTO
        [EnableQuery]
        public IQueryable<ORDENPRODUCCIONXPRODUCTO> GetORDENPRODUCCIONXPRODUCTO([FromODataUri] string key)
        {
            return db.ORDENES_DE_PRODUCCION.Where(m => m.CODIGOORDENPRODUCCION == key).SelectMany(m => m.ORDENPRODUCCIONXPRODUCTO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ORDENES_DE_PRODUCCIONExists(string key)
        {
            return db.ORDENES_DE_PRODUCCION.Count(e => e.CODIGOORDENPRODUCCION == key) > 0;
        }
    }
}

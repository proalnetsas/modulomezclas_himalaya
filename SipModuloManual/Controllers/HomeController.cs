﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SipModuloManual.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/Home/Index.cshtml");
        }

        public ActionResult Home()
        {
            return View("~/Views/Home/Home.cshtml");
        }

        public ActionResult FormatoToc02()
        {
            return View("~/Views/SipModuloManual/FormatoToc02.cshtml");
        }

        public ActionResult EtiquetaPPR() {
            return View("~/Views/Reports/EtiquetaPPR.cshtml");
        }
        public ActionResult Formatoppr()
        {
            return View("~/Views/SipModuloManual/FormatoPpr.cshtml");
        }

        public ActionResult Formatoppr01() {
            return View("~/Views/SipModuloManual/FormatoPpr01.cshtml");
        }

        public ActionResult FormatopprMezclado()
        {
            return View("~/Views/SipModuloManual/FormatoPprMezclado.cshtml");
        }

        public ActionResult FormatoToc() {
            return View("~/Views/SipModuloManual/FormatoToc.cshtml");
        }

        public ActionResult FormatocPPRControl() {
            return View("~/Views/SipModuloManual/FormatoPprControl.cshtml");
        }


        public ActionResult ReportPpr027() {
            return View("~/Views/Reports/ReportePpr027.cshtml");
        }


        public ActionResult ReporteToc01() {
            return View("~/Views/Reports/ReporteToc01.cshtml");
        }


        public ActionResult ReportePpr019()
        {
            return View("~/Views/Reports/ReportePpr019.cshtml");
        }

        public ActionResult ReporteToc02()
        {
            return View("~/Views/Reports/ReporteToc02.cshtml");
        }


        public ActionResult ReportePpr01()
        {
            return View("~/Views/Reports/ReportePpr01.cshtml");
        }


        public ActionResult Registrousser()
        {
            return View("~/Views/SipModuloManual/RegistroUsuarios.cshtml");
        }

        public ActionResult AdminUsers()
        {
            return View("~/Views/SipModuloManual/AdminUsers.cshtml");
        }

        public ActionResult AdminPersonal()
        {
            return View("~/Views/SipModuloManual/AdminPersonal.cshtml");
        }

        public ActionResult CrearPersonal()
        {
            return View("~/Views/SipModuloManual/CrearPersonal.cshtml");
        }



    }
}

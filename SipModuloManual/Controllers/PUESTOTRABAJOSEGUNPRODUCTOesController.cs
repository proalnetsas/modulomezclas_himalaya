﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PUESTOTRABAJOSEGUNPRODUCTO>("PUESTOTRABAJOSEGUNPRODUCTOes");
    builder.EntitySet<MATERIALES>("MATERIALES"); 
    builder.EntitySet<PRODUCTOPORPROCESO>("PRODUCTOPORPROCESO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PUESTOTRABAJOSEGUNPRODUCTOesController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/PUESTOTRABAJOSEGUNPRODUCTOes
        [EnableQuery]
        public IQueryable<PUESTOTRABAJOSEGUNPRODUCTO> GetPUESTOTRABAJOSEGUNPRODUCTOes()
        {
            return db.PUESTOTRABAJOSEGUNPRODUCTO;
        }

        // GET: odata/PUESTOTRABAJOSEGUNPRODUCTOes(5)
        [EnableQuery]
        public SingleResult<PUESTOTRABAJOSEGUNPRODUCTO> GetPUESTOTRABAJOSEGUNPRODUCTO([FromODataUri] string key)
        {
            return SingleResult.Create(db.PUESTOTRABAJOSEGUNPRODUCTO.Where(pUESTOTRABAJOSEGUNPRODUCTO => pUESTOTRABAJOSEGUNPRODUCTO.CODIGOPRODUCTOS == key));
        }

        // PUT: odata/PUESTOTRABAJOSEGUNPRODUCTOes(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<PUESTOTRABAJOSEGUNPRODUCTO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PUESTOTRABAJOSEGUNPRODUCTO pUESTOTRABAJOSEGUNPRODUCTO = db.PUESTOTRABAJOSEGUNPRODUCTO.Find(key);
            if (pUESTOTRABAJOSEGUNPRODUCTO == null)
            {
                return NotFound();
            }

            patch.Put(pUESTOTRABAJOSEGUNPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PUESTOTRABAJOSEGUNPRODUCTOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pUESTOTRABAJOSEGUNPRODUCTO);
        }

        // POST: odata/PUESTOTRABAJOSEGUNPRODUCTOes
        public IHttpActionResult Post(PUESTOTRABAJOSEGUNPRODUCTO pUESTOTRABAJOSEGUNPRODUCTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PUESTOTRABAJOSEGUNPRODUCTO.Add(pUESTOTRABAJOSEGUNPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PUESTOTRABAJOSEGUNPRODUCTOExists(pUESTOTRABAJOSEGUNPRODUCTO.CODIGOPRODUCTOS))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(pUESTOTRABAJOSEGUNPRODUCTO);
        }

        // PATCH: odata/PUESTOTRABAJOSEGUNPRODUCTOes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<PUESTOTRABAJOSEGUNPRODUCTO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PUESTOTRABAJOSEGUNPRODUCTO pUESTOTRABAJOSEGUNPRODUCTO = db.PUESTOTRABAJOSEGUNPRODUCTO.Find(key);
            if (pUESTOTRABAJOSEGUNPRODUCTO == null)
            {
                return NotFound();
            }

            patch.Patch(pUESTOTRABAJOSEGUNPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PUESTOTRABAJOSEGUNPRODUCTOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pUESTOTRABAJOSEGUNPRODUCTO);
        }

        // DELETE: odata/PUESTOTRABAJOSEGUNPRODUCTOes(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            PUESTOTRABAJOSEGUNPRODUCTO pUESTOTRABAJOSEGUNPRODUCTO = db.PUESTOTRABAJOSEGUNPRODUCTO.Find(key);
            if (pUESTOTRABAJOSEGUNPRODUCTO == null)
            {
                return NotFound();
            }

            db.PUESTOTRABAJOSEGUNPRODUCTO.Remove(pUESTOTRABAJOSEGUNPRODUCTO);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/PUESTOTRABAJOSEGUNPRODUCTOes(5)/MATERIALES
        [EnableQuery]
        public SingleResult<MATERIALES> GetMATERIALES([FromODataUri] string key)
        {
            return SingleResult.Create(db.PUESTOTRABAJOSEGUNPRODUCTO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.MATERIALES));
        }

        // GET: odata/PUESTOTRABAJOSEGUNPRODUCTOes(5)/PRODUCTOPORPROCESO
        [EnableQuery]
        public SingleResult<PRODUCTOPORPROCESO> GetPRODUCTOPORPROCESO([FromODataUri] string key)
        {
            return SingleResult.Create(db.PUESTOTRABAJOSEGUNPRODUCTO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.PRODUCTOPORPROCESO));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PUESTOTRABAJOSEGUNPRODUCTOExists(string key)
        {
            return db.PUESTOTRABAJOSEGUNPRODUCTO.Count(e => e.CODIGOPRODUCTOS == key) > 0;
        }
    }
}

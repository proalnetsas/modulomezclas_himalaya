﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PARTESXPRODUCTOXPROCESO>("PARTESXPRODUCTOXPROCESOes");
    builder.EntitySet<PRODUCTOS>("PRODUCTOS"); 
    builder.EntitySet<PRODUCTOPORPROCESO>("PRODUCTOPORPROCESO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PARTESXPRODUCTOXPROCESOesController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/PARTESXPRODUCTOXPROCESOes
        [EnableQuery]
        public IQueryable<PARTESXPRODUCTOXPROCESO> GetPARTESXPRODUCTOXPROCESOes()
        {
            return db.PARTESXPRODUCTOXPROCESO;
        }

        // GET: odata/PARTESXPRODUCTOXPROCESOes(5)
        [EnableQuery]
        public SingleResult<PARTESXPRODUCTOXPROCESO> GetPARTESXPRODUCTOXPROCESO([FromODataUri] string key)
        {
            return SingleResult.Create(db.PARTESXPRODUCTOXPROCESO.Where(pARTESXPRODUCTOXPROCESO => pARTESXPRODUCTOXPROCESO.CODIGOPRODUCTOS == key));
        }

        // PUT: odata/PARTESXPRODUCTOXPROCESOes(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<PARTESXPRODUCTOXPROCESO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PARTESXPRODUCTOXPROCESO pARTESXPRODUCTOXPROCESO = db.PARTESXPRODUCTOXPROCESO.Find(key);
            if (pARTESXPRODUCTOXPROCESO == null)
            {
                return NotFound();
            }

            patch.Put(pARTESXPRODUCTOXPROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PARTESXPRODUCTOXPROCESOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pARTESXPRODUCTOXPROCESO);
        }

        // POST: odata/PARTESXPRODUCTOXPROCESOes
        public IHttpActionResult Post(PARTESXPRODUCTOXPROCESO pARTESXPRODUCTOXPROCESO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PARTESXPRODUCTOXPROCESO.Add(pARTESXPRODUCTOXPROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PARTESXPRODUCTOXPROCESOExists(pARTESXPRODUCTOXPROCESO.CODIGOPRODUCTOS))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(pARTESXPRODUCTOXPROCESO);
        }

        // PATCH: odata/PARTESXPRODUCTOXPROCESOes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<PARTESXPRODUCTOXPROCESO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PARTESXPRODUCTOXPROCESO pARTESXPRODUCTOXPROCESO = db.PARTESXPRODUCTOXPROCESO.Find(key);
            if (pARTESXPRODUCTOXPROCESO == null)
            {
                return NotFound();
            }

            patch.Patch(pARTESXPRODUCTOXPROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PARTESXPRODUCTOXPROCESOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pARTESXPRODUCTOXPROCESO);
        }

        // DELETE: odata/PARTESXPRODUCTOXPROCESOes(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            PARTESXPRODUCTOXPROCESO pARTESXPRODUCTOXPROCESO = db.PARTESXPRODUCTOXPROCESO.Find(key);
            if (pARTESXPRODUCTOXPROCESO == null)
            {
                return NotFound();
            }

            db.PARTESXPRODUCTOXPROCESO.Remove(pARTESXPRODUCTOXPROCESO);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/PARTESXPRODUCTOXPROCESOes(5)/PRODUCTOS
        [EnableQuery]
        public SingleResult<PRODUCTOS> GetPRODUCTOS([FromODataUri] string key)
        {
            return SingleResult.Create(db.PARTESXPRODUCTOXPROCESO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.PRODUCTOS));
        }

        // GET: odata/PARTESXPRODUCTOXPROCESOes(5)/PRODUCTOPORPROCESO
        [EnableQuery]
        public SingleResult<PRODUCTOPORPROCESO> GetPRODUCTOPORPROCESO([FromODataUri] string key)
        {
            return SingleResult.Create(db.PARTESXPRODUCTOXPROCESO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.PRODUCTOPORPROCESO));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PARTESXPRODUCTOXPROCESOExists(string key)
        {
            return db.PARTESXPRODUCTOXPROCESO.Count(e => e.CODIGOPRODUCTOS == key) > 0;
        }
    }
}

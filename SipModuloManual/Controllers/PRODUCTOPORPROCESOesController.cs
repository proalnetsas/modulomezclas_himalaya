﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PRODUCTOPORPROCESO>("PRODUCTOPORPROCESOes");
    builder.EntitySet<MATERIALESPORPRODUCTO>("MATERIALESPORPRODUCTO"); 
    builder.EntitySet<PARTESXPRODUCTOXPROCESO>("PARTESXPRODUCTOXPROCESO"); 
    builder.EntitySet<PROCESO>("PROCESO"); 
    builder.EntitySet<PRODUCTOS>("PRODUCTOS"); 
    builder.EntitySet<PUESTOTRABAJOSEGUNPRODUCTO>("PUESTOTRABAJOSEGUNPRODUCTO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PRODUCTOPORPROCESOesController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/PRODUCTOPORPROCESOes
        [EnableQuery]
        public IQueryable<PRODUCTOPORPROCESO> GetPRODUCTOPORPROCESOes()
        {
            return db.PRODUCTOPORPROCESO;
        }

        // GET: odata/PRODUCTOPORPROCESOes(5)
        [EnableQuery]
        public SingleResult<PRODUCTOPORPROCESO> GetPRODUCTOPORPROCESO([FromODataUri] string key)
        {
            return SingleResult.Create(db.PRODUCTOPORPROCESO.Where(pRODUCTOPORPROCESO => pRODUCTOPORPROCESO.CODIGOPRODUCTOS == key));
        }

        // PUT: odata/PRODUCTOPORPROCESOes(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<PRODUCTOPORPROCESO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PRODUCTOPORPROCESO pRODUCTOPORPROCESO = db.PRODUCTOPORPROCESO.Find(key);
            if (pRODUCTOPORPROCESO == null)
            {
                return NotFound();
            }

            patch.Put(pRODUCTOPORPROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCTOPORPROCESOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pRODUCTOPORPROCESO);
        }

        // POST: odata/PRODUCTOPORPROCESOes
        public IHttpActionResult Post(PRODUCTOPORPROCESO pRODUCTOPORPROCESO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PRODUCTOPORPROCESO.Add(pRODUCTOPORPROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PRODUCTOPORPROCESOExists(pRODUCTOPORPROCESO.CODIGOPRODUCTOS))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(pRODUCTOPORPROCESO);
        }

        // PATCH: odata/PRODUCTOPORPROCESOes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<PRODUCTOPORPROCESO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PRODUCTOPORPROCESO pRODUCTOPORPROCESO = db.PRODUCTOPORPROCESO.Find(key);
            if (pRODUCTOPORPROCESO == null)
            {
                return NotFound();
            }

            patch.Patch(pRODUCTOPORPROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCTOPORPROCESOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pRODUCTOPORPROCESO);
        }

        // DELETE: odata/PRODUCTOPORPROCESOes(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            PRODUCTOPORPROCESO pRODUCTOPORPROCESO = db.PRODUCTOPORPROCESO.Find(key);
            if (pRODUCTOPORPROCESO == null)
            {
                return NotFound();
            }

            db.PRODUCTOPORPROCESO.Remove(pRODUCTOPORPROCESO);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/PRODUCTOPORPROCESOes(5)/MATERIALESPORPRODUCTO
        [EnableQuery]
        public IQueryable<MATERIALESPORPRODUCTO> GetMATERIALESPORPRODUCTO([FromODataUri] string key)
        {
            return db.PRODUCTOPORPROCESO.Where(m => m.CODIGOPRODUCTOS == key).SelectMany(m => m.MATERIALESPORPRODUCTO);
        }

        // GET: odata/PRODUCTOPORPROCESOes(5)/PARTESXPRODUCTOXPROCESO
        [EnableQuery]
        public IQueryable<PARTESXPRODUCTOXPROCESO> GetPARTESXPRODUCTOXPROCESO([FromODataUri] string key)
        {
            return db.PRODUCTOPORPROCESO.Where(m => m.CODIGOPRODUCTOS == key).SelectMany(m => m.PARTESXPRODUCTOXPROCESO);
        }

        // GET: odata/PRODUCTOPORPROCESOes(5)/PROCESO
        [EnableQuery]
        public SingleResult<PROCESO> GetPROCESO([FromODataUri] string key)
        {
            return SingleResult.Create(db.PRODUCTOPORPROCESO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.PROCESO));
        }

        // GET: odata/PRODUCTOPORPROCESOes(5)/PRODUCTOS
        [EnableQuery]
        public SingleResult<PRODUCTOS> GetPRODUCTOS([FromODataUri] string key)
        {
            return SingleResult.Create(db.PRODUCTOPORPROCESO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.PRODUCTOS));
        }

        // GET: odata/PRODUCTOPORPROCESOes(5)/PUESTOTRABAJOSEGUNPRODUCTO
        [EnableQuery]
        public IQueryable<PUESTOTRABAJOSEGUNPRODUCTO> GetPUESTOTRABAJOSEGUNPRODUCTO([FromODataUri] string key)
        {
            return db.PRODUCTOPORPROCESO.Where(m => m.CODIGOPRODUCTOS == key).SelectMany(m => m.PUESTOTRABAJOSEGUNPRODUCTO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PRODUCTOPORPROCESOExists(string key)
        {
            return db.PRODUCTOPORPROCESO.Count(e => e.CODIGOPRODUCTOS == key) > 0;
        }
    }
}

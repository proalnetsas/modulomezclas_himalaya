﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PROCESO>("PROCESOes");
    builder.EntitySet<PRODUCTOPORPROCESO>("PRODUCTOPORPROCESO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PROCESOesController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/PROCESOes
        [EnableQuery]
        public IQueryable<PROCESO> GetPROCESOes()
        {
            return db.PROCESO;
        }

        // GET: odata/PROCESOes(5)
        [EnableQuery]
        public SingleResult<PROCESO> GetPROCESO([FromODataUri] string key)
        {
            return SingleResult.Create(db.PROCESO.Where(pROCESO => pROCESO.CODIGOPROCESO == key));
        }

        // PUT: odata/PROCESOes(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<PROCESO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PROCESO pROCESO = db.PROCESO.Find(key);
            if (pROCESO == null)
            {
                return NotFound();
            }

            patch.Put(pROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROCESOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pROCESO);
        }

        // POST: odata/PROCESOes
        public IHttpActionResult Post(PROCESO pROCESO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROCESO.Add(pROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PROCESOExists(pROCESO.CODIGOPROCESO))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(pROCESO);
        }

        // PATCH: odata/PROCESOes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<PROCESO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PROCESO pROCESO = db.PROCESO.Find(key);
            if (pROCESO == null)
            {
                return NotFound();
            }

            patch.Patch(pROCESO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROCESOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pROCESO);
        }

        // DELETE: odata/PROCESOes(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            PROCESO pROCESO = db.PROCESO.Find(key);
            if (pROCESO == null)
            {
                return NotFound();
            }

            db.PROCESO.Remove(pROCESO);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/PROCESOes(5)/PRODUCTOPORPROCESO
        [EnableQuery]
        public IQueryable<PRODUCTOPORPROCESO> GetPRODUCTOPORPROCESO([FromODataUri] string key)
        {
            return db.PROCESO.Where(m => m.CODIGOPROCESO == key).SelectMany(m => m.PRODUCTOPORPROCESO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROCESOExists(string key)
        {
            return db.PROCESO.Count(e => e.CODIGOPROCESO == key) > 0;
        }
    }
}

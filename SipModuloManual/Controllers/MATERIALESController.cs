﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;
using SipModuloManualModelo.Dto;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<MATERIALES>("MATERIALES");
    builder.EntitySet<MATERIALESPORPRODUCTO>("MATERIALESPORPRODUCTO"); 
    builder.EntitySet<PUESTOTRABAJOSEGUNPRODUCTO>("PUESTOTRABAJOSEGUNPRODUCTO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class MATERIALESController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/MATERIALES
        [EnableQuery]
        public IQueryable<MATERIALES> GetMATERIALES()
        {
            return db.MATERIALES;
        }

        // GET: odata/MATERIALES(5)
        [EnableQuery]
        public SingleResult<MATERIALES> GetMATERIALES([FromODataUri] string key)
        {
            return SingleResult.Create(db.MATERIALES.Where(mATERIALES => mATERIALES.CODIGOMATERIAL == key));
        }




        // PUT: odata/MATERIALES(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<MATERIALES> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MATERIALES mATERIALES = db.MATERIALES.Find(key);
            if (mATERIALES == null)
            {
                return NotFound();
            }

            patch.Put(mATERIALES);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MATERIALESExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(mATERIALES);
        }

        // POST: odata/MATERIALES
        public IHttpActionResult Post(MATERIALES mATERIALES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MATERIALES.Add(mATERIALES);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MATERIALESExists(mATERIALES.CODIGOMATERIAL))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(mATERIALES);
        }

        // PATCH: odata/MATERIALES(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<MATERIALES> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MATERIALES mATERIALES = db.MATERIALES.Find(key);
            if (mATERIALES == null)
            {
                return NotFound();
            }

            patch.Patch(mATERIALES);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MATERIALESExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(mATERIALES);
        }

        // DELETE: odata/MATERIALES(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            MATERIALES mATERIALES = db.MATERIALES.Find(key);
            if (mATERIALES == null)
            {
                return NotFound();
            }

            db.MATERIALES.Remove(mATERIALES);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/MATERIALES(5)/MATERIALESPORPRODUCTO
        [EnableQuery]
        public IQueryable<MATERIALESPORPRODUCTO> GetMATERIALESPORPRODUCTO([FromODataUri] string key)
        {
            return db.MATERIALES.Where(m => m.CODIGOMATERIAL == key).SelectMany(m => m.MATERIALESPORPRODUCTO);
        }

        // GET: odata/MATERIALES(5)/PUESTOTRABAJOSEGUNPRODUCTO
        [EnableQuery]
        public IQueryable<PUESTOTRABAJOSEGUNPRODUCTO> GetPUESTOTRABAJOSEGUNPRODUCTO([FromODataUri] string key)
        {
            return db.MATERIALES.Where(m => m.CODIGOMATERIAL == key).SelectMany(m => m.PUESTOTRABAJOSEGUNPRODUCTO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MATERIALESExists(string key)
        {
            return db.MATERIALES.Count(e => e.CODIGOMATERIAL == key) > 0;
        }
    }
}

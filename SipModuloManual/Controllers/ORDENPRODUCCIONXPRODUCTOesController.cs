﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;
using SipModuloManualModelo.Dto;
using System.Data.SqlClient;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ORDENPRODUCCIONXPRODUCTO>("ORDENPRODUCCIONXPRODUCTOes");
    builder.EntitySet<ORDENES_DE_PRODUCCION>("ORDENES_DE_PRODUCCION"); 
    builder.EntitySet<PRODUCTOS>("PRODUCTOS"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ORDENPRODUCCIONXPRODUCTOesController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/ORDENPRODUCCIONXPRODUCTOes
        [EnableQuery]
        public IQueryable<ORDENPRODUCCIONXPRODUCTO> GetORDENPRODUCCIONXPRODUCTOes()
        {
            return db.ORDENPRODUCCIONXPRODUCTO;
        }



        [HttpPost]
        public List<OrdenProduccionDTO> GetOrdenProduccion()
        {
            List<OrdenProduccionDTO> listaordenes = new List<OrdenProduccionDTO>();
            listaordenes = db.ORDENPRODUCCIONXPRODUCTO.Where(c => c.ORDENES_DE_PRODUCCION != null).Select(c => new OrdenProduccionDTO
            {
                CODIGOORDENPRODUCCION = c.CODIGOORDENPRODUCCION,
                CODIGOPRODUCTOS = c.CODIGOPRODUCTOS,
                CANTIDAD = c.CANTIDAD,
                NOMBREPRODUCTO = c.PRODUCTOS.NOMBREPRODUCTO
            }).ToList();


            return listaordenes;
        }

        [HttpPost]
        public string ConsultarProgramacionSemana()
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("SP_ConsultaProgramacionOrdenesDeProduccion", (SqlConnection)db.Database.Connection);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();

                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col].ToString());
                }
                rows.Add(row);
            }


            return serializer.Serialize(rows);

        }

        // GET: odata/ORDENPRODUCCIONXPRODUCTOes(5)
        [EnableQuery]
        public IQueryable<ORDENPRODUCCIONXPRODUCTO> GetORDENPRODUCCIONXPRODUCTO([FromODataUri] string key)
        {
            return db.ORDENPRODUCCIONXPRODUCTO.Where(oRDENPRODUCCIONXPRODUCTO => oRDENPRODUCCIONXPRODUCTO.CODIGOORDENPRODUCCION == key);
        }

        // PUT: odata/ORDENPRODUCCIONXPRODUCTOes(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<ORDENPRODUCCIONXPRODUCTO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ORDENPRODUCCIONXPRODUCTO oRDENPRODUCCIONXPRODUCTO = db.ORDENPRODUCCIONXPRODUCTO.Find(key);
            if (oRDENPRODUCCIONXPRODUCTO == null)
            {
                return NotFound();
            }

            patch.Put(oRDENPRODUCCIONXPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDENPRODUCCIONXPRODUCTOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(oRDENPRODUCCIONXPRODUCTO);
        }

        // POST: odata/ORDENPRODUCCIONXPRODUCTOes
        public IHttpActionResult Post(ORDENPRODUCCIONXPRODUCTO oRDENPRODUCCIONXPRODUCTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ORDENPRODUCCIONXPRODUCTO.Add(oRDENPRODUCCIONXPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ORDENPRODUCCIONXPRODUCTOExists(oRDENPRODUCCIONXPRODUCTO.CODIGOORDENPRODUCCION))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(oRDENPRODUCCIONXPRODUCTO);
        }

        // PATCH: odata/ORDENPRODUCCIONXPRODUCTOes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<ORDENPRODUCCIONXPRODUCTO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ORDENPRODUCCIONXPRODUCTO oRDENPRODUCCIONXPRODUCTO = db.ORDENPRODUCCIONXPRODUCTO.Find(key);
            if (oRDENPRODUCCIONXPRODUCTO == null)
            {
                return NotFound();
            }

            patch.Patch(oRDENPRODUCCIONXPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDENPRODUCCIONXPRODUCTOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(oRDENPRODUCCIONXPRODUCTO);
        }

        // DELETE: odata/ORDENPRODUCCIONXPRODUCTOes(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            ORDENPRODUCCIONXPRODUCTO oRDENPRODUCCIONXPRODUCTO = db.ORDENPRODUCCIONXPRODUCTO.Find(key);
            if (oRDENPRODUCCIONXPRODUCTO == null)
            {
                return NotFound();
            }

            db.ORDENPRODUCCIONXPRODUCTO.Remove(oRDENPRODUCCIONXPRODUCTO);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/ORDENPRODUCCIONXPRODUCTOes(5)/ORDENES_DE_PRODUCCION
        [EnableQuery]
        public SingleResult<ORDENES_DE_PRODUCCION> GetORDENES_DE_PRODUCCION([FromODataUri] string key)
        {
            return SingleResult.Create(db.ORDENPRODUCCIONXPRODUCTO.Where(m => m.CODIGOORDENPRODUCCION == key).Select(m => m.ORDENES_DE_PRODUCCION));
        }

        // GET: odata/ORDENPRODUCCIONXPRODUCTOes(5)/PRODUCTOS
        [EnableQuery]
        public SingleResult<PRODUCTOS> GetPRODUCTOS([FromODataUri] string key)
        {
            return SingleResult.Create(db.ORDENPRODUCCIONXPRODUCTO.Where(m => m.CODIGOORDENPRODUCCION == key).Select(m => m.PRODUCTOS));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ORDENPRODUCCIONXPRODUCTOExists(string key)
        {
            return db.ORDENPRODUCCIONXPRODUCTO.Count(e => e.CODIGOORDENPRODUCCION == key) > 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using SipModuloManualModelo.Model;
using SipModuloManualModelo.Dto;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using SipModuloManualModelo.General;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Globalization;
using SipModuloManual.Models.Security;
using System.Web.Configuration;
using InterfazHimalaya;

namespace SipModuloManual.Controllers
{
    public class FORMATOTOCsController : ODataController
    {
        private MATERIALESPORPRODUCTOesController materialesproducto = new MATERIALESPORPRODUCTOesController();
        private ContextEntities db = new ContextEntities();
        private String codBodega = WebConfigurationManager.AppSettings["Bodega"];

        public string UsuarioLogin { get; set; }

        // GET: odata/FORMATOTOCs
        [EnableQuery]
        public IQueryable<FORMATOTOC> GetFORMATOTOCs()
        {
            return db.FORMATOTOC;
        }

        // GET: odata/FORMATOTOCs(5)
        [EnableQuery]
        public SingleResult<FORMATOTOC> GetFORMATOTOC([FromODataUri] long key)
        {
            return SingleResult.Create(db.FORMATOTOC.Where(fORMATOTOC => fORMATOTOC.ID == key));
        }


        [HttpPost]
        public string ReporteToc01Orden(ODataActionParameters parameters)
        {

            string CodigoOrden = (string)parameters["CODIGOORDENPRODUCCION"];
            string CodigoProducto = (string)parameters["CODIGOPRODUCTO"];
            DateTime fechadesde = Convert.ToDateTime(parameters["FECHADESDE"]);
            DateTime fechahasta = Convert.ToDateTime(parameters["FECHAHASTA"]);

            if (CodigoProducto == "" && CodigoOrden != "")
            {

                var a = db.FORMATOTOC.Where(c=> c.CODIGOORDENPRODUCCION == CodigoOrden && ( c.FECHA>= fechadesde && c.FECHA<=fechahasta) ).OrderBy(c => c.CODIGOORDENPRODUCCION).Select(c => new {
                    c.CODIGOORDENPRODUCCION,
                    c.CODIGOPRODUCTOS,
                    c.PRODUCTOS.NOMBREPRODUCTO,

                }).Distinct().ToList();

                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(a);
                return json;
            }
            else if (CodigoProducto != "" && CodigoOrden == "")
            {

                var a = db.FORMATOTOC.Where(c => c.CODIGOPRODUCTOS == CodigoProducto && (c.FECHA >= fechadesde && c.FECHA <= fechahasta)).OrderBy(c => c.CODIGOORDENPRODUCCION).Select(c => new {
                    c.CODIGOORDENPRODUCCION,
                    c.CODIGOPRODUCTOS,
                    c.PRODUCTOS.NOMBREPRODUCTO,

                }).Distinct().ToList();

                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(a);
                return json;
            }
            else
            {

                var a = db.FORMATOTOC.Where( c =>c.FECHA >= fechadesde && c.FECHA <= fechahasta).OrderBy(c => c.CODIGOORDENPRODUCCION).Select(c => new {
                    c.CODIGOORDENPRODUCCION,
                    c.CODIGOPRODUCTOS,
                    c.PRODUCTOS.NOMBREPRODUCTO,

                }).Distinct().ToList();

                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(a);
                return json;
            }


        }

        [HttpPost]
        public string CrearReporteDinamycTOC(ODataActionParameters parameters)
        {

            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodPuesto = (string)parameters["CODIGOPUESTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];

            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("SP_DynamicReportTOC", (SqlConnection)db.Database.Connection);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CODIGOPRODUCTOS ", CodProducto);

            cmd.Parameters.AddWithValue("@CODIGOORDEN ", CodOrdenProduccion);

            cmd.Parameters.AddWithValue("@CODIGOPUESTO ", CodPuesto);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ToString().Contains("FECHA"))
                        row.Add(col.ColumnName, dr[col].ToString().Substring(0, 11));
                    else
                        row.Add(col.ColumnName, dr[col].ToString());
                }
                rows.Add(row);

            }

            return serializer.Serialize(rows);
            //return dt;
        }


        [HttpPost]
        public string CrearReporteDinamycTOCHimalaya(ODataActionParameters parameters)
        {

            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodPuesto = (string)parameters["CODIGOPUESTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            double TotalBatch = 0;

            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("SP_DynamicReportTOC", (SqlConnection)db.Database.Connection);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CODIGOPRODUCTOS ", CodProducto);

            cmd.Parameters.AddWithValue("@CODIGOORDEN ", CodOrdenProduccion);

            cmd.Parameters.AddWithValue("@CODIGOPUESTO ", CodPuesto);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            dt.Columns.Add("TOTALBATCH");
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ToString().Contains("FECHA"))
                    {
                        row.Add(col.ColumnName, dr[col].ToString().Substring(0, 11));
                    }
                    else if (col.ToString().Contains("NROOPERACION") || col.ToString().Contains("HORA") || col.ToString().Contains("ORDENPRODUCCIOBASE"))
                    {
                        row.Add(col.ColumnName, dr[col].ToString());
                    }
                    else if (col.ToString().Contains("TOTALBATCH"))
                    {
                        row.Add(col.ColumnName, Math.Round(TotalBatch, 3));

                    }
                    else
                    {
                        TotalBatch = TotalBatch + Convert.ToDouble(dr[col]);
                        row.Add(col.ColumnName, Math.Round(Convert.ToDouble(dr[col].ToString()),3));
                    }
                    
                }
                rows.Add(row);
                TotalBatch = 0;
            }

            return serializer.Serialize(rows);
            //return dt;
        }

        [HttpPost]
        public string CrearReporteHistoricoHimalaya(ODataActionParameters parameters)
        {

            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodPuesto = (string)parameters["CODIGOPUESTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            double CantidadProgramada = 0;
            double CantidadReceta = 0;
            double Kilogramos = 0;
            double CantidadPesada = 0;
            double PorcentajeCumplimiento = 0;

            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("SELECT * FROM vw_ReporteHistorico WHERE CODIGOORDENPRODUCCION = @CODIGOORDEN AND CODIGOMEZCLA = @CODIGOPRODUCTOS", (SqlConnection)db.Database.Connection);

            cmd.Parameters.AddWithValue("@CODIGOPRODUCTOS ", CodProducto);

            cmd.Parameters.AddWithValue("@CODIGOORDEN ", CodOrdenProduccion);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            dt.Columns.Add("CANTIDADPROGRAMADA");
            dt.Columns.Add("CUMPLIMIENTO");
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ToString().Contains("FECHA"))
                    {
                        row.Add(col.ColumnName, dr[col].ToString().Substring(0, 11));
                    }
                    else if (col.ToString() != "FECHA" && col.ToString() != "CUMPLIMIENTO" && col.ToString() != "KILOGRAMOS" && col.ToString() != "CANTIDADRECETA" && col.ToString() != "CANTIDADPROGRAMADA")
                    {
                        
                        
                        if (col.ToString().Contains("CANTIDADPESADA"))
                        {
                            CantidadPesada = Convert.ToDouble(dr[col]);
                            row.Add(col.ColumnName, Math.Round(CantidadPesada, 3).ToString());
                        }
                        else
                        {
                            row.Add(col.ColumnName, dr[col].ToString());
                        }

                    }
                    else if (col.ToString().Contains("CANTIDADRECETA"))
                    {
                        CantidadReceta = Convert.ToDouble(dr[col]);
                    }
                    else if (col.ToString().Contains("KILOGRAMOS"))
                    {
                        Kilogramos = Convert.ToDouble(dr[col]);
                    }
                    else if (col.ToString().Contains("CANTIDADPROGRAMADA"))
                    {
                        CantidadProgramada = Kilogramos * CantidadReceta;

                        row.Add(col.ColumnName, Math.Round(CantidadProgramada , 3).ToString());
                    }
                    else if (col.ToString().Contains("CUMPLIMIENTO"))
                    {
                        if(CantidadProgramada != 0)
                        {
                            PorcentajeCumplimiento = (CantidadPesada / CantidadProgramada) * 100;
                            PorcentajeCumplimiento = Math.Round(PorcentajeCumplimiento, 3);
                        }
                        
                        var porcentaje = PorcentajeCumplimiento.ToString() + " %";

                        row.Add(col.ColumnName, porcentaje) ;
                    }

                }
                rows.Add(row);
            }

            return serializer.Serialize(rows);
            //return dt;
        }
        
        [HttpPost]
        public string CrearReporteConsolidadoHimalaya(ODataActionParameters parameters)
        {

            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            string FechaDesde = (string)parameters["FECHADESDE"];
            string FechaHasta = (string)parameters["FECHAHASTA"];
            double CantidadProgramada = 0;
            double CumplimientoBatch = 0;
            double CantidadPesadaMezcla = 0;
            double CantidadPesadaBatch = 0;
            double CumplimientoMezcla = 0;

            FechaDesde = FechaDesde + " 00:00:00";
            FechaHasta = FechaHasta + " 23:59:59";

            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("SELECT * FROM vw_ReporteConsolidado WHERE CODIGOORDENPRODUCCION = @CODIGOORDEN AND CODIGOMEZCLA = @CODIGOPRODUCTOS AND FECHA BETWEEN @FECHADESDE AND @FECHAHASTA", (SqlConnection)db.Database.Connection);

            cmd.Parameters.AddWithValue("@CODIGOPRODUCTOS ", CodProducto);

            cmd.Parameters.AddWithValue("@CODIGOORDEN ", CodOrdenProduccion);

            cmd.Parameters.AddWithValue("@FECHADESDE", FechaDesde);
            cmd.Parameters.AddWithValue("@FECHAHASTA", FechaHasta);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            dt.Columns.Add("CUMPLIMIENTOBATCH");
            dt.Columns.Add("CUMPLIMIENTOMEZCLA");
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ToString().Contains("FECHA"))
                    {
                        row.Add(col.ColumnName, dr[col].ToString().Substring(0, 11));
                    }
                    else if (col.ToString() != "FECHA" && col.ToString() != "CUMPLIMIENTOBATCH" && col.ToString() != "CUMPLIMIENTOMEZCLA")
                    {

                        if (col.ToString().Contains("CANTIDADPESADABATCH"))
                        {
                            try
                            {
                                CantidadPesadaBatch = Convert.ToDouble(dr[col]);
                                row.Add(col.ColumnName, Math.Round(CantidadPesadaBatch, 3).ToString());
                            }
                            catch
                            {
                                CantidadPesadaBatch = 0;
                                row.Add(col.ColumnName, 0);
                            }
                        }
                        else if (col.ToString().Contains("CANTIDADPESADAMEZCLA"))
                        {
                            try
                            {
                                CantidadPesadaMezcla = Convert.ToDouble(dr[col]);
                                row.Add(col.ColumnName, Math.Round(CantidadPesadaMezcla, 3).ToString());
                            }
                            catch
                            {
                                CantidadPesadaMezcla = 0;
                                row.Add(col.ColumnName, 0);
                            }
                        }
                        else if (col.ToString().Contains("CANTIDADPROGRAMADA"))
                        {
                            try
                            {
                                CantidadProgramada = Convert.ToDouble(dr[col]);
                                row.Add(col.ColumnName, Math.Round(CantidadProgramada, 3).ToString());
                            }
                            catch
                            {
                                CantidadProgramada = 0;
                                row.Add(col.ColumnName, 0);
                            }
                        }
                        else
                        {
                            row.Add(col.ColumnName, dr[col].ToString());
                        }

                    }
                    else if (col.ToString().Contains("CUMPLIMIENTOBATCH"))
                    {
                        if (CantidadProgramada != 0)
                            CumplimientoBatch = Math.Round((CantidadPesadaBatch / CantidadProgramada) * 100, 3);

                        row.Add(col.ColumnName, CumplimientoBatch.ToString() + " %");
                    }
                    else if (col.ToString().Contains("CUMPLIMIENTOMEZCLA"))
                    {
                        if (CantidadProgramada != 0)
                            CumplimientoMezcla = Math.Round((CantidadPesadaMezcla / CantidadProgramada) * 100, 3);

                        row.Add(col.ColumnName, CumplimientoMezcla.ToString() + " %");
                    }

                }
                rows.Add(row);
            }

            return serializer.Serialize(rows);
        }

        [HttpPost]
        public int CantidadOperacionesHechas(ODataActionParameters parameters)
        {
            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodPuesto = (string)parameters["CODIGOPUESTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            int Cantidad = 0;

            try
            {
                Cantidad = db.FORMATOTOC.OrderByDescending(c => c.NROOPERACION).Where(c => c.CODIGOPRODUCTOS == CodProducto && c.CODIGOPUESTO == CodPuesto && c.CODIGOORDENPRODUCCION == CodOrdenProduccion).Select(c => c.NROOPERACION).FirstOrDefault();
            }
            catch {

            }
            return Cantidad;
        }

        [HttpPost]
        public double ConsultaPesoBatch(ODataActionParameters parameters)
        {
            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodPuesto = (string)parameters["CODIGOPUESTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            double PesoBatch = 0;

            try
            {
                PesoBatch = db.FORMATOTOC.OrderByDescending(c => c.NROOPERACION).Where(c => c.CODIGOPRODUCTOS == CodProducto && c.CODIGOPUESTO == CodPuesto && c.CODIGOORDENPRODUCCION == CodOrdenProduccion).Select(c => c.KILOGRAMOS).FirstOrDefault();
            }
            catch
            {

            }
            return Math.Round(PesoBatch, 3);
        }

        [HttpPost]
        public double ConsultarCantidadAPesar(ODataActionParameters parameters)
        {
            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodMaterial = (string)parameters["CODIGOMATERIAL"];
            double CantidadAPesar = 0;

            try
            {
                CantidadAPesar = Convert.ToDouble(db.MATERIALESPORPRODUCTO.Where(mp => mp.CODIGOPRODUCTOS == CodProducto && mp.CODIGOMATERIAL == CodMaterial).Select(mp => mp.CANTIDAD).FirstOrDefault());
            }
            catch
            {

            }
            return CantidadAPesar;
        }

        [HttpPost]
        public List<OrdenProduccionDTO> ConsultarDatosOPIniciarFabricacion(ODataActionParameters parameters)
        {
            string codOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];

            List<OrdenProduccionDTO> ListaOP = db.INT_SAP_CARGUE.Join(db.PRODUCTOS, s => s.CodigoItem, sa => sa.CODIGOPRODUCTOS,
                                               (s, sa) => new { INT_SAP_CARGUE = s, PRODUCTOS = sa })
                                               .Where(s_sa => s_sa.INT_SAP_CARGUE.NumOP == codOrdenProduccion)
                                               .Select(s_sa => new OrdenProduccionDTO
                                               {
                                                   CODIGOORDENPRODUCCION = s_sa.INT_SAP_CARGUE.NumOP,
                                                   CODIGOPRODUCTOS = s_sa.INT_SAP_CARGUE.CodigoItem,
                                                   NOMBREPRODUCTO = s_sa.PRODUCTOS.NOMBREPRODUCTO,
                                                   CANTIDAD = s_sa.INT_SAP_CARGUE.CantidadOP

                                               }).ToList();

            //List<OrdenProduccionDTO> ListaOP = db.ORDENPRODUCCIONXPRODUCTO.Join(db.PRODUCTOS, s => s.CODIGOPRODUCTOS, sa => sa.CODIGOPRODUCTOS,
            //                                   (s, sa) => new { ORDENPRODUCCIONXPRODUCTO = s, PRODUCTOS = sa })
            //                                   .Where(s_sa => s_sa.ORDENPRODUCCIONXPRODUCTO.CODIGOORDENPRODUCCION == codOrdenProduccion)
            //                                   .Select(s_sa => new OrdenProduccionDTO
            //                                   {
            //                                       CODIGOORDENPRODUCCION = s_sa.ORDENPRODUCCIONXPRODUCTO.CODIGOORDENPRODUCCION,
            //                                       CODIGOPRODUCTOS = s_sa.ORDENPRODUCCIONXPRODUCTO.CODIGOPRODUCTOS,
            //                                       NOMBREPRODUCTO = s_sa.PRODUCTOS.NOMBREPRODUCTO,
            //                                       CANTIDAD = s_sa.ORDENPRODUCCIONXPRODUCTO.CANTIDAD

            //                                   }).ToList();

            return ListaOP;
        }


        [HttpPost]
        public List<MateriaPrimaDTO> ConsultarDatosOperacion(ODataActionParameters parameters) {
            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodPuesto = (string)parameters["CODIGOPUESTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            int NroOperacion = (int)parameters["NROOPERACION"];

            List<MateriaPrimaDTO> ListaFormato = db.FORMATOTOC.Where(c => c.CODIGOORDENPRODUCCION == CodOrdenProduccion && c.CODIGOPRODUCTOS == CodProducto && c.CODIGOPUESTO == CodPuesto && c.NROOPERACION == NroOperacion)
                .Select(c=> new MateriaPrimaDTO
                {
                    CodigoMaterial = c.CODMATERIAL,
                    NroOperacion = c.NROOPERACION,
                    Cantidad = c.CANTIDAD,
                    Material = c.MATERIALES.NOMBREMATERIAL == null ? c.CODIGOPREMEZCLA : c.MATERIALES.NOMBREMATERIAL
                 })
                .ToList();


            return ListaFormato;
        }

        [HttpPost]
        public double CantidadPesadaOP(ODataActionParameters parameters)
        {
            try
            {
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];

                double CantidadPesada = db.FORMATOTOC.Where(x => x.CODIGOORDENPRODUCCION == CodOrdenProduccion).Select(x => x.CANTIDAD).Sum();

                return CantidadPesada;
            }
            catch(Exception ex)
            {
                return 0;
            }
           
        }

        [HttpPost]
        public void ActualizarOPFormatoTocSAP(ODataActionParameters parameters)
        {
            try
            {
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];

                List<FORMATOTOC> LstFormatoToc = new List<FORMATOTOC>();
                LstFormatoToc = db.FORMATOTOC.Where(x => x.CODIGOORDENPRODUCCION == CodOrdenProduccion).ToList();

                foreach(FORMATOTOC FormatoToc in LstFormatoToc)
                {
                    db.FORMATOTOC.Remove(FormatoToc);
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error en ActualizarOPFormatoTocSAP: " + ex);   
            }


        }

        [HttpPost]
        public List<INT_SAP_CARGUE> ConsultarRecetaOP(ODataActionParameters parameters)
        {
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];

            List<INT_SAP_CARGUE> LstSap = db.INT_SAP_CARGUE.Where(x => x.NumOP == CodOrdenProduccion).ToList();


            return LstSap;
        }

        [HttpPost]
        public List<MateriaPrimaDTO> ConsultarDatosOperacionHimalaya(ODataActionParameters parameters)
        {
            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            int NroOperacion = (int)parameters["NROOPERACION"];

            List<MateriaPrimaDTO> ListaFormato = db.FORMATOTOC.Where(c => c.CODIGOORDENPRODUCCION == CodOrdenProduccion && c.CODIGOPRODUCTOS == CodProducto && c.NROOPERACION == NroOperacion)
                .Select(c => new MateriaPrimaDTO
                {
                    CodigoMaterial = c.CODMATERIAL,
                    NroOperacion = c.NROOPERACION,
                    Cantidad = c.CANTIDAD,
                    Material = c.MATERIALES.NOMBREMATERIAL == null ? c.CODIGOPREMEZCLA : c.MATERIALES.NOMBREMATERIAL
                })
                .ToList();


            return ListaFormato;

        }

        [HttpPost]
        public string ConsultarOperacionBase(ODataActionParameters parameters)
        {
            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            string CodPuesto = (string)parameters["CODIGOPUESTO"];
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            string OperacionBase = "";

            try
            {
                OperacionBase = db.FORMATOTOC.OrderByDescending(x => x.ORDENPRODUCCIOBASE).Where(x => x.CODIGOPRODUCTOS == CodProducto && x.CODIGOPUESTO == CodPuesto && x.CODIGOORDENPRODUCCION == CodOrdenProduccion).Select(c => c.ORDENPRODUCCIOBASE).FirstOrDefault();
            }
            catch
            {

                
            }

            return OperacionBase;
        }


        [HttpPost]
        public bool ConsultarBatchCerrado(ODataActionParameters parameters)
        {
            var result = false;

            try
            {
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
                string CodProducto = (string)parameters["CODIGOPRODUCTO"];
                int NroOperacion = Convert.ToInt32((string)parameters["NROOPERACION"]);


                //FORMATOTOC.LAMINADAS => En este modulo se utiliza para verificar si un batch esta cerrado o no, se utilizo esta columna para no modificar la base de datos

                var batch = db.FORMATOTOC.Where(x => x.CODIGOORDENPRODUCCION == CodOrdenProduccion && x.CODIGOPRODUCTOS == CodProducto && x.NROOPERACION == NroOperacion).ToList().Select(x => x.LAMINADAS).FirstOrDefault();

                if(batch == 0 || batch == null)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Error al Consultar Batch: " + ex.Message);
            }




            return result;
        }


        [HttpPost]
        public void CrearOperacionTrabajoHimalaya(ODataActionParameters parameters)
        {
            try
            {
                string CodProducto = (string)parameters["CODIGOPRODUCTOS"];
                string Lote = (string)parameters["LOTE"];
                string CodPuesto = (string)parameters["CODIGOPUESTO"];
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
                string CodTurno = Generales.RetornaTurno();// (string)parameters["CODIGOTURNO"];
                string Login = (string)parameters["LOGIN"];
                int NroOperacion = (int)parameters["NROOPERACION"];
                string OrdenBase = (string)parameters["ORDENPRODUCCIOBASE"];
                double Kilogramos = (double)parameters["KILOGRAMOS"];
                TimeSpan Hora = TimeSpan.Parse(parameters["HORA"].ToString());
                int MinutosMezcla = (int)parameters["MINUTOSMEZCLA"];
                bool Silo = (bool)parameters["BASE_SILO"];
                int Laminadas = (int)parameters["LAMINADAS"];
                string CodMaterial = (string)parameters["CODMATERIAL"];
                double PesoMaterial = (double)parameters["CANTIDAD"];
                bool IngresoPesoManual = (bool)parameters["PESOMANUAL"];
                //Se usa este campo para decir si es un dato de sap
                string codigopremezcla = (string)parameters["CODIGOPREMEZCLA"];

                DateTime fecha;
                try
                {

                    DateTime.TryParse(parameters["FECHA"].ToString().Replace(".", ""), out fecha);
                }
                catch
                {
                    fecha = DateTime.Now;
                }

                if (fecha.Year == 0001)
                    fecha = DateTime.Now;

                FORMATOTOC createformatotoc = new FORMATOTOC();
                createformatotoc.LOTE = Lote;
                createformatotoc.FECHA = fecha;
                createformatotoc.CODIGOPUESTO = CodPuesto;
                createformatotoc.CODIGOORDENPRODUCCION = CodOrdenProduccion;
                createformatotoc.CODIGOTURNO = CodTurno;
                createformatotoc.LOGIN = Login; /*Generales.SesionTrabajo.CodigoUsuario;*/
                createformatotoc.NROOPERACION = NroOperacion;
                createformatotoc.ORDENPRODUCCIOBASE = OrdenBase;
                createformatotoc.KILOGRAMOS = Kilogramos;
                createformatotoc.HORA = Hora;
                createformatotoc.MINUTOSMEZCLA = MinutosMezcla;
                createformatotoc.BASE_SILO = Silo;
                createformatotoc.LAMINADAS = Laminadas;
                
                if (CodMaterial.Substring(0, 2) == "PM")
                {
                    createformatotoc.CODMATERIAL = null;
                    createformatotoc.CODIGOPREMEZCLA = CodMaterial;
                }
                else
                {
                    createformatotoc.CODMATERIAL = CodMaterial;
                    createformatotoc.CODIGOPREMEZCLA = null;
                }

                createformatotoc.CODIGOPREMEZCLA = codigopremezcla;
                createformatotoc.CANTIDAD = PesoMaterial;
                createformatotoc.PESOMANUAL = IngresoPesoManual;
                createformatotoc.CODIGOPRODUCTOS = CodProducto;

                int RegCount = db.FORMATOTOC.Where(x => x.CODIGOPRODUCTOS == CodProducto && x.CODMATERIAL == createformatotoc.CODMATERIAL && x.NROOPERACION == createformatotoc.NROOPERACION
                                    && x.CODIGOORDENPRODUCCION == createformatotoc.CODIGOORDENPRODUCCION).Count();

                string RegLote = db.FORMATOTOC.Where(x => x.CODIGOPRODUCTOS == CodProducto && x.CODMATERIAL == createformatotoc.CODMATERIAL && x.NROOPERACION == createformatotoc.NROOPERACION
                                    && x.CODIGOORDENPRODUCCION == createformatotoc.CODIGOORDENPRODUCCION).Select(y => y.LOTE).FirstOrDefault();


                if (RegCount == 1 && RegLote == string.Empty)
                {
                    FORMATOTOC update = (from FT in db.FORMATOTOC
                                         where FT.CODIGOPRODUCTOS == CodProducto && FT.CODMATERIAL == createformatotoc.CODMATERIAL
                                         && FT.NROOPERACION == createformatotoc.NROOPERACION && FT.CODIGOORDENPRODUCCION == createformatotoc.CODIGOORDENPRODUCCION
                                         select FT).FirstOrDefault();

                    update.LOTE = createformatotoc.LOTE;
                    update.KILOGRAMOS = createformatotoc.KILOGRAMOS;
                    update.CANTIDAD = createformatotoc.CANTIDAD;
                    update.LAMINADAS = createformatotoc.LAMINADAS;
                    update.CODIGOPREMEZCLA = codigopremezcla;
                    db.SaveChanges();

                }
                else if (RegCount >= 1 && RegLote != string.Empty)
                {

                    Post(createformatotoc);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error Creando Operacion " + ex.Message);
            }
        }

        [HttpPost]
        public void CerrarBatch(ODataActionParameters parameters)
        {
            try
            {
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
                string CodProducto = (string)parameters["CODIGOPRODUCTO"];
                int NroOperacion = Convert.ToInt32((string)parameters["NUMEROBATCH"]);


                //FORMATOTOC.LAMINADAS => En este modulo se utiliza para verificar si un batch esta cerrado o no, se utilizo esta columna para no modificar la base de datos

                db.FORMATOTOC.Where(x => x.CODIGOORDENPRODUCCION == CodOrdenProduccion && x.CODIGOPRODUCTOS == CodProducto && x.NROOPERACION == NroOperacion).ToList()
                    .ForEach(x => x.LAMINADAS = 1);

                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception("Error al Cerrar Batch: " + ex.Message);
            }


        }

        [HttpPost]
        public List<LoteDTO> CargarLotesSAP(ODataActionParameters parameters)
        {
            List<LoteDTO> lstLote = new List<LoteDTO>();
            try
            {   
                string CodItem = (string)parameters["CODIGOITEM"];
                Comunication comunictionsap = new Comunication();
                lstLote = comunictionsap.StartRead(CodItem, codBodega).Select(c=> new LoteDTO{
                    Cantidad = c.Qt,
                    Lote = c.Lote
                }).ToList();

                return lstLote;
            }
            catch (Exception ex)
            {
                return lstLote;
                //throw new Exception("Error al Cerrar Batch: " + ex.Message);
            }


        }


        [HttpPost]
        public List<NuevoBatchDTO> ConsultarInformacionNuevoBatch(ODataActionParameters parameters)
        {
            try
            {
                string CodProducto = (string)parameters["CODIGOPRODUCTO"];
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
                int NroOperacion = (int)parameters["NROOPERACION"];
                double PesoBatch =(double)parameters["PESOBATCH"];
                parameters.Add("CODIGOPUESTO", "9999");
                var cantidadOperaciones = CantidadOperacionesHechas(parameters);
                List<NuevoBatchDTO> ListaMateriales = new List<NuevoBatchDTO>();

                double SumCantidadRequerida = (from x in db.INT_SAP_CARGUE where x.NumOP == CodOrdenProduccion select x.CantidadReq).Sum();
                double PesoOp = (from x in db.INT_SAP_CARGUE where x.NumOP == CodOrdenProduccion select x.CantidadOP).FirstOrDefault();
                double PorcentajeOpBatch = ((PesoBatch * 100) / PesoOp) / 100;

                if (NroOperacion == 1 )
                {
                    ListaMateriales = (from MP in db.INT_SAP_CARGUE
                                       join M in db.MATERIALES on MP.CodigoItemDetalle equals M.CODIGOMATERIAL
                                       where MP.NumOP == CodOrdenProduccion
                                       select new NuevoBatchDTO
                                       {
                                           CodigoMaterial = MP.CodigoItemDetalle,
                                           NombreMaterial = M.NOMBREMATERIAL,
                                           CantidadAPesar = MP.CantidadReq * (MP.CantidadOP / (from ic in db.INT_SAP_CARGUE where ic.NumOP == MP.NumOP && ic.DocEntry == MP.DocEntry select ic.CantidadReq).Sum()), //* PesoBatch,
                                           CantidadPesada = 0
                                       }).GroupBy(n => n.CodigoMaterial).Select(g => g.FirstOrDefault()).ToList();

                    //ListaMateriales = (from MP in db.MATERIALESPORPRODUCTO
                    //                   join M in db.MATERIALES on MP.CODIGOMATERIAL equals M.CODIGOMATERIAL
                    //                   where MP.CODIGOPRODUCTOS == CodProducto
                    //                   select new NuevoBatchDTO
                    //                   {
                    //                       CodigoMaterial = MP.CODIGOMATERIAL,
                    //                       NombreMaterial = M.NOMBREMATERIAL,
                    //                       CantidadAPesar = (MP.CANTIDAD * PesoBatch),
                    //                       CantidadPesada = 0
                    //                   }).GroupBy(n => n.CodigoMaterial).Select(g => g.FirstOrDefault()).ToList();
                }
                else
                {
                    ListaMateriales = (from MP in db.INT_SAP_CARGUE
                                       join M in db.MATERIALES on MP.CodigoItemDetalle equals M.CODIGOMATERIAL
                                       join FT in db.FORMATOTOC on MP.CodigoItemDetalle equals FT.CODMATERIAL
                                       where MP.NumOP == CodOrdenProduccion && FT.CODIGOORDENPRODUCCION == CodOrdenProduccion
                                       select new NuevoBatchDTO
                                       {
                                           CodigoMaterial = MP.CodigoItemDetalle,
                                           NombreMaterial = M.NOMBREMATERIAL,
                                           CantidadAPesar = MP.CantidadReq * (MP.CantidadOP / (from ic in db.INT_SAP_CARGUE where ic.NumOP == MP.NumOP && ic.DocEntry == MP.DocEntry select ic.CantidadReq).Sum()), //* PesoBatch,
                                           CantidadPesada = 0
                                       }).GroupBy(n => n.CodigoMaterial).Select(g => g.FirstOrDefault()).ToList();


                    //ListaMateriales = (from MP in db.MATERIALESPORPRODUCTO
                    //                       join M in db.MATERIALES on MP.CODIGOMATERIAL equals M.CODIGOMATERIAL
                    //                       join FT in db.FORMATOTOC on MP.CODIGOMATERIAL equals FT.CODMATERIAL
                    //                       where MP.CODIGOPRODUCTOS == CodProducto && FT.CODIGOORDENPRODUCCION == CodOrdenProduccion
                    //                       select new NuevoBatchDTO
                    //                       {
                    //                           CodigoMaterial = MP.CODIGOMATERIAL,
                    //                           NombreMaterial = M.NOMBREMATERIAL,
                    //                           CantidadAPesar = (MP.CANTIDAD * PesoBatch),
                    //                           CantidadPesada = 0
                    //                       }).GroupBy(n => n.CodigoMaterial).Select(g => g.FirstOrDefault()).ToList();

                }

                foreach (var item in ListaMateriales)
                {
                    item.CantidadAPesar = item.CantidadAPesar * PorcentajeOpBatch;
                }


                if (NroOperacion > cantidadOperaciones)
                {
                    return ListaMateriales;
                }
                else
                {
                    var ListaMaterialesExistentes = (from MP in db.INT_SAP_CARGUE
                                                     join M in db.MATERIALES on MP.CodigoItemDetalle equals M.CODIGOMATERIAL
                                                     join FT in db.FORMATOTOC on MP.CodigoItemDetalle equals FT.CODMATERIAL
                                                     where MP.NumOP == CodOrdenProduccion && FT.CODIGOORDENPRODUCCION == CodOrdenProduccion && FT.NROOPERACION == NroOperacion
                                                     select new NuevoBatchDTO
                                                     {
                                                         CodigoMaterial = MP.CodigoItemDetalle,
                                                         NombreMaterial = M.NOMBREMATERIAL,
                                                         CantidadAPesar = MP.CantidadReq * (MP.CantidadOP / (from ic in db.INT_SAP_CARGUE where ic.NumOP == MP.NumOP && ic.DocEntry == MP.DocEntry select ic.CantidadReq).Sum()), //* PesoBatch,
                                                         CantidadPesada = FT.CANTIDAD
                                                     })
                                                    .GroupBy(x => x.CodigoMaterial).ToList();

                   
                    foreach (var item in ListaMaterialesExistentes)
                    {
                        ListaMateriales.First(x => x.CodigoMaterial == item.Key).CantidadPesada = item.Sum(x => x.CantidadPesada);
                    }

                    return ListaMateriales.ToList();
                }
            }
            catch
            {
                return null;
            }
        }

        [HttpPost]
        public void CrearOperacionTrabajo(ODataActionParameters parameters)
        {
            try
            {
                string CodProducto = (string)parameters["CODIGOPRODUCTOS"];
                string Lote = (string)parameters["LOTE"];
                string CodPuesto = (string)parameters["CODIGOPUESTO"];
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
                string CodTurno = Generales.RetornaTurno();// (string)parameters["CODIGOTURNO"];
                string Login = (string)parameters["LOGIN"];
                int NroOperacion = (int)parameters["NROOPERACION"];
                string OrdenBase = (string)parameters["ORDENPRODUCCIOBASE"];
                double Kilogramos = (double)parameters["KILOGRAMOS"];
                TimeSpan Hora = TimeSpan.Parse(parameters["HORA"].ToString());
                int MinutosMezcla = (int)parameters["MINUTOSMEZCLA"];
                bool Silo = (bool)parameters["BASE_SILO"];
                int Laminadas = (int)parameters["LAMINADAS"];
                string CodMaterial = (string)parameters["CODMATERIAL"];
                double PesoMaterial = (double)parameters["CANTIDAD"];
                bool IngresoPesoManual = (bool)parameters["PESOMANUAL"];
                DateTime fecha;
                try
                {
                    DateTime.TryParse(parameters["FECHA"].ToString().Replace(".", ""), out fecha);
                }
                catch
                {
                    fecha = DateTime.Now;
                }

                if (fecha.Year == 0001)
                    fecha = DateTime.Now;

                FORMATOTOC createformatotoc = new FORMATOTOC();
                createformatotoc.LOTE = Lote;
                createformatotoc.FECHA = fecha;
                createformatotoc.CODIGOPUESTO = CodPuesto;
                createformatotoc.CODIGOORDENPRODUCCION = CodOrdenProduccion;
                createformatotoc.CODIGOTURNO = CodTurno;
                createformatotoc.LOGIN = Generales.SesionTrabajo.CodigoUsuario; 
                createformatotoc.NROOPERACION = NroOperacion;
                createformatotoc.ORDENPRODUCCIOBASE = OrdenBase;
                createformatotoc.KILOGRAMOS = Kilogramos;
                createformatotoc.HORA = Hora;
                createformatotoc.MINUTOSMEZCLA = MinutosMezcla;
                createformatotoc.BASE_SILO = Silo;
                createformatotoc.LAMINADAS = Laminadas;
                if (CodMaterial.Substring(0, 2) == "PM")
                {
                    createformatotoc.CODMATERIAL = null;
                    createformatotoc.CODIGOPREMEZCLA = CodMaterial;
                }
                else
                {
                    createformatotoc.CODMATERIAL = CodMaterial;
                    createformatotoc.CODIGOPREMEZCLA = null;
                }
                createformatotoc.CANTIDAD = PesoMaterial;
                createformatotoc.PESOMANUAL = IngresoPesoManual;
                createformatotoc.CODIGOPRODUCTOS = CodProducto;

                Post(createformatotoc);


            }
            catch (Exception ex)
            {
                throw new Exception("Error Creando Operacion " + ex.Message);
            }
        }

        [HttpPost]
        public List<NuevoBatchDTO> CheckMezclaPesada(ODataActionParameters parameters)
        {
            string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
            string CodProducto = (string)parameters["CODIGOPRODUCTO"];
            List<NuevoBatchDTO> validar = new List<NuevoBatchDTO>();

            validar = (from toc in db.FORMATOTOC
                       where toc.LOTE == CodOrdenProduccion && toc.CODMATERIAL == "9999" && toc.CODIGOORDENPRODUCCION == CodOrdenProduccion && toc.CODIGOPRODUCTOS == CodProducto
                       select new NuevoBatchDTO
                       {
                           NombreMaterial = "TOTAL MEZCLA"
                       }).ToList();


            return validar;

        }

        [HttpPost]
        public List<OrdenProduccionDTO> ConsultarOrdenReporte(ODataActionParameters parameters)
        {
            List<OrdenProduccionDTO> OrdenesTurno = new List<OrdenProduccionDTO>();
           
            try
            {
                
                string TipoConsulta = (string)parameters["TIPOCONSULTA"];
                DateTime FechaInicio = new DateTime();
                DateTime.TryParse(parameters["FECHAINICIO"].ToString(), out FechaInicio);
                DateTime FechaFin = new DateTime();
                DateTime.TryParse(parameters["FECHAFIN"].ToString(), out FechaFin);
                
                    OrdenesTurno = (from op in db.FORMATOTOC
                                    where op.FECHA >= FechaInicio && op.FECHA <= FechaFin
                                    select new OrdenProduccionDTO
                                    {
                                        CODIGOORDENPRODUCCION = op.CODIGOORDENPRODUCCION

                                    }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw;
            }

            return OrdenesTurno;
        }

        [HttpPost]
        public List<ProductosDTO> ConsultarMezclaReporte(ODataActionParameters parameters)
        {
            List<ProductosDTO> Productos = new List<ProductosDTO>();
            try
            {

                string TipoConsulta = (string)parameters["TIPOCONSULTA"];
                DateTime FechaInicio = new DateTime();
                DateTime.TryParse(parameters["FECHAINICIO"].ToString(), out FechaInicio);
                DateTime FechaFin = new DateTime();
                DateTime.TryParse(parameters["FECHAFIN"].ToString(), out FechaFin);

                Productos =     (from op in db.FORMATOTOC
                                where op.FECHA >= FechaInicio && op.FECHA <= FechaFin
                                select new ProductosDTO
                                {
                                    CODIGOPRODUCTOS = op.CODIGOPRODUCTOS,

                                }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw;
            }

            return Productos;

        }

        [HttpPost]
        public void CrearNuevoBatch(ODataActionParameters parameters)
        {
            try
            {
                string CodProducto = (string)parameters["CODIGOPRODUCTO"];
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
                string NroOperacion = (string)parameters["NROOPERACION"];
                string Kilogramos = (string)parameters["PESOBATCH"];
                FORMATOTOC createformatotoc = new FORMATOTOC();
                DateTime Fecha = DateTime.Now;
                TimeSpan Hora = new TimeSpan(Fecha.Hour, Fecha.Minute, Fecha.Second);

                MATERIALESPORPRODUCTOesController MaterialesPorProducto = new MATERIALESPORPRODUCTOesController();
                List<INT_SAP_CARGUE> MatxProducto = MaterialesPorProducto.GetMatPorProducto(CodOrdenProduccion, CodProducto).ToList();
                //List<MATERIALESPORPRODUCTO> MatxProducto = MaterialesPorProducto.GetMATERIALESPORPRODUCTO(CodProducto).ToList();

                foreach (INT_SAP_CARGUE mat in MatxProducto)
                {
                    createformatotoc.LOTE = "";
                    createformatotoc.FECHA = Fecha;
                    createformatotoc.CODIGOPUESTO = "9999";
                    createformatotoc.CODIGOORDENPRODUCCION = CodOrdenProduccion;
                    createformatotoc.CODIGOTURNO = "1";
                    //createformatotoc.LOGIN = Generales.SesionTrabajo.CodigoUsuario;
                    createformatotoc.LOGIN = UsersSecurity.UserLogin;
                    createformatotoc.NROOPERACION = Convert.ToInt32(NroOperacion);
                    createformatotoc.ORDENPRODUCCIOBASE = CodOrdenProduccion;
                    createformatotoc.KILOGRAMOS = Convert.ToDouble(Kilogramos, CultureInfo.InvariantCulture);
                    createformatotoc.HORA = Hora;
                    createformatotoc.MINUTOSMEZCLA = 0;
                    createformatotoc.BASE_SILO = true;
                    createformatotoc.LAMINADAS = 0;

                    if (mat.CodigoItemDetalle.Substring(0, 2) == "PM")
                    {
                        createformatotoc.CODMATERIAL = null;
                        createformatotoc.CODIGOPREMEZCLA = mat.CodigoItemDetalle;
                    }
                    else
                    {
                        createformatotoc.CODMATERIAL = mat.CodigoItemDetalle;
                        createformatotoc.CODIGOPREMEZCLA = null;
                    }
                    //if (mat.CODIGOMATERIAL.Substring(0, 2) == "PM")
                    //{
                    //    createformatotoc.CODMATERIAL = null;
                    //    createformatotoc.CODIGOPREMEZCLA = mat.CODIGOMATERIAL;
                    //}
                    //else
                    //{
                    //    createformatotoc.CODMATERIAL = mat.CODIGOMATERIAL;
                    //    createformatotoc.CODIGOPREMEZCLA = null;
                    //}

                    createformatotoc.CANTIDAD = 0;
                    createformatotoc.PESOMANUAL = true;
                    createformatotoc.CODIGOPRODUCTOS = CodProducto;

                    Post(createformatotoc);
                   
                }

                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("Se creo correctamente el Batch #" + createformatotoc.NROOPERACION, EventLogEntryType.Information, 10, 1);
                }
            }
            catch(Exception ex)
            {

                throw new Exception("No se pudo crear el Batch: " + ex.StackTrace + " // Ex.Message: " + ex.Message);
                //using (EventLog eventLog = new EventLog("Application"))
                //{
                //    eventLog.Source = "Application";
                //    eventLog.WriteEntry("No se pudo crear el Batch: " + ex.StackTrace, EventLogEntryType.Error, 10, 1);
                //}
            }

        }

        [HttpPost]
        public List<string> ConsultarLimitesMezclas()
        {
            List<string> Parametros = new List<string>();
            try
            {
                string limiteInferior = (from param in db.PARAMETROS
                                         where param.NOMBRECAMPO == "TOLERANCIA_MEZCLAS_INFERIOR"
                                         select param.VALOR).FirstOrDefault();

                Parametros.Add(limiteInferior);


                string limiteSuperior = (from param in db.PARAMETROS
                                         where param.NOMBRECAMPO == "TOLERANCIA_MEZCLAS_SUPERIOR"
                                         select param.VALOR).FirstOrDefault();

                Parametros.Add(limiteSuperior);
            }
            catch (Exception ex)
            {
                throw;
            }

            return Parametros;

        }

        // PUT: odata/FORMATOTOCs(5)
        public IHttpActionResult Put([FromODataUri] long key, Delta<FORMATOTOC> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            FORMATOTOC fORMATOTOC = db.FORMATOTOC.Find(key);
            if (fORMATOTOC == null)
            {
                return NotFound();
            }

            patch.Put(fORMATOTOC);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FORMATOTOCExists(fORMATOTOC.NROOPERACION, fORMATOTOC.CODIGOORDENPRODUCCION, fORMATOTOC.CODIGOPRODUCTOS, fORMATOTOC.CODIGOPUESTO, fORMATOTOC.CODIGOTURNO, fORMATOTOC.LOGIN))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(fORMATOTOC);
        }

        // POST: odata/FORMATOTOCs
        public IHttpActionResult Post(FORMATOTOC fORMATOTOC)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FORMATOTOC.Add(fORMATOTOC);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (FORMATOTOCExists(fORMATOTOC.NROOPERACION, fORMATOTOC.CODIGOORDENPRODUCCION, fORMATOTOC.CODIGOPRODUCTOS, fORMATOTOC.CODIGOPUESTO, fORMATOTOC.CODIGOTURNO, fORMATOTOC.LOGIN))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(fORMATOTOC);
        }

        // PATCH: odata/FORMATOTOCs(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] long key, Delta<FORMATOTOC> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            FORMATOTOC fORMATOTOC = db.FORMATOTOC.Find(key);
            if (fORMATOTOC == null)
            {
                return NotFound();
            }

            patch.Patch(fORMATOTOC);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FORMATOTOCExists(fORMATOTOC.NROOPERACION, fORMATOTOC.CODIGOORDENPRODUCCION, fORMATOTOC.CODIGOPRODUCTOS, fORMATOTOC.CODIGOPUESTO, fORMATOTOC.CODIGOTURNO, fORMATOTOC.LOGIN))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(fORMATOTOC);
        }

        // DELETE: odata/FORMATOTOCs(5)
        public IHttpActionResult Delete([FromODataUri] long key)
        {
            FORMATOTOC fORMATOTOC = db.FORMATOTOC.Find(key);
            if (fORMATOTOC == null)
            {
                return NotFound();
            }

            db.FORMATOTOC.Remove(fORMATOTOC);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/FORMATOTOCs(5)/MATERIALES
        [EnableQuery]
        public SingleResult<MATERIALES> GetMATERIALES([FromODataUri] long key)
        {
            return SingleResult.Create(db.FORMATOTOC.Where(m => m.ID == key).Select(m => m.MATERIALES));
        }

        // GET: odata/FORMATOTOCs(5)/PRODUCTOS
        [EnableQuery]
        public SingleResult<PRODUCTOS> GetPRODUCTOS([FromODataUri] long key)
        {
            return SingleResult.Create(db.FORMATOTOC.Where(m => m.ID == key).Select(m => m.PRODUCTOS));
        }

        // GET: odata/FORMATOTOCs(5)/USUARIOS
        [EnableQuery]
        public SingleResult<USUARIOS> GetUSUARIOS([FromODataUri] long key)
        {
            return SingleResult.Create(db.FORMATOTOC.Where(m => m.ID == key).Select(m => m.USUARIOS));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FORMATOTOCExists(int nroopera, string codigoorden, string codigoproducto, string codigopuesto, string codigoturno, string login)
        {
            return db.FORMATOTOC.Count(e => e.NROOPERACION == nroopera && e.CODIGOORDENPRODUCCION== codigoorden && e.CODIGOPRODUCTOS == codigoproducto && e.CODIGOPUESTO == codigopuesto && e.CODIGOTURNO == codigoturno && e.LOGIN== login) > 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PRODUCTOS>("PRODUCTOS");
    builder.EntitySet<ORDENPRODUCCIONXPRODUCTO>("ORDENPRODUCCIONXPRODUCTO"); 
    builder.EntitySet<PARTESXPRODUCTOXPROCESO>("PARTESXPRODUCTOXPROCESO"); 
    builder.EntitySet<PRODUCTOPORPROCESO>("PRODUCTOPORPROCESO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PRODUCTOSController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/PRODUCTOS
        [EnableQuery]
        public IQueryable<PRODUCTOS> GetPRODUCTOS()
        {
            return db.PRODUCTOS.Where(pRODUCTOS => pRODUCTOS.ESTADOPRODUCTO == 1).OrderBy(c=> c.CODIGOPRODUCTOS);
        }

        // GET: odata/PRODUCTOS(5)
        [EnableQuery]
        public SingleResult<PRODUCTOS> GetPRODUCTOS([FromODataUri] string key)
        {
            return SingleResult.Create(db.PRODUCTOS.Where(pRODUCTOS => pRODUCTOS.CODIGOPRODUCTOS == key));
        }

        // PUT: odata/PRODUCTOS(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<PRODUCTOS> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PRODUCTOS pRODUCTOS = db.PRODUCTOS.Find(key);
            if (pRODUCTOS == null)
            {
                return NotFound();
            }

            patch.Put(pRODUCTOS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCTOSExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pRODUCTOS);
        }

        // POST: odata/PRODUCTOS
        public IHttpActionResult Post(PRODUCTOS pRODUCTOS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PRODUCTOS.Add(pRODUCTOS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PRODUCTOSExists(pRODUCTOS.CODIGOPRODUCTOS))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(pRODUCTOS);
        }

        // PATCH: odata/PRODUCTOS(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<PRODUCTOS> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PRODUCTOS pRODUCTOS = db.PRODUCTOS.Find(key);
            if (pRODUCTOS == null)
            {
                return NotFound();
            }

            patch.Patch(pRODUCTOS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCTOSExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pRODUCTOS);
        }

        // DELETE: odata/PRODUCTOS(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            PRODUCTOS pRODUCTOS = db.PRODUCTOS.Find(key);
            if (pRODUCTOS == null)
            {
                return NotFound();
            }

            db.PRODUCTOS.Remove(pRODUCTOS);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/PRODUCTOS(5)/ORDENPRODUCCIONXPRODUCTO
        [EnableQuery]
        public IQueryable<ORDENPRODUCCIONXPRODUCTO> GetORDENPRODUCCIONXPRODUCTO([FromODataUri] string key)
        {
            return db.PRODUCTOS.Where(m => m.CODIGOPRODUCTOS == key).SelectMany(m => m.ORDENPRODUCCIONXPRODUCTO);
        }

        // GET: odata/PRODUCTOS(5)/PARTESXPRODUCTOXPROCESO
        [EnableQuery]
        public IQueryable<PARTESXPRODUCTOXPROCESO> GetPARTESXPRODUCTOXPROCESO([FromODataUri] string key)
        {
            return db.PRODUCTOS.Where(m => m.CODIGOPRODUCTOS == key).SelectMany(m => m.PARTESXPRODUCTOXPROCESO);
        }

        // GET: odata/PRODUCTOS(5)/PRODUCTOPORPROCESO
        [EnableQuery]
        public IQueryable<PRODUCTOPORPROCESO> GetPRODUCTOPORPROCESO([FromODataUri] string key)
        {
            return db.PRODUCTOS.Where(m => m.CODIGOPRODUCTOS == key).SelectMany(m => m.PRODUCTOPORPROCESO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PRODUCTOSExists(string key)
        {
            return db.PRODUCTOS.Count(e => e.CODIGOPRODUCTOS == key) > 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using SipModuloManualModelo.Model;
using SipModuloManualModelo.Dto;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<MATERIALESPORPRODUCTO>("MATERIALESPORPRODUCTOes");
    builder.EntitySet<MATERIALES>("MATERIALES"); 
    builder.EntitySet<PRODUCTOPORPROCESO>("PRODUCTOPORPROCESO"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class MATERIALESPORPRODUCTOesController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/MATERIALESPORPRODUCTOes
        [EnableQuery]
        public IQueryable<MATERIALESPORPRODUCTO> GetMATERIALESPORPRODUCTOes()
        {
            return db.MATERIALESPORPRODUCTO;
        }

        // GET: odata/MATERIALESPORPRODUCTOes(5)
        [EnableQuery]
        public IQueryable<MATERIALESPORPRODUCTO> GetMATERIALESPORPRODUCTO([FromODataUri] string key)
        {
             //return SigleResult.Create(db.MATERIALESPORPRODUCTO.Where(mATERIALESPORPRODUCTO => mATERIALESPORPRODUCTO.CODIGOPRODUCTOS == key));
            return db.MATERIALESPORPRODUCTO.Where(m => m.CODIGOPRODUCTOS == key);
        }

        [EnableQuery]
        public IQueryable<INT_SAP_CARGUE> GetMatPorProducto([FromODataUri] string key, string prod)
        {
            return db.INT_SAP_CARGUE.Where(x => x.NumOP == key && x.CodigoItem == prod);
        }

        [HttpPost]
        public List<ProductosDTO> ListaProductos(ODataActionParameters parameters)
        {
            try
            {
                string CodOrden = (string)parameters["CODIGOORDENPRODUCCION"];

                string Sqlquery = "SELECT TOP 1 F.CODIGOPRODUCTOS, P.NOMBREPRODUCTO FROM FORMATOPPR F INNER JOIN PRODUCTOS P ON F.CODIGOPRODUCTOS=P.CODIGOPRODUCTOS WHERE CODIGOORDENPRODUCCION='"+ CodOrden +"'";

                var ListaProductos = db.Database.SqlQuery<ProductosDTO>(Sqlquery).ToList();


                if (ListaProductos.Count == 0)
                {
                    Sqlquery = "SELECT P.CODIGOPRODUCTOS, P.NOMBREPRODUCTO FROM PRODUCTOS P WHERE CODIGOPRODUCTOS NOT LIKE 'PM%'";
                    ListaProductos = db.Database.SqlQuery<ProductosDTO>(Sqlquery).ToList();
                }

                return ListaProductos;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            
            
        }

        [HttpPost]
        public List<ProductosDTO> ListaProductosYPm(ODataActionParameters parameters)
        {
            try
            {
                string CodOrden = (string)parameters["CODIGOORDENPRODUCCION"];

                string Sqlquery = "  SELECT TOP 1 F.CODIGOPRODUCTOS, P.NOMBREPRODUCTO FROM ORDENESDEPRODUCCIONAUTOMATICO F INNER JOIN PRODUCTOS P ON F.CODIGOPRODUCTOS=P.CODIGOPRODUCTOS WHERE CODIGOORDENPRODUCCION='" + CodOrden + "'";

                var ListaProductos = db.Database.SqlQuery<ProductosDTO>(Sqlquery).ToList();

                if (ListaProductos.Count == 0)
                {
                    Sqlquery = "SELECT TOP 1 F.CODIGOPRODUCTOS, P.NOMBREPRODUCTO FROM FORMATOPPR F INNER JOIN PRODUCTOS P ON F.CODIGOPRODUCTOS=P.CODIGOPRODUCTOS WHERE CODIGOORDENPRODUCCION='" + CodOrden + "'";
                    ListaProductos = db.Database.SqlQuery<ProductosDTO>(Sqlquery).ToList();
                }

                if (ListaProductos.Count == 0)
                {
                    Sqlquery = "SELECT P.CODIGOPRODUCTOS, P.NOMBREPRODUCTO FROM PRODUCTOS P";
                    ListaProductos = db.Database.SqlQuery<ProductosDTO>(Sqlquery).ToList();
                }

                return ListaProductos;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }


        }



        [HttpPost]
        public List<MaterialesDTO> ListaMaterialesProductos(ODataActionParameters parameters)
        {
            try
            {
                string CodProducto = (string)parameters["CODIGOPRODUCTO"];
                string CodOrden = (string)parameters["CODIGOORDENPRODUCCION"];
                //List<MaterialesDTO> ListaMateriales = db.MATERIALESPORPRODUCTO.Where(m => m.CODIGOPRODUCTOS == CodProducto).Select(c => new MaterialesDTO
                //{
                //    CODIGOMATERIAL = c.CODIGOMATERIAL,
                //    NOMBREPRODUCTO = c.PRODUCTOPORPROCESO.PRODUCTOS.NOMBREPRODUCTO,
                //    NOMBREMATERIAL = c.MATERIALES.NOMBREMATERIAL
                //}).ToList();

                string Sqlquery = "SELECT  MP.CODIGOMATERIAL, P.NOMBREPRODUCTO,  M.NOMBREMATERIAL,CASE WHEN F.IMPRESO IS NULL THEN 2 ELSE F.IMPRESO END AS IMPRESION, F.APROBADO,CASE WHEN F.IMPRESO IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS GUARDADO " +
                                                    "FROM MATERIALESPORPRODUCTO MP INNER JOIN PRODUCTOS P ON MP.CODIGOPRODUCTOS = P.CODIGOPRODUCTOS " +
                                                    "INNER JOIN MATERIALES M ON MP.CODIGOMATERIAL = M.CODIGOMATERIAL " +
                                                    "LEFT JOIN FORMATOPPR F ON F.CODIGOPRODUCTOS=MP.CODIGOPRODUCTOS AND F.CODIGOMATERIAL=MP.CODIGOMATERIAL AND F.CODIGOORDENPRODUCCION='"+ CodOrden +"' AND F.MOMENTO=1 "+
                                                    "WHERE MP.CODIGOPRODUCTOS='" + CodProducto + "'";

                var ListaMateriales = db.Database.SqlQuery<MaterialesDTO>(Sqlquery).ToList();


                return ListaMateriales;
            }
            catch (Exception Ex) {
                throw new Exception("Error Consultando Lista Materiales " + Ex.Message);
            }
        }


        [HttpPost]
        public List<MaterialesDTO> ListaMaterialesProductosMezclado(ODataActionParameters parameters)
        {
            try
            {
                string CodProducto = (string)parameters["CODIGOPRODUCTO"];
                string CodOrdenProduccion = (string)parameters["CODIGOORDENPRODUCCION"];
                int NroOperacion = (int)parameters["NROOPERACION"];
                //List<MaterialesDTO> ListaMateriales = db.MATERIALESPORPRODUCTO.Where(m => m.CODIGOPRODUCTOS == CodProducto).Select(c => new MaterialesDTO
                //{
                //    CODIGOMATERIAL = c.CODIGOMATERIAL,
                //    NOMBREPRODUCTO = c.PRODUCTOPORPROCESO.PRODUCTOS.NOMBREPRODUCTO,
                //    NOMBREMATERIAL = c.MATERIALES.NOMBREMATERIAL
                //}).ToList();

                string SqlQuery = "SELECT * FROM " +
                                  "(" +
                                  "SELECT MP.CODIGOMATERIAL, P.NOMBREPRODUCTO, M.NOMBREMATERIAL FROM MATERIALESPORPRODUCTO MP " +
                                  "INNER JOIN PRODUCTOS P ON MP.CODIGOPRODUCTOS = P.CODIGOPRODUCTOS " +
                                  "INNER JOIN MATERIALES M ON MP.CODIGOMATERIAL = M.CODIGOMATERIAL " +
                                  "WHERE MP.CODIGOPRODUCTOS = '"+ CodProducto + "' AND MP.CODIGOMATERIAL NOT IN (SELECT FTC.CODMATERIAL FROM FORMATOTOC FTC WHERE CODIGOPRODUCTOS = '" + CodProducto + "' AND NROOPERACION = '" + NroOperacion + "' AND CODIGOORDENPRODUCCION = '" + CodOrdenProduccion + "')" + 
                                  "AND MP.CODIGOMATERIAL NOT IN(SELECT MPP.CODIGOMATERIAL FROM PARTESXPRODUCTOXPROCESO PP "+
                                  "INNER JOIN MATERIALESPORPRODUCTO MPP ON  PP.CODIGOPRODUCTOASPARTE = MPP.CODIGOPRODUCTOS "+ 
                                  "WHERE PP.CODIGOPRODUCTOS = '"+ CodProducto + "') "+
                                  "UNION ALL " +
                                  "SELECT DISTINCT(PP.CODIGOPRODUCTOASPARTE) CODIGOPRODUCTOASPARTE, P.NOMBREPRODUCTO, P.NOMBREPRODUCTO AS NOMBREMATERIAL FROM PARTESXPRODUCTOXPROCESO PP " +
                                  "INNER JOIN PRODUCTOS P ON PP.CODIGOPRODUCTOASPARTE = P.CODIGOPRODUCTOS "+
                                  "INNER JOIN MATERIALESPORPRODUCTO MPP ON  PP.CODIGOPRODUCTOASPARTE = MPP.CODIGOPRODUCTOS "+ 
                                  "WHERE PP.CODIGOPRODUCTOS = '"+ CodProducto + "' GROUP BY CODIGOPRODUCTOASPARTE, NOMBREPRODUCTO " +
                                  ") AS TABLA  ";


                var ListaMateriales = db.Database.SqlQuery<MaterialesDTO>(SqlQuery).ToList();

                //if(ListaMateriales.Count == 0)
                //{
                //    NroOperacion = 0;

                //    SqlQuery = "SELECT * FROM " +
                //                  "(" +
                //                  "SELECT MP.CODIGOMATERIAL, P.NOMBREPRODUCTO, M.NOMBREMATERIAL FROM MATERIALESPORPRODUCTO MP " +
                //                  "INNER JOIN PRODUCTOS P ON MP.CODIGOPRODUCTOS = P.CODIGOPRODUCTOS " +
                //                  "INNER JOIN MATERIALES M ON MP.CODIGOMATERIAL = M.CODIGOMATERIAL " +
                //                  "WHERE MP.CODIGOPRODUCTOS = '" + CodProducto + "' AND MP.CODIGOMATERIAL NOT IN (SELECT FTC.CODMATERIAL FROM FORMATOTOC FTC WHERE CODIGOPRODUCTOS = '" + CodProducto + "' AND NROOPERACION = '" + NroOperacion + "') " +
                //                  "AND MP.CODIGOMATERIAL NOT IN(SELECT MPP.CODIGOMATERIAL FROM PARTESXPRODUCTOXPROCESO PP " +
                //                  "INNER JOIN MATERIALESPORPRODUCTO MPP ON  PP.CODIGOPRODUCTOASPARTE = MPP.CODIGOPRODUCTOS " +
                //                  "WHERE PP.CODIGOPRODUCTOS = '" + CodProducto + "') " +
                //                  "UNION ALL " +
                //                  "SELECT DISTINCT(PP.CODIGOPRODUCTOASPARTE) CODIGOPRODUCTOASPARTE, P.NOMBREPRODUCTO, P.NOMBREPRODUCTO AS NOMBREMATERIAL FROM PARTESXPRODUCTOXPROCESO PP " +
                //                  "INNER JOIN PRODUCTOS P ON PP.CODIGOPRODUCTOASPARTE = P.CODIGOPRODUCTOS " +
                //                  "INNER JOIN MATERIALESPORPRODUCTO MPP ON  PP.CODIGOPRODUCTOASPARTE = MPP.CODIGOPRODUCTOS " +
                //                  "WHERE PP.CODIGOPRODUCTOS = '" + CodProducto + "' GROUP BY CODIGOPRODUCTOASPARTE, NOMBREPRODUCTO " +
                //                  ") AS TABLA  ";

                //    ListaMateriales = db.Database.SqlQuery<MaterialesDTO>(SqlQuery).ToList();
                //}


                return ListaMateriales;

            }
            catch (Exception Ex)
            {
                throw new Exception("Error Consultando Lista Materiales " + Ex.Message);
            }
        }

        [HttpPost]
        public List<MaterialesDTO> ListaMaterialesProductosMezclado2(ODataActionParameters parameters)
         {
            try
            {
                string CodProducto = (string)parameters["CODIGOPRODUCTO"];
                string CodOrden = (string)parameters["CODIGOORDENPRODUCCION"];


                //List<MaterialesDTO> ListaMateriales = (from ep in db.FORMATOPPR
                //                                       join e in db.MATERIALES on ep.CODIGOMATERIAL equals e.CODIGOMATERIAL
                //                                       join t in db.MATERIALESPORPRODUCTO on new { e.CODIGOMATERIAL, ep.CODIGOPRODUCTOS } equals new { t.CODIGOMATERIAL, t.CODIGOPRODUCTOS }
                //                                       where ep.CODIGOORDENPRODUCCION == CodOrden && ep.MOMENTO == 1 && ep.CODIGOPRODUCTOS == CodProducto
                //                                       select new MaterialesDTO
                //                                       {
                //                                           CODIGOMATERIAL = ep.CODIGOMATERIAL,
                //                                           NOMBREMATERIAL = e.NOMBREMATERIAL,
                //                                           NOMBREPRODUCTO = t.PRODUCTOPORPROCESO.PRODUCTOS.NOMBREPRODUCTO,
                //                                           PROCESADO = (db.FORMATOPPR.Where(c => c.CODIGOMATERIAL == ep.CODIGOMATERIAL && c.CODIGOORDENPRODUCCION == CodOrden && c.MOMENTO == 2 && c.CODIGOPRODUCTOS == CodProducto).Count() > 0),
                //                                           DEVUELTO = (db.FORMATOPPR.Where(c => c.CODIGOMATERIAL == ep.CODIGOMATERIAL && c.CODIGOORDENPRODUCCION == CodOrden && c.MOMENTO == 3 && c.CODIGOPRODUCTOS == CodProducto).Count() > 0),
                //                                           APROBADO = (db.FORMATOPPR.Where(c => c.CODIGOMATERIAL == ep.CODIGOMATERIAL && c.CODIGOORDENPRODUCCION == CodOrden && c.MOMENTO == 1 && c.APROBADO == true && c.CODIGOPRODUCTOS == CodProducto).Count() > 0)
                //                                       }).ToList();



                string Sqlquery = "SELECT MP.CODIGOMATERIAL, P.NOMBREPRODUCTO,  M.NOMBREMATERIAL,F.APROBADO  AS APROBADO, 1 AS IMPRESION ," +
                                  " CASE WHEN(SELECT TOP 1 ID FROM FORMATOPPR WHERE CODIGOORDENPRODUCCION = '"+ CodOrden + "' AND MOMENTO = 2 AND CODIGOMATERIAL = MP.CODIGOMATERIAL) IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS PROCESADO," +
                                  " CASE WHEN (SELECT TOP 1 ID FROM FORMATOPPR WHERE CODIGOORDENPRODUCCION = '"+ CodOrden + "' AND MOMENTO = 3 AND CODIGOMATERIAL = MP.CODIGOMATERIAL ) IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS DEVUELTO" +
                                  " FROM MATERIALESPORPRODUCTO MP INNER JOIN PRODUCTOS P ON MP.CODIGOPRODUCTOS = P.CODIGOPRODUCTOS" +
                                  " INNER JOIN MATERIALES M ON MP.CODIGOMATERIAL = M.CODIGOMATERIAL" +
                                  " LEFT JOIN FORMATOPPR F ON F.CODIGOPRODUCTOS = MP.CODIGOPRODUCTOS AND F.CODIGOMATERIAL = MP.CODIGOMATERIAL AND F.CODIGOORDENPRODUCCION = '"+ CodOrden + "' AND F.MOMENTO = 1" +
                                  " WHERE MP.CODIGOPRODUCTOS = '"+ CodProducto + "' AND MOMENTO = 1" +
                                  " UNION ALL" +
                                   " SELECT MP.CODIGOPRODUCTOASPARTE, P.NOMBREPRODUCTO,  M.NOMBREPRODUCTO,CAST(1 AS BIT) AS APROBADO, 1 AS IMPRESION, " +
                                  " CASE WHEN(SELECT TOP 1 ID FROM FORMATOPPR WHERE CODIGOORDENPRODUCCION = '" + CodOrden + "' AND MOMENTO = 2 AND CODIGOPREMEZCLA = MP.CODIGOPRODUCTOASPARTE) IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS PROCESADO," +
                                  " CAST(1 AS BIT) AS DEVUELTO FROM PARTESXPRODUCTOXPROCESO MP INNER JOIN PRODUCTOS P ON MP.CODIGOPRODUCTOS = P.CODIGOPRODUCTOS" +
                                  " INNER JOIN PRODUCTOS M ON MP.CODIGOPRODUCTOASPARTE = M.CODIGOPRODUCTOS" +
                                  " LEFT JOIN FORMATOPPR F ON F.CODIGOPRODUCTOS = MP.CODIGOPRODUCTOS AND F.CODIGOMATERIAL = MP.CODIGOPRODUCTOASPARTE AND F.CODIGOORDENPRODUCCION = '" + CodOrden + "'" +
                                  " WHERE MP.CODIGOPRODUCTOS = '"+ CodProducto +"'";
                


                var ListaMateriales = db.Database.SqlQuery<MaterialesDTO>(Sqlquery).ToList();

                //List<MaterialesDTO> ListaMateriales = db.MATERIALESPORPRODUCTO.Where(m => m.CODIGOPRODUCTOS == CodProducto).Select(c => new MaterialesDTO
                //{
                //    CODIGOMATERIAL = c.CODIGOMATERIAL,
                //    NOMBREPRODUCTO = c.PRODUCTOPORPROCESO.PRODUCTOS.NOMBREPRODUCTO,
                //    NOMBREMATERIAL = c.MATERIALES.NOMBREMATERIAL
                //}).ToList();

                return ListaMateriales;

            }
            catch (Exception Ex)
            {
                throw new Exception("Error Consultando Lista Materiales " + Ex.Message);
            }
        }

        // PUT: odata/MATERIALESPORPRODUCTOes(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<MATERIALESPORPRODUCTO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MATERIALESPORPRODUCTO mATERIALESPORPRODUCTO = db.MATERIALESPORPRODUCTO.Find(key);
            if (mATERIALESPORPRODUCTO == null)
            {
                return NotFound();
            }

            patch.Put(mATERIALESPORPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MATERIALESPORPRODUCTOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(mATERIALESPORPRODUCTO);
        }

        // POST: odata/MATERIALESPORPRODUCTOes
        public IHttpActionResult Post(MATERIALESPORPRODUCTO mATERIALESPORPRODUCTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MATERIALESPORPRODUCTO.Add(mATERIALESPORPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MATERIALESPORPRODUCTOExists(mATERIALESPORPRODUCTO.CODIGOPRODUCTOS))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(mATERIALESPORPRODUCTO);
        }

        // PATCH: odata/MATERIALESPORPRODUCTOes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<MATERIALESPORPRODUCTO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MATERIALESPORPRODUCTO mATERIALESPORPRODUCTO = db.MATERIALESPORPRODUCTO.Find(key);
            if (mATERIALESPORPRODUCTO == null)
            {
                return NotFound();
            }

            patch.Patch(mATERIALESPORPRODUCTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MATERIALESPORPRODUCTOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(mATERIALESPORPRODUCTO);
        }

        // DELETE: odata/MATERIALESPORPRODUCTOes(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            MATERIALESPORPRODUCTO mATERIALESPORPRODUCTO = db.MATERIALESPORPRODUCTO.Find(key);
            if (mATERIALESPORPRODUCTO == null)
            {
                return NotFound();
            }

            db.MATERIALESPORPRODUCTO.Remove(mATERIALESPORPRODUCTO);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/MATERIALESPORPRODUCTOes(5)/MATERIALES
        [EnableQuery]
        public SingleResult<MATERIALES> GetMATERIALES([FromODataUri] string key)
        {
            return SingleResult.Create(db.MATERIALESPORPRODUCTO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.MATERIALES));
        }

        // GET: odata/MATERIALESPORPRODUCTOes(5)/PRODUCTOPORPROCESO
        [EnableQuery]
        public SingleResult<PRODUCTOPORPROCESO> GetPRODUCTOPORPROCESO([FromODataUri] string key)
        {
            return SingleResult.Create(db.MATERIALESPORPRODUCTO.Where(m => m.CODIGOPRODUCTOS == key).Select(m => m.PRODUCTOPORPROCESO));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MATERIALESPORPRODUCTOExists(string key)
        {
            return db.MATERIALESPORPRODUCTO.Count(e => e.CODIGOPRODUCTOS == key) > 0;
        }
    }
}

﻿$(document).ready(function () {

    ConfigurarPantallaInicial();
    ConfigurarPopupNuevoTOC();
    //ConfigurarPopup($("#PopupNuevoTOC"), 650, 850, "Ingreso Materiales captura información", true);
    ConfigurarPopup($("#PopupNuevoBatch"), 600, 900, "Batch actual", true);
    ConfigurarPopup($("#PopupNuevoPPR019"), 300, 600, "Despeje Linea ", true);
    ConfigurarPopup($("#PopupNuevoPesos"), 380, 250, "Ingrese Los Valores de La Muestra", true);
    ConfigurarPopup($("#PopupNuevoAtrib"), 380, 250, "Ingrese Los Valores de Atributos", true);
    ConfigurarPopup($("#PopupNuevoCondicion"), 460, 750, "Ingrese El Valor de Las Condiciones", true);
    ConfigurarPopup($("#PopupAlmacenarLote"), 340, 700, "Ingrese El Lote del material", true);
    ConfigurarPopup($("#PopupEditar"), 700, 600, "Editar Usuario", true);
    ConfigurarPopup($("#PopupProgramacionSemana"), 500, 700, "Programación Semana", true);
    ConfigurarPopup($("#PopupRecetaOrdenProduccion"), 500, 700, "Receta Orden Producción", true);
    LlenarComboProductos($("#ListaProductoActual"), '');
    LlenarComboProductos($("#ListaProductoAnterior"), '');

    $("#sidemenu").addClass("left-side sidebar-offcanvas collapse-left");
});

function RecalcularPesoNeto()
{
    var bruto = $("#PesoBruto").val();

    if ($("#Tara").val() != "" && bruto != "")
    {
        var tara = $("#Tara").val();
        var neto = parseFloat(bruto).toFixed(3) - parseFloat(tara);
        $("#PesoNeto").val(neto.toFixed(3));

    }
}

function RecalcularPesoBruto() {

    var neto = $("#PesoNeto").val();

    if ($("#Tara").val() != "" && neto != "") {
        var tara = $("#Tara").val();
        var bruto = parseFloat(neto) + parseFloat(tara);
        $("#PesoBruto").val(bruto);

    }
}

function cambiarcheck(opcion) {
    $("[name=Status][value=" + opcion + "]").iCheck('check');
}

function SelecionarMaterial()
{
    debugger;
    if($("#CodMaterial").val() != "")
    {
        $("#ListaMateriales").data("kendoDropDownList").value($("#CodMaterial").val());
    }

    //if ($("#ListaMateriales").val() == "")
    //{
    //    $("#ErrorCrearToc").html("Este Material No Pertenece A La Receta del Producto: " + $("#Producto").val());
    //}
}



function CargarProductoPPR019() {
    var ordenProduccion = $("#OrdenProduccion").val();

    request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs('" + ordenProduccion + "')", function (data)
    {
        
        $("#LoteProducto").val(data.LOTEPRODUCTO);
        $("#ListaProductoActual").data("kendoDropDownList").value(data.CODIGOPRODUCTOS);
    });
}

function ChangeTitle(e, ventana) {
    
    //get window object
    var kendoWindow = e.container.data("kendoWindow");

    if (e.model.isNew()) {
        //set options using the setOptions method
        kendoWindow.setOptions({
            title: "Ingresar " + ventana
        });
    } else {
        //set options using the setOptions method
        kendoWindow.setOptions({
            title: "Editar " + ventana
        });
    }
}

function ImprimirFormato()
{
    window.open(window.location.origin + '/Home/EtiquetaPPR', 'popup', 'width=450,height=300');
}

function LimpiarDatosPPR() {


    var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd-MMM-yyyy');
    $("#FechaPesaje").data("kendoDatePicker").value(todayDate);
    $("#FechaEntrega").data("kendoDatePicker").value(todayDate);
    $("#FechaRecibido").data("kendoDatePicker").value(todayDate);
    $("#Observaciones").val("");
   
    $("#LoteMaterial").val("");
    $("#NAnalisis").val("");
    $("#NRecipiente").val("");
    $("#Consecutivo").val(0);
    $("#LoteProducto").val("");

    $("#Tara").val(0);
    $("#PesoBruto").val(0);
    $("#PesoNeto").val(0);

    $("#OperarioEntrega").val("");
    $("#OperarioRecibe").val("");
}


//Función para hacer ajaxrequest
function request(contenttype, tokentype, token, type, url, sucess, error, data) {
    $.ajax({
        beforeSend: function (xhrObj) {
            xhrObj.setRequestHeader("Content-Type", contenttype);
            if (tokentype)
                xhrObj.setRequestHeader("Authorization", tokentype + " " + token);
        },
        type: type,
        url: url,
        data: data,
        dataType: "json",
        success: sucess,
        error: error
    });
}


//Función para controlar error de llamados ajax
function errorHandler(jqXHR, exception) {
    debugger;
    var txt = '';
    //if (jqXHR.status === 0)
    //{
    //    txt = 'No se puede encontrar la ruta especificada.';
    //} else if (jqXHR.status == 404) {
    //    txt = 'La página solicitada no se encuentra. [404]';
    //} else if (jqXHR.status == 500) {
    //    txt = 'Error interno del servidor. [500]';
    //} else if (exception === 'parsererror') {
    //    txt = 'Requested JSON parse failed.';
    //} else if (exception === 'timeout') {
    //    txt = 'Error de tiempo de espera agotado';
    //} else if (exception === 'abort') {
    //    txt = 'Ajax request aborted.';
    //}
    //else
    //{
    if (jqXHR.responseJSON)
    {
            if (jqXHR.responseJSON["odata.error"]) {
                var error = '';
                if (jqXHR.responseJSON["odata.error"].message) {
                    error = jqXHR.responseJSON["odata.error"].message.value;
                }
                if (jqXHR.responseJSON["odata.error"].innererror) {
                    error = error + " " + jqXHR.responseJSON["odata.error"].innererror.message;
                }
                if (error != "")
                    txt = error;
                else
                    txt = 'Error no controlado.\n' + jqXHR.responseText;
            }
            else
                txt = 'Error no controlado.\n' + jqXHR.responseText;
    }
    else
        txt = 'Error no controlado.\n' + jqXHR.responseText;
    //}

    showModal("Sistema Cadivi", txt, "red");
}

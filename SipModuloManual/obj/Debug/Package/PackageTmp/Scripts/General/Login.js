﻿var baseUrl = "../odata/";
var baseUrlPage = window.location.origin;

function login()
{


    var username = $('#txUserName').val();
    var password = $('#txPassword').val();


    var filterLogin =
    {
        USUARIO: username.toUpperCase(),
        PASSWORD: password
    }

    var msjError = '';
    if (username === '')
        msjError = '<p>Debe ingresar un nombre de usuario</p>';

    if (password === '')
        msjError += '<p>Debe ingresar una contraseña</p>';

    if (msjError === '')
    {
    }
    else 
    {
        showModal("Sistema Cadivi",msjError, 'red');
        //$('#btnIngresar').removeAttr('enabled');
        //espera.TerminarEspera();
    }


    request("application/json", null, null, "POST", baseUrl + "USUARIOS/Login", function (data) {
        if (data.value == true) {
            window.localStorage['usuario'] = username.toUpperCase();
            window.location.href = baseUrlPage + "/Home/Home";

        }
        else
        {
            showModal("Sistema Cadivi", "Error de inicio usuario o contraseña incorrecto", 'red'); 
        }

    }, errorHandler, JSON.stringify(filterLogin))

}



//Función para hacer ajaxrequest
function request(contenttype, tokentype, token, type, url, sucess, error, data) {
    $.ajax({
        beforeSend: function (xhrObj) {
            xhrObj.setRequestHeader("Content-Type", contenttype);
            if (tokentype)
                xhrObj.setRequestHeader("Authorization", tokentype + " " + token);
        },
        type: type,
        url: url,
        data: data,
        dataType: "json",
        success: sucess,
        error: error
    });
}



//Función para controlar error de llamados ajax
function errorHandler(jqXHR, exception)
{
    debugger;
    var txt = '';
    if (jqXHR.status === 0) {
        txt = 'No se puede encontrar la ruta especificada.';
    } else if (jqXHR.status == 404) {
        txt = 'La página solicitada no se encuentra. [404]';
    } else if (jqXHR.status == 500) {
        txt = 'Error interno del servidor. [500]';
    } else if (exception === 'parsererror') {
        txt = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        txt = 'Error de tiempo de espera agotado';
    } else if (exception === 'abort') {
        txt = 'Ajax request aborted.';
    }
    else {
        if (jqXHR.responseJSON) {
            if (jqXHR.responseJSON["odata.error"]) {
                var error = '';
                if (jqXHR.responseJSON["odata.error"].message) {
                    error = jqXHR.responseJSON["odata.error"].message.value;
                }
                if (jqXHR.responseJSON["odata.error"].innererror) {
                    error = error + " " + jqXHR.responseJSON["odata.error"].innererror.message;
                }
                if (error != "")
                    txt = error;
                else
                    txt = 'Error no controlado.\n' + jqXHR.responseText;
            }
            else
                txt = 'Error no controlado.\n' + jqXHR.responseText;
        }
        else
            txt = 'Error no controlado.\n' + jqXHR.responseText;
    }

    showModal("Sistema Cadivi", txt, 'red');
    
}

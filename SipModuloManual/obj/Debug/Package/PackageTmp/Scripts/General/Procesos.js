﻿var lotesap;
$(document).ready(function () {

    var a = 0;
    

});


function iniciarProcesoMezclado(e)
{
    $("#divaprobar").hide();
    $("#divguardar").show();


    cambiarcheck(2);
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");

    $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
    $("#codMaterial").val(dataItem.CODIGOMATERIAL);
    $("#codProducto").val($("#codigoProducto").val());
    $("#Producto").val(dataItem.NOMBREPRODUCTO);


    if (dataItem.PROCESADO)
    {
        consultaPPREnBodega(dataItem);
    }
    else
    {
        LimpiarDatosPPR();
        LlamarPopupPPR($("#PopupNuevoPPR27"));
        request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/MostrarUsuario", function (data) {

            if (dataItem.NOMBREMATERIAL.includes("Pre-Mezc-") == false)
            { 
                consultaPPRLoteYNroAnalisis(dataItem);
            }


            $("#OperarioEntrega").val(data.Nombre);
            $("#idOperarioEntrega").val(data.CodigoUsuario);
        }, errorHandler, null)

    }

    

  

    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}


function iniciarDevolucion(e) {

    $("#divaprobar").hide();
    $("#divguardar").show();

    cambiarcheck(3);
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));


    if (dataItem.NOMBREMATERIAL.includes("Pre-Mezc-") == true)
    {
        showModal("Sistema Cadivi", "<h2>Una Pre-Mezcla No Puede Ingresar Formato de Devolucion</h2>", 'red');
    }
    else
    {

      
    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");

        $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
        $("#codMaterial").val(dataItem.CODIGOMATERIAL);
        $("#codProducto").val($("#codigoProducto").val());
        $("#Producto").val(dataItem.NOMBREPRODUCTO);


        if (dataItem.DEVUELTO)
        {
       
                consultaPPREnBodega(dataItem);
       
        
        }
        else
        {
            LimpiarDatosPPR();
            LlamarPopupPPR($("#PopupNuevoPPR27"));


            request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/MostrarUsuario", function (data)
            {
                consultaPPRLoteYNroAnalisis(dataItem);
                $("#OperarioEntrega").val(data.Nombre);
                $("#idOperarioEntrega").val(data.CodigoUsuario);
            }, errorHandler, null)
}
    }
    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}

function iniciarEntrega(e)
{
   
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
   
    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");

    $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
    $("#codMaterial").val(dataItem.CODIGOMATERIAL);
    
    $("#Producto").val(dataItem.NOMBREPRODUCTO);
    

    

        
    if (dataItem.APROBADO == false || dataItem.APROBADO == null)
    {
        if (dataItem.GUARDADO == true)
        {
            PararComunicacion();
            consultaPPREnBodega(dataItem);
        }
        else
        {

            ReactivarComunicacion();
            LimpiarDatosPPR();
            LlamarPopupPPR($("#PopupNuevoPPR27"));

            request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/MostrarUsuario", function (data) {

                $("#OperarioEntrega").val(data.Nombre);
                $("#idOperarioEntrega").val(data.CodigoUsuario);
            }, errorHandler, null)

        }
    }
    else
        showModal("Sistema Cadivi", "No se puede modificar un Material ya Aprobado por Mezclador", "red");
    
    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}


function iniciarActualizacion(e)
{
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    $("#codFormato").val(dataItem.ID);
    $("#Pregunta").val(dataItem.PREGUNTA);

    $("#Respuesta").val(dataItem.RESPUESTA);
    $("#Aplica").val(dataItem.APLICA);

    $("#Observaciones").val(dataItem.OBSERVACIONES);

    
    LlamarPopupPPR($("#PopupNuevoPPR019"));

    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}

function iniciarFabricacion()
{
    var f = new Date();
    
    
    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora= f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();


    $("#Hora").val(hora + ":" + min);
    $("#FechaIngreso").val(f.getDate() + "-" + parseInt(f.getMonth() + 1) + "-" + f.getFullYear());

    $("#ErrorEditar").html("");

    if($("#OrdenProduccion").val()==""){
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion Para Iniciar Fabricacion", "red");
        return;
    }

    if($("#CodProducto").val()==""){
        showModal("Sistema Cadivi", "Ingrese El Codigo de Producto Para Iniciar Fabricacion", "red");
        return;
    }

    if ($("#LoteProducto").val() == "" || $("#LoteProducto").val() == "0")
    {
        showModal("Sistema Cadivi", "Ingrese Un Numero de Lote Valido Para Iniciar Fabricacion", "red");
        return;
    }


    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }

    $("#PesoKg").val("200");

    LlenarGridToc01(filter);
    consultarOperacionActual(filter);
    consultarOperacionBase(filter);

}

function iniciarOperacionToc()
{
    $("#ErrorCrearOperacion").html("");
    
    if ($("#OpBase").val() == "") {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion de La Base Para Crear una Operacion", "red");
        return;
    }

    if ($("#PesoKg").val() == "") {
        showModal("Sistema Cadivi", "Ingrese El Peso En Kilogramos De La Base Para Crear una Operacion", "red");
        return;
    }

    if ($("#Hora").val() == "") {
        showModal("Sistema Cadivi", "Ingrese La Hora De Adición Para Crear una Operacion", "red");
        return;
    }

    if ($("#MinutosMezcla").val() == "") {
        showModal("Sistema Cadivi", "Ingrese Los Minutos De Mezcla Para Crear una Operacion", "red");
        return;
    }


    

    
    var filterProducto =
        {
            CODIGOPRODUCTO: $("#CodProducto").val()
        }


    LlenarCombo($("#ListaMateriales"), filterProducto);
    LlamarPopupPPR($("#PopupNuevoTOC"));
    llenargridMateriaPrima();
}

function iniciarToc02() {
    $("#FechaIngreso").val(new Date().toJSON().slice(0, 10));


    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi","Error Iniciando Proceso: Ingrese El Numero de Orden de Produccion Para Iniciar El Proceso", 'red');
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Cadivi" ,"Error Iniciando Proceso: Ingrese El Codigo Del Producto", 'red');
        return;
    }

    if ($("#LoteProducto").val() == "") {
        showModal("Sistema Cadivi","Error Iniciando Proceso: Ingrese El Numero de Lote", 'red');
        return;
    }

    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        LOTE: $("#LoteProducto").val(),
        LOGIN: window.localStorage['usuario'],
        ID: 0,
        FECHA: $("#FechaIngreso").val(),
        OBSERVACIONES: "",
        PASTARECHAZADAFIN: 0,
        PASTARECHAZADAINICIO:0,
        PASTASPRODUCIDAS: 0,
        PORCENTAJERECHAZADO: 0,
        CUMPLEGALGAS: false
    }

    $("#btnEditar").css("visibility", "hidden");
    LlenarGridControlProceso(filter);
    showModal("Sistema Cadivi", "Datos Cargados Con Exito", 'blue');
   
}

function continuarPpr01()
{
    $("#FechaIngreso").val(new Date().toJSON().slice(0, 10));

    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Orden de Produccion Para Iniciar El Proceso", 'red');
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Codigo Del Producto", 'red');
        return;
    }

    if ($("#LoteProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Lote", 'red');
        return;
    }

    if ($("#Presentacion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Presentacion", 'red');
        return;
    }


    if ($("#PesoMin").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Valor del Peso Minimo", 'red');
        return;
    }



    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        LOTE: $("#LoteProducto").val(),
        LOGIN: window.localStorage['usuario'],
        ID: 1,
        FECHA: $("#FechaIngreso").val(),
        PRESENTACION: parseInt( $("#Presentacion").val()),
        PESOMINIMO: parseInt($("#PesoMin").val()),
    }


    LlenarGridControlPpr01(filter);
    showModal("Sistema Cadivi", "Datos Cargados Con Exito", 'blue');

}

function iniciarPpr01() {
    $("#FechaIngreso").val(new Date().toJSON().slice(0, 10));

    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Orden de Produccion Para Iniciar El Proceso", 'red');
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Codigo Del Producto", 'red');
        return;
    }

    if ($("#LoteProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Lote", 'red');
        return;
    }

    if ($("#Presentacion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Presentacion", 'red');
        return;
    }


    if ($("#PesoMin").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Valor del Peso Minimo", 'red');
        return;
    }



    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        LOTE: $("#LoteProducto").val(),
        LOGIN: window.localStorage['usuario'],
        ID: 0,
        FECHA: $("#FechaIngreso").val(),
        PRESENTACION: parseInt($("#Presentacion").val()),
        PESOMINIMO: parseInt($("#PesoMin").val()),
    }


    LlenarGridControlPpr01(filter);
    showModal("Sistema Cadivi", "Datos Cargados Con Exito", 'blue');

}



function iniciarFabricacionHimalaya() {

    var f = new Date();


    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora = f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();

    $("#Hora").val(hora + ":" + min);

    $("#FechaIngreso").val(f.getDate() + "-" + parseInt(f.getMonth() + 1) + "-" + f.getFullYear());

    $("#ErrorEditar").html("");

    $("#PesoBatch").val("");

    $("#TotalProducido").val(0);

    document.getElementById("TotalAProducir").value = (parseFloat(document.getElementById("TotalProduccion").value) - (document.getElementById("TotalProducido").value))

    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Mezclas", "Ingrese El Numero de Orden de Producción Para Iniciar Fabricación", "red");
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Mezclas", "Ingrese El Codigo de Producto Para Iniciar Fabricación", "red");
        return;
    }

    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: "9999",
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }

    LlenarGridCapturaHimalaya(filter);
    consultarOperacionActualHimalaya(filter);
}

function ContinuarBatch() {
    
    if (a != parseFloat($("#PesoBatch").val())) {
        showModal("Sistema Mezclas A.H", "El peso del batch no debe cambiar a mitad de una operación, por favor termine la operación actual.<br /> Peso Batch esperado " + a + "", "red");
        return;
    }
    iniciarOperacionTocHimalaya();

}

function iniciarOperacionTocHimalaya() {
    
    let pesoBatch = $("#PesoBatch").val();

    if ($("#NumeroBatch").val() == "0") {
        showModal("Mezclas A.H", "No se ha creado ningun batch, por favor cree un batch", "red");
        return;
    }

    pesoBatch = commaToPoint(pesoBatch);

    if ($("#PesoBatch").val() == "" || $("#PesoBatch").val() == "0" || parseFloat(pesoBatch) < 0) {
        showModal("Sistema Mezclas", "Por Favor valide el peso del Batch", 'red');
        return;
    }

    $("#ErrorCrearOperacion").html("");

    var filterProducto =
    {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        NROOPERACION: $("#NumeroBatch").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    };

    var filter =
    {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        NROOPERACION: $("#NumeroBatch").val(),
        PESOBATCH: parseFloat(pesoBatch)
    };

    
    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultarBatchCerrado", function (data) {
        
        if (!data.value) {
            showModal("Mezclas A.H", "El Batch ya se ha cerrado, Por favor cree un nuevo Batch para continuar con la operacion", "red");
            return;
        }
        else {
            
            LlamarPopupPPR($("#PopupNuevoBatch"));

            if (pesoTerminado || batchNuevo || pesoBulto) {

                $("#CantidadAPesar").val("0.00");
                CerrarPopup($("#PopupNuevoTOC"));
            }
            else {

                LlamarPopupPPR($("#PopupNuevoTOC"));
            }

            llenarGridNuevoBatch(filter);
        }
    }, errorHandler, JSON.stringify(filterProducto))

}

function CalcularLimite() {

    debugger;

    var cantidadAPesar = parseFloat($("#CantidadAPesar").val());
    var pesoTiempoReal = parseFloat($("#Peso").val());
    var cantidadPesada = parseFloat($("#CantidadPesada").val());
    var tiempoRealPesada = (pesoTiempoReal + cantidadPesada).toFixed(3);

    let LimiteInferior = parseFloat((cantidadAPesar - LimiteInferiorMezclas).toFixed(3));
    let LimiteSuperior = parseFloat((cantidadAPesar + LimiteSuperiorMezclas).toFixed(3));

    if (parseFloat(tiempoRealPesada) == cantidadAPesar) {
        document.getElementById("Peso").style.backgroundColor = "Green";
        //$("#btnGuardarFormato").attr("disabled", false);
    }
    else if ((parseFloat(tiempoRealPesada) > LimiteSuperior) || (parseFloat(tiempoRealPesada) < LimiteInferior)) {

        document.getElementById("Peso").style.backgroundColor = "Red";
        //$("#btnGuardarFormato").attr("disabled", true);
    }
    else if ((parseFloat(tiempoRealPesada) >= LimiteInferior) && (parseFloat(tiempoRealPesada) <= LimiteSuperior)) {

        document.getElementById("Peso").style.backgroundColor = "Yellow";
        //$("#btnGuardarFormato").attr("disabled", false);
    }
}

function ConsultarCantidadAPesar(filter) {
   
    var cantidadAPesar = 0;
    

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultarCantidadAPesar", function (data) {

        if (data != null) {
            cantidadAPesar = (data.value);
            $("#CantidadAPesar").val((cantidadAPesar * a).toFixed(3));
        }



    }, errorHandler, JSON.stringify(filter))


}

function ConfirmCerrarBatch() {

    var grid = $("#gridNuevoBatch").data("kendoGrid");

    for (var i = 0; i < grid._data.length; i++) {

        if (grid._data[i].CantidadPesada == 0)
        {
            showModal("Mezclas A.H", "Por favor termine de pesar todos los materiales antes de cerrar el batch", "red");
            return;
        }

    }

    showConfirm('¿Está Seguro?', '<p><h2>¿Esta seguro que desea Cerrar el Batch actual?</h2></p>', function (e) {
        e.sender.close(e.sender.element);
        CerrarBatch();
    }, function () {
        e.sender.close();
    });
}

function CerrarBatch() {

    var filter =
    {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        NUMEROBATCH: $("#NumeroBatch").val()
       
    }
    

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CerrarBatch", function (data) {
        showModal("Mezclas A.H", "Datos Guardados Con Exito!", "green");
        CerrarPopup($("#PopupNuevoBatch"));
        iniciarFabricacionHimalaya();
    }, errorHandler, JSON.stringify(filter))
}



function CargarLotesSAP(codigoItem, cantidadpeso) {

    var filter =
    {
        CODIGOITEM: codigoItem // '16550008' //$("#CodProducto").val(),
    }

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CargarLotesSAP", function (data) {

        let total = 0;
        
        if (data.value.length > 0) {
            $('#selectLote').empty();
            var lotes = data.value;
            
            

            for (var i = 0; i < lotes.length; i++) {
                
                if (parseFloat(lotes[i].Cantidad) >= parseFloat(cantidadpeso)) {
                    total = total + 1;
                    $('#selectLote').append('<option value="' + lotes[i].Cantidad + '">' + lotes[i].Lote + '</option>');
                }
            }

            $('#selectLote').append('<option value="Manual">Ingreso Manual</option>')

            if (total == 0) {
                $("#selectLote").val("Manual").change();
            }
            else
                $("#selectLote").val(lotes[lotes.length - 1].Cantidad).change();
                



        }
        else {
            $('#selectLote').empty();
            $('#selectLote').append('<option value="Manual">Ingreso Manual</option>')
            $("#selectLote").val("Manual").change();
        }
            

        
        
    }, errorHandler, JSON.stringify(filter))
}

function getVal(sel)
{
    
    if (sel.value != "Manual") {
        $("#Lote").attr('disabled', true);
        //$("#divlotemanual").css({ "visibility": "hidden" });
        if (sel.value != "")
        {
            $("#LblCantidadLoteDisponible").html("Cantidad Disponible En Bodega " + sel.value + " KG")
            lotesap = sel.options[sel.selectedIndex].text
        }
        else
            $("#LblCantidadLoteDisponible").html("Cantidad Disponible En Bodega 0 KG")
    }
    else {
        $("#Lote").attr('disabled', false);
        //$("#divlotemanual").css({ "visibility": "visible" });
        $("#LblCantidadLoteDisponible").html("")
    }
        
}

function consultarBatchCerrado(filter) {

    var batchAbierto = false;

    return batchAbierto;
}

function AbrirPesaje(e) {

    var checkPeso = $(e.target);

    if (checkPeso[0].innerText != "PESADO") {
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

        //CargarLotesSAP(dataItem.CodigoMaterial);

        $("#CodMaterial").text(dataItem.CodigoMaterial);
        $("#ListaMateriales").val(dataItem.NombreMaterial);
        //$("#CantidadAPesar").val(Math.round(dataItem.CantidadAPesar * 100, 3) / 100);
        //$("#CantidadPesada").val(Math.round(dataItem.CantidadPesada * 100, 3) / 100);
        $("#CantidadAPesar").val(dataItem.CantidadAPesar);
        $("#CantidadPesada").val(dataItem.CantidadPesada);
        $("#ErrorCrearToc").html("");
        $("#Lote").val("");
        //let faltantePorPesar = ((Math.round(dataItem.CantidadAPesar * 100, 3) / 100) - (Math.round(dataItem.CantidadPesada * 100, 3) / 100));
        let faltantePorPesar = (dataItem.CantidadAPesar - dataItem.CantidadPesada);
        $("#FaltantePorPesar").val(faltantePorPesar.toFixed(3));
        $("#Peso").val("0");

        $("#LblCantidadAPesar").show();
        $("#CantidadAPesar").show();

        $("#LblCantidadPesada").show();
        $("#CantidadPesada").show();

        $("#LblFaltantePorPesar").show();
        $("#FaltantePorPesar").show();

        $("#TotalPesadoBatch").show();
        $("#LblTotalPesadoBatch").show();

        $("#btnGuardarFormato").prop("disabled", false);
        document.getElementById("Peso").style.backgroundColor = "White";

        $("#IngresoPeso").iCheck('uncheck');

        LlamarPopupPPR($("#PopupNuevoTOC"));
    }
    else
    {
        showModal("Mezclas A.H", "El material ya ha sido pesado", "red")
    }
}

function PesarMezcla() {

    pesoBulto = true;
    $("#CodMaterial").text("9999");
    $("#ListaMateriales").val("TOTAL MEZCLA");
    $("#ErrorCrearToc").html("");
    $("#Lote1").hide();
    $("#Lote2").hide();
    $("#Lote3").hide();
    $("#Lote4").hide();
    $("#LblLotes").hide();

    $("#LblCantidadAPesar").hide();
    $("#CantidadAPesar").hide();

    $("#LblCantidadPesada").hide();
    $("#CantidadPesada").hide();

    $("#LblFaltantePorPesar").hide();
    $("#FaltantePorPesar").hide();

    $("#TotalPesadoBatch").hide();
    $("#LblTotalPesadoBatch").hide();
    $("#btnGuardarFormato").prop("disabled", false);
    document.getElementById("Peso").style.backgroundColor = "White";
    document.getElementById("Peso").removeAttribute("onchange");

    LlamarPopupPPR($("#PopupNuevoTOC"));
}

function HabilitarCampoLote(CampoLote) {

    document.getElementById(CampoLote).removeAttribute('disabled');
}

function commaToPoint(numberToConvert) {
    
    if (numberToConvert != "" || numberToConvert != "0") {

        if (numberToConvert.includes(",")) {

            numberToConvert = numberToConvert.replace(",", ".");
        }
    }

    return numberToConvert;
}

//function infiniteLoop() {
   
//    iniciarOperacionTocHimalaya();

//    console.log('Taking a break...' + Date());
//    setTimeout(function () {
//        console.log('Two seconds later, showing sleep in a loop...' + Date());
//        processLoop();
//    },5000);
//}

//function processLoop() {

//    for (let x = 0; x <= 100; x++) {
//        setDelay(x);
//    }
//}

//function setDelay(x) {
//    setTimeout(function () {
//        var grid = $("#gridNuevoBatch").data("kendoGrid");
//        var datasource = grid.dataSource.options.data;

//        for (let i = 0; i < datasource.length; i++) {
//            var dataItem = datasource[i];

//            $("#CodMaterial").text(dataItem.CodigoMaterial);
//            $("#ListaMateriales").val(dataItem.NombreMaterial);
//            $("#CantidadAPesar").val(Math.round(dataItem.CantidadAPesar * 100) / 100);
//            $("#CantidadPesada").val(Math.round(dataItem.CantidadPesada * 100) / 100);
//            $("#ErrorCrearToc").html("");
//            $("#Lote").val("");

//            let faltantePorPesar = ((Math.round(dataItem.CantidadAPesar * 100) / 100) - (Math.round(dataItem.CantidadPesada * 100) / 100));
//            $("#FaltantePorPesar").val(faltantePorPesar.toFixed(2));
//            $("#Peso").val("0");

//            $("#LblCantidadAPesar").show();
//            $("#CantidadAPesar").show();

//            $("#LblCantidadPesada").show();
//            $("#CantidadPesada").show();

//            $("#LblFaltantePorPesar").show();
//            $("#FaltantePorPesar").show();

//            $("#TotalPesadoBatch").show();
//            $("#LblTotalPesadoBatch").show();

//            $("#btnGuardarFormato").prop("disabled", false);
//            document.getElementById("Peso").style.backgroundColor = "White";

//            $("#Peso").val($("#CantidadAPesar").val())


//            let peso = $("#Peso").val();

//            peso = commaToPoint(peso);

//            $("#Lote").val('Prueba Infinite Loop');

//            GuardarFormatoTocHimalaya();
//        }

//        CerrarBatch();

//        let filtroGuardarNuevoBatch =
//        {
//            CODIGOPRODUCTO: $("#CodProducto").val(),
//            CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
//            NROOPERACION: (x + 1),
//            PESOBATCH: $("PesoBatch").val()
//        }
//        guardarNuevoBatch(filtroGuardarNuevoBatch);
        
//    }, 5000);
//}

function rowTemplate(items) {
    var rowTemplate = $("#rowTemplate");
    var html = "";

    html += '<tr data-uid="#: uid #">';

    for (var i = 0; i < items.length; i++) {
        html += '<td style="font-size:15px; border: 1px solid black; text-align:center">#:' + items[i] + '#</td>';
    }

    html += '</tr>';

    rowTemplate.html(html);
}


function ActualizarOrdenProduccionSAP() {
    debugger;

    if ($("#OrdenProduccion").val() == null || $("#OrdenProduccion").val() == "") {
        showModal("Mezclas A.H", "Por favor ingrese una orden de producción para actualizarla, la orden de producción no debe tener ningun peso registrado", "red")
        return; 
    }

    if ($("#NumeroBatch").val() == null || $("#NumeroBatch").val() == "") {
        showModal("Mezclas A.H", "Por favor inicie fabricación antes de reiniciar la orden", "red")
        return;
    }

    if ($("#PesoBatch").val() == 0 || $("#PesoBatch").val() == null) {
        showModal("Mezclas A.H", "Por favor ingrese un peso para el batch antes de actualizar la orden", "red")
        return;
    }


    var filter =
    {
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }


    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CantidadPesadaOP", function (data) {
        debugger;
        if (data.value > 0) {
            showModal("Mezclas A.H", "Esta orden ya ha iniciado el proceso de pesaje, para realizar algun cambio por favor comuniquese con el Administrador", "red")
            return;
        }

        request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ActualizarOPFormatoTocSAP", function (data) {
            debugger;

            let pesoBatch = $("#PesoBatch").val();

            var filterProducto =
            {
                CODIGOPRODUCTO: $("#CodProducto").val(),
                CODIGOPUESTO: "9999",
                CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
            }

            consultarOperacionATrabajarPromise(filterProducto).then((result) => {
                debugger;
                let filtroGuardarNuevoBatch =
                {
                    CODIGOPRODUCTO: $("#CodProducto").val(),
                    CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
                    NROOPERACION: $("#NumeroBatch").val(),
                    PESOBATCH: pesoBatch
                }

                guardarNuevoBatch(filtroGuardarNuevoBatch).then((result) => {
                    iniciarOperacionTocHimalaya();
                    iniciarFabricacionHimalaya();
                }).catch((error) => {
                    showModal("Mezclas A.H", "Ha ocurrido un error al crear el batch" + error, "red");
                    consultarOperacionActualHimalaya(filterProducto);
                    return;
                });

            }).catch((error) => {
                showModal("Mezclas A.H", "Ha ocurrido un error al crear el batch" + error, "red");
                consultarOperacionActualHimalaya(filterProducto);
                return;
            });
        }, errorHandler, JSON.stringify(filter))
    }, errorHandler, JSON.stringify(filter))
}

function ConsultarRecetaOP() {

    if ($("#OrdenProduccion").val() == null || $("#OrdenProduccion").val() == "") {
        showModal("Mezclas A.H", "Por favor ingrese una orden de producción para consultar la receta", "red")
        return;
    }

    var filter =
    {
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultarRecetaOP", function (data) {
        debugger;
        if (data.value == null) {
            showModal("Mezclas A.H", "No se encontró información para la orden ingresada, por favor comuniquese con el Administrador", "red")
            return;
        }

        LlamarPopupPPR($("#PopupRecetaOrdenProduccion"));

        $("#gridRecetaOrden").kendoGrid({
            dataSource: {
                data: data.value
            },
            schema: {
                model: {
                    fields: {
                        NumOP: { type: "string" },
                        CodigoItemDetalle: { type: "string" },
                        DescripcionItemDetalle: { type: "string" },
                        CantidadReq: { type: "number" }
                    }
                }
            },
            scrollable: false,
            columns: [
                {
                    field: "NumOP",
                    title: "Codigo de O.P",
                    width: "10px"
                },
                {
                    field: "CodigoItemDetalle",
                    title: "Codigo De Material",
                },
                {
                    field: "DescripcionItemDetalle",
                    title: "Descripcion Material",
                },
                {
                    field: "CantidadReq",
                    title: "Peso Programado (Kg)",
                    width: "150px"
                }
            ]
        });


    }, errorHandler, JSON.stringify(filter))
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;

using System.Web.Http.Results;
using SipModuloManual.Models.Security;
using System.Web;
using SipModuloManualModelo.Model;
using SipModuloManualModelo.Dto;
using System.Globalization;

namespace SipModuloManual.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using SipModuloManualModelo.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<USUARIOS>("USUARIOS");
    builder.EntitySet<ROLES>("ROLES"); 
    builder.EntitySet<FORMATOPPR>("FORMATOPPR"); 
    builder.EntitySet<FORMATOTOC>("FORMATOTOC"); 
    builder.EntitySet<FORMATOPPRCONTROL>("FORMATOPPRCONTROL"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class USUARIOSController : ODataController
    {
        private ContextEntities db = new ContextEntities();

        // GET: odata/USUARIOS
        [EnableQuery]
        public IQueryable<USUARIOS> GetUSUARIOS()
        {
            return db.USUARIOS;
            
        }

        [HttpPost]
        public IQueryable<AdminUsuariosDTO> GetAdminUsuarios()
        {

            return from u in db.USUARIOS
                   join p in db.USUARIOSXROL
                   on u.LOGIN equals p.LOGIN
                   select new AdminUsuariosDTO() { Login = u.LOGIN, Password = u.PASSWORD, Fecha_Creacion = u.FECHA_CREACION, CodigoPersonal = u.CODIGOPERSONAL, Rol = p.ROLES.ROL, IdRol = p.ROLES.IDROL};

        }
        
        [HttpPost]
        public IQueryable<AdminPersonalDTO> GetAdminPersonal()
        {
            return from p in db.PERSONAL
                   select new AdminPersonalDTO() { CodigoPersonal = p.CODIGOPERSONAL, Cedula = p.CEDULA, Nombres = p.NOMBRES, Apellidos = p.APELLIDOS, Sexo = p.SEXO, Fecha_Nacimiento = p.FECHANACIMIENTO };
        }


        // GET: odata/USUARIOS(5)
        [EnableQuery]
        public SingleResult<USUARIOS> GetUSUARIOS([FromODataUri] string key)
        {
            return SingleResult.Create(db.USUARIOS.Where(uSUARIOS => uSUARIOS.LOGIN == key));
        }

        [HttpPost]
        public IQueryable<PersonalDTO> GetPersonalSinUsuario()
        {
            return  from p in db.PERSONAL
                    where !(db.USUARIOS.Any(u => u.CODIGOPERSONAL == p.CODIGOPERSONAL))
                    select new PersonalDTO() { CodigoPersonal = p.CODIGOPERSONAL, NombrePersonal = p.NOMBRES +" " + p.APELLIDOS } ;
        }

        [HttpPost]
        public IQueryable<RolesDTO> GetRolesParaUsuario()
        {
            return from p in db.ROLES
                   select new RolesDTO() { IdRol = p.IDROL, Rol = p.ROL};
        }

        [HttpPost]
        public bool Login(ODataActionParameters parameters)
        {
            string user = (string)parameters["USUARIO"];
            string pass = (string)parameters["PASSWORD"];
            Boolean Result = false;
            new UsersSecurity().ValidacionLogin(user, pass, ref Result);

            //HttpContext.Current.Session["autenticated-user"] = user;
            return (Result);
        }

        [HttpPost]
        public void DeleteUsuarioXRol (ODataActionParameters parameters)
        {

            try
            {
                string Login = (string)parameters["LOGIN"];
                string Password = (string)parameters["PASSWORD"];
                string CodigoPersonal = (string)parameters["CODIGOPERSONAL"];
                int IdRol = (int)parameters["IDROL"];
                string Rol = (string)parameters["ROL"];

                var y = (from a in db.USUARIOSXROL
                         where a.LOGIN == Login
                         select a).FirstOrDefault();

                db.USUARIOSXROL.Remove(y);
                db.SaveChanges();
                try
                {
                    var u = (from x in db.USUARIOS
                             where x.LOGIN == Login
                             select x).FirstOrDefault();
                    db.USUARIOS.Remove(u);
                    db.SaveChanges();
                }
                catch(Exception ex)
                {
                    throw new Exception("El usuario " +Login+" se ha desasociado de su rol, pero no puede ser eliminado definitivamente porque se encuentra" +
                        " asociado a formatos", ex);
                }
            }
            catch (Exception ex )
            {
                if(ex.InnerException != null)
                {
                    throw new Exception("El usuario se ha desasociado de su rol, pero no puede ser eliminado definitivamente porque se encuentra" +
                       " asociado a formatos", ex);
                }
                else
                {
                    throw new Exception("El usuario ya ha sido eliminado", ex);
                }
            }

        }

        [HttpPost]
        public void DeletePersonal(ODataActionParameters parameters)
        {

            try
            {
                string CodPersonal = (string)parameters["CODIGOPERSONAL"];
                string Cedula = (string)parameters["CEDULA"];
                string Nombres = (string)parameters["NOMBRES"];
                string Apellidos = (string)parameters["APELLIDOS"];
                string Sexo = (string)parameters["SEXO"];
                DateTime Fecha_Nacimiento = (DateTime)parameters["FECHANACIMIENTO"];

                var u = db.USUARIOS.Count(e => e.CODIGOPERSONAL == CodPersonal) > 0;

                if(u)
                {
                    throw new Exception("No es posible eliminar el Personal porque tiene un usuario asignado");
                }
                else
                {
                    var p = (from a in db.PERSONAL
                             where a.CODIGOPERSONAL == CodPersonal
                             select a).FirstOrDefault();

                    db.PERSONAL.Remove(p);
                    db.SaveChanges();
                }


            }
            catch (Exception ex)
            {

                throw new Exception("No es posible eliminar el Personal porque tiene un usuario asignado" + ex); ;
            }


        }

        [HttpPost]
        public void CrearUsuarioXRol(ODataActionParameters parameters)
        {
            try
            {
                string Login = (string)parameters["LOGIN"];
                int IdRol = (int)parameters["IDROL"];
                string Rol = (string)parameters["ROL"];
                string Password = (string)parameters["PASSWORD"];
                int Nivel = 1;
                DateTime FechaCreacion = DateTime.Now;
                string CodigoPersonal = (string)parameters["CODIGOPERSONAL"];
                DateTime Timespan = DateTime.Now;

                USUARIOSXROL UsuarioXRol = new USUARIOSXROL
                {
                    IDROL = IdRol,
                    LOGIN = Login
                };
                USUARIOS usuario = new USUARIOS
                {
                    LOGIN = Login,
                    PASSWORD = Password,
                    NIVEL = Nivel,
                    FECHA_CREACION = DateTime.Now,
                    ULTIMO_ACCESO = null,
                    CODIGOPERSONAL = CodigoPersonal,
                    ESTADO = 1,
                    TIMESPAN = DateTime.Now
                };
                if(!USUARIOSExists(Login))
                {
                    if(!USUARIOSXROLExists(Login))
                    {
                        db.USUARIOS.Add(usuario);
                        db.USUARIOSXROL.Add(UsuarioXRol);
                    }
                }
                else
                {
                    throw new Exception("Error Creando Usuario");
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception("Error Creando Usuario " + ex.Message);
            }
        }

        [HttpPost]
        public void CrearPersonal(ODataActionParameters parameters)
        {
            try
            {
               string CodPersonal = (string)parameters["CODIGOPERSONAL"];
               string Cedula =(string)parameters["CEDULA"];
               string Nombre = (string)parameters["NOMBRE"];
               string Apellidos = (string)parameters["APELLIDOS"];
               string Sexo = (string)parameters["SEXO"];
               string FechaParameters = (string)parameters["FECHANACIMIENTO"];
               DateTime Fecha_Nacimiento = DateTime.ParseExact(FechaParameters,"MM/dd/yyyy",CultureInfo.InvariantCulture);

                var p = db.PERSONAL.Count(e => e.CODIGOPERSONAL == CodPersonal) >0;
                var d = db.PERSONAL.Count(e => e.CEDULA == Cedula) > 0;
               
                if(p)
                {
                    throw new Exception("El codigo de Personal ya Existe");
                }
                if(d)
                {
                    throw new Exception("La Cedula ya tiene un Codigo de Personal Asignado");
                }

                PERSONAL newPersonal = new PERSONAL
                {
                    CODIGOPERSONAL = CodPersonal,
                    CEDULA = Cedula,
                    TIPODOCUMENTO = "CC",
                    NOMBRES = Nombre,
                    APELLIDOS = Apellidos,
                    SEXO = Sexo,
                    FECHANACIMIENTO = Fecha_Nacimiento,
                    DIRECCION = "1",
                    TELEFONOS = "1",
                    CODIGOSUELDO = "9999",
                    CODIDOCARGOSPERSONAL = "1",
                    ESTADO = 1,
                    FECHAINGRESO = DateTime.Now,
                    EXPERIENCIACARGOS1 = "1",
                    EXPERIENCIACARGOS2 = "2",
                    EXPERIENCIACARGOS3 = "3",
                    ESTUDIOS = "1",
                    CURSOS = "1",
                    TIMESPAN = DateTime.Now
                };


                db.PERSONAL.Add(newPersonal);
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                throw new Exception("" + ex);
            }
        }

        [HttpPost]
        public void CrearUsuario(ODataActionParameters parameters)
        {
            
            try
            {
                string Login = (string)parameters["LOGIN"];
                string Password = (string)parameters["PASSWORD"];
                int Nivel = 1;
                DateTime FechaCreacion = DateTime.Now;
                DateTime? UltimoAcceso = null;
                string CodigoPersonal = (string)parameters["CODIGOPERSONAL"];
                int Estado = 1;
                DateTime Timespan = DateTime.Now;

                USUARIOS usuario = new USUARIOS();
                usuario.LOGIN = Login;
                usuario.PASSWORD = Password;
                usuario.NIVEL = Nivel;
                usuario.FECHA_CREACION = FechaCreacion;
                usuario.ULTIMO_ACCESO = UltimoAcceso;
                usuario.CODIGOPERSONAL = CodigoPersonal;
                usuario.ESTADO = Estado;
                usuario.TIMESPAN = Timespan;

                if (!USUARIOSExists(usuario.LOGIN))
                {
                    Post(usuario);
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Error Creando Usuario " + ex.Message);
            }
                
    }

        [HttpPost]
        public void ActualizarUsuario(ODataActionParameters parameters)
        {
            try
            {
                string Login = (string)parameters["LOGIN"];
                string Password = (string)parameters["PASSWORD"];
                string CodigoPersonal = (string)parameters["CODIGOPERSONAL"];
                int IdRol = (int)parameters["IDROL"];
              

                USUARIOS usuario = (from p in db.USUARIOS
                                    where p.CODIGOPERSONAL == CodigoPersonal
                                    select p).FirstOrDefault();

                usuario.LOGIN = Login;
                usuario.PASSWORD = Password;
                usuario.CODIGOPERSONAL = CodigoPersonal;
                usuario.TIMESPAN = DateTime.Now;

                USUARIOSXROL usuarioXRol = (from r in db.USUARIOSXROL
                                           where r.LOGIN == Login
                                           select r).FirstOrDefault();

                usuarioXRol.IDROL = IdRol;
                usuarioXRol.LOGIN = Login;

                db.SaveChanges();

            }
            catch
            {

            }
        }

        [HttpPost]
        public void ActualizarPersonal(ODataActionParameters parameters)
        {
            try
            {
                string CodPersonal = (string)parameters["CODIGOPERSONAL"];
                string Cedula = (string)parameters["CEDULA"];
                string Nombres = (string)parameters["NOMBRES"];
                string Apellidos = (string)parameters["APELLIDOS"];
                string Sexo = (string)parameters["SEXO"];
                DateTime Fecha_Nacimiento = (DateTime)parameters["FECHANACIMIENTO"];

                PERSONAL personal = (from p in db.PERSONAL
                                     where p.CODIGOPERSONAL == CodPersonal
                                     select p).FirstOrDefault();

                personal.CEDULA = Cedula;
                personal.NOMBRES = Nombres;
                personal.APELLIDOS = Apellidos;
                personal.SEXO = Sexo;
                personal.FECHANACIMIENTO = Fecha_Nacimiento;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception("No se pudo actualizar el Personal" + ex);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo actualizar el Personal" + ex);
            }
        }


        // PUT: odata/USUARIOS(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<USUARIOS> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            USUARIOS uSUARIOS = db.USUARIOS.Find(key);
            if (uSUARIOS == null)
            {
                return NotFound();
            }

            patch.Put(uSUARIOS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!USUARIOSExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(uSUARIOS);
        }


        public IHttpActionResult PostUsuarioXRol(USUARIOSXROL UsuarioXRol)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.USUARIOSXROL.Add(UsuarioXRol);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                if (USUARIOSXROLExists(UsuarioXRol.LOGIN))
                {
                    return Conflict();
                }
                else
                {
                    throw new Exception("Error Creando Usuario " + ex.Message);
                }
            }
            return Created(UsuarioXRol);
        }

        // POST: odata/USUARIOS
        public IHttpActionResult Post(USUARIOS uSUARIOS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.USUARIOS.Add(uSUARIOS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (USUARIOSExists(uSUARIOS.LOGIN))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(uSUARIOS);
        }

        // PATCH: odata/USUARIOS(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<USUARIOS> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            USUARIOS uSUARIOS = db.USUARIOS.Find(key);
            if (uSUARIOS == null)
            {
                return NotFound();
            }

            patch.Patch(uSUARIOS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!USUARIOSExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(uSUARIOS);
        }

        // DELETE: odata/USUARIOS(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            USUARIOS uSUARIOS = db.USUARIOS.Find(key);
            if (uSUARIOS == null)
            {
                return NotFound();
            }

            db.USUARIOS.Remove(uSUARIOS);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }
      

        // GET: odata/USUARIOS(5)/FORMATOPPR
        [EnableQuery]
        public IQueryable<FORMATOPPR> GetFORMATOPPR([FromODataUri] string key)
        {
            return db.USUARIOS.Where(m => m.LOGIN == key).SelectMany(m => m.FORMATOPPR);
        }

        // GET: odata/USUARIOS(5)/FORMATOPPR1
        [EnableQuery]
        public IQueryable<FORMATOPPR> GetFORMATOPPR1([FromODataUri] string key)
        {
            return db.USUARIOS.Where(m => m.LOGIN == key).SelectMany(m => m.FORMATOPPR1);
        }

        // GET: odata/USUARIOS(5)/FORMATOTOC
        [EnableQuery]
        public IQueryable<FORMATOTOC> GetFORMATOTOC([FromODataUri] string key)
        {
            return db.USUARIOS.Where(m => m.LOGIN == key).SelectMany(m => m.FORMATOTOC);
        }

        // GET: odata/USUARIOS(5)/FORMATOPPRCONTROL
        [EnableQuery]
        public IQueryable<FORMATOPPRCONTROL> GetFORMATOPPRCONTROL([FromODataUri] string key)
        {
            return db.USUARIOS.Where(m => m.LOGIN == key).SelectMany(m => m.FORMATOPPRCONTROL);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool USUARIOSXROLExists(string key)
        {
            return db.USUARIOSXROL.Count(e => e.LOGIN == key) > 0;
        }
       
        private bool USUARIOSExists(string key)
        {
            return db.USUARIOS.Count(e => e.LOGIN == key) > 0;
        }
    }
}

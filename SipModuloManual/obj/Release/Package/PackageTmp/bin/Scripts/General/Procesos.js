﻿$(document).ready(function () {

    var a = 0;

});


function iniciarProcesoMezclado(e)
{
    $("#divaprobar").hide();
    $("#divguardar").show();


    cambiarcheck(2);
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");

    $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
    $("#codMaterial").val(dataItem.CODIGOMATERIAL);
    $("#codProducto").val($("#codigoProducto").val());
    $("#Producto").val(dataItem.NOMBREPRODUCTO);


    if (dataItem.PROCESADO)
    {
        consultaPPREnBodega(dataItem);
    }
    else
    {
        LimpiarDatosPPR();
        LlamarPopupPPR($("#PopupNuevoPPR27"));
        request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/MostrarUsuario", function (data) {

            if (dataItem.NOMBREMATERIAL.includes("Pre-Mezc-") == false)
            { 
                consultaPPRLoteYNroAnalisis(dataItem);
            }


            $("#OperarioEntrega").val(data.Nombre);
            $("#idOperarioEntrega").val(data.CodigoUsuario);
        }, errorHandler, null)

    }

    

  

    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}


function iniciarDevolucion(e) {

    $("#divaprobar").hide();
    $("#divguardar").show();

    cambiarcheck(3);
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));


    if (dataItem.NOMBREMATERIAL.includes("Pre-Mezc-") == true)
    {
        showModal("Sistema Cadivi", "<h2>Una Pre-Mezcla No Puede Ingresar Formato de Devolucion</h2>", 'red');
    }
    else
    {

      
    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");

        $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
        $("#codMaterial").val(dataItem.CODIGOMATERIAL);
        $("#codProducto").val($("#codigoProducto").val());
        $("#Producto").val(dataItem.NOMBREPRODUCTO);


        if (dataItem.DEVUELTO)
        {
       
                consultaPPREnBodega(dataItem);
       
        
        }
        else
        {
            LimpiarDatosPPR();
            LlamarPopupPPR($("#PopupNuevoPPR27"));


            request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/MostrarUsuario", function (data)
            {
                consultaPPRLoteYNroAnalisis(dataItem);
                $("#OperarioEntrega").val(data.Nombre);
                $("#idOperarioEntrega").val(data.CodigoUsuario);
            }, errorHandler, null)
}
    }
    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}

function iniciarEntrega(e)
{
   
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
   
    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");

    $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
    $("#codMaterial").val(dataItem.CODIGOMATERIAL);
    
    $("#Producto").val(dataItem.NOMBREPRODUCTO);
    

    

        
    if (dataItem.APROBADO == false || dataItem.APROBADO == null)
    {
        if (dataItem.GUARDADO == true)
        {
            PararComunicacion();
            consultaPPREnBodega(dataItem);
        }
        else
        {

            ReactivarComunicacion();
            LimpiarDatosPPR();
            LlamarPopupPPR($("#PopupNuevoPPR27"));

            request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/MostrarUsuario", function (data) {

                $("#OperarioEntrega").val(data.Nombre);
                $("#idOperarioEntrega").val(data.CodigoUsuario);
            }, errorHandler, null)

        }
    }
    else
        showModal("Sistema Cadivi", "No se puede modificar un Material ya Aprobado por Mezclador", "red");
    
    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}


function iniciarActualizacion(e)
{
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    $("#codFormato").val(dataItem.ID);
    $("#Pregunta").val(dataItem.PREGUNTA);

    $("#Respuesta").val(dataItem.RESPUESTA);
    $("#Aplica").val(dataItem.APLICA);

    $("#Observaciones").val(dataItem.OBSERVACIONES);

    
    LlamarPopupPPR($("#PopupNuevoPPR019"));

    //$("#gridControlAgitacion").data("kendoGrid").dataSource.transport.update();
}

function iniciarFabricacion()
{
    var f = new Date();
    
    
    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora= f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();


    $("#Hora").val(hora + ":" + min);
    $("#FechaIngreso").val(f.getDate() + "-" + parseInt(f.getMonth() + 1) + "-" + f.getFullYear());

    $("#ErrorEditar").html("");

    if($("#OrdenProduccion").val()==""){
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion Para Iniciar Fabricacion", "red");
        return;
    }

    if($("#CodProducto").val()==""){
        showModal("Sistema Cadivi", "Ingrese El Codigo de Producto Para Iniciar Fabricacion", "red");
        return;
    }

    if ($("#LoteProducto").val() == "" || $("#LoteProducto").val() == "0")
    {
        showModal("Sistema Cadivi", "Ingrese Un Numero de Lote Valido Para Iniciar Fabricacion", "red");
        return;
    }


    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }

    $("#PesoKg").val("200");

    LlenarGridToc01(filter);
    consultarOperacionActual(filter);
    consultarOperacionBase(filter);

}

function iniciarOperacionToc()
{
    $("#ErrorCrearOperacion").html("");
    
    if ($("#OpBase").val() == "") {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion de La Base Para Crear una Operacion", "red");
        return;
    }

    if ($("#PesoKg").val() == "") {
        showModal("Sistema Cadivi", "Ingrese El Peso En Kilogramos De La Base Para Crear una Operacion", "red");
        return;
    }

    if ($("#Hora").val() == "") {
        showModal("Sistema Cadivi", "Ingrese La Hora De Adición Para Crear una Operacion", "red");
        return;
    }

    if ($("#MinutosMezcla").val() == "") {
        showModal("Sistema Cadivi", "Ingrese Los Minutos De Mezcla Para Crear una Operacion", "red");
        return;
    }


    

    
    var filterProducto =
        {
            CODIGOPRODUCTO: $("#CodProducto").val()
        }


    LlenarCombo($("#ListaMateriales"), filterProducto);
    LlamarPopupPPR($("#PopupNuevoTOC"));
    llenargridMateriaPrima();
}

function iniciarToc02() {
    $("#FechaIngreso").val(new Date().toJSON().slice(0, 10));


    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi","Error Iniciando Proceso: Ingrese El Numero de Orden de Produccion Para Iniciar El Proceso", 'red');
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Cadivi" ,"Error Iniciando Proceso: Ingrese El Codigo Del Producto", 'red');
        return;
    }

    if ($("#LoteProducto").val() == "") {
        showModal("Sistema Cadivi","Error Iniciando Proceso: Ingrese El Numero de Lote", 'red');
        return;
    }

    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        LOTE: $("#LoteProducto").val(),
        LOGIN: window.localStorage['usuario'],
        ID: 0,
        FECHA: $("#FechaIngreso").val(),
        OBSERVACIONES: "",
        PASTARECHAZADAFIN: 0,
        PASTARECHAZADAINICIO:0,
        PASTASPRODUCIDAS: 0,
        PORCENTAJERECHAZADO: 0,
        CUMPLEGALGAS: false
    }

    $("#btnEditar").css("visibility", "hidden");
    LlenarGridControlProceso(filter);
    showModal("Sistema Cadivi", "Datos Cargados Con Exito", 'blue');
   
}

function continuarPpr01()
{
    $("#FechaIngreso").val(new Date().toJSON().slice(0, 10));

    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Orden de Produccion Para Iniciar El Proceso", 'red');
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Codigo Del Producto", 'red');
        return;
    }

    if ($("#LoteProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Lote", 'red');
        return;
    }

    if ($("#Presentacion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Presentacion", 'red');
        return;
    }


    if ($("#PesoMin").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Valor del Peso Minimo", 'red');
        return;
    }



    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        LOTE: $("#LoteProducto").val(),
        LOGIN: window.localStorage['usuario'],
        ID: 1,
        FECHA: $("#FechaIngreso").val(),
        PRESENTACION: parseInt( $("#Presentacion").val()),
        PESOMINIMO: parseInt($("#PesoMin").val()),
    }


    LlenarGridControlPpr01(filter);
    showModal("Sistema Cadivi", "Datos Cargados Con Exito", 'blue');

}

function iniciarPpr01() {
    $("#FechaIngreso").val(new Date().toJSON().slice(0, 10));

    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Orden de Produccion Para Iniciar El Proceso", 'red');
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Codigo Del Producto", 'red');
        return;
    }

    if ($("#LoteProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Lote", 'red');
        return;
    }

    if ($("#Presentacion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Presentacion", 'red');
        return;
    }


    if ($("#PesoMin").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Valor del Peso Minimo", 'red');
        return;
    }



    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        LOTE: $("#LoteProducto").val(),
        LOGIN: window.localStorage['usuario'],
        ID: 0,
        FECHA: $("#FechaIngreso").val(),
        PRESENTACION: parseInt($("#Presentacion").val()),
        PESOMINIMO: parseInt($("#PesoMin").val()),
    }


    LlenarGridControlPpr01(filter);
    showModal("Sistema Cadivi", "Datos Cargados Con Exito", 'blue');

}



function iniciarFabricacionHimalaya() {

    var f = new Date();


    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora = f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();

    $("#Hora").val(hora + ":" + min);

    $("#FechaIngreso").val(f.getDate() + "-" + parseInt(f.getMonth() + 1) + "-" + f.getFullYear());

    $("#ErrorEditar").html("");

    $("#PesoBatch").val("");
    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Mezclas", "Ingrese El Numero de Orden de Produccion Para Iniciar Fabricacion", "red");
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Mezclas", "Ingrese El Codigo de Producto Para Iniciar Fabricacion", "red");
        return;
    }

    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: "9999",
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }

    LlenarGridCapturaHimalaya(filter);
    consultarOperacionActualHimalaya(filter);


}

function iniciarOperacionTocHimalaya() {
    $("#ErrorCrearOperacion").html("");

    
    //if ($("#PesoBatch").val() == "") {
    //    showModal("Sistema Cadivi", "Ingrese El Peso del Batch", "red");
    //    return;
    //}

    var filterProducto =
    {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        NROOPERACION: $("#NumeroBatch").val()
    }


    LlenarCombo($("#ListaMateriales"), filterProducto);
    LlamarPopupPPR($("#PopupNuevoTOC"));
    llenargridMateriaPrimaHimalaya();
}

function CalcularLimite() {

    var cantidadAPesar = parseFloat($("#CantidadAPesar").val());
    var pesoTiempoReal = parseFloat($("#Peso").val());


    if (pesoTiempoReal == cantidadAPesar)
    {

        document.getElementById("Peso").style.backgroundColor = "Green";
        $("#btnGuardarFormato").attr("disabled", false);
    }
    else if ((pesoTiempoReal > (cantidadAPesar - 0.1) && pesoTiempoReal < cantidadAPesar) || ((pesoTiempoReal < (cantidadAPesar + 0.1)) && pesoTiempoReal > cantidadAPesar) || (pesoTiempoReal == (cantidadAPesar - 0.1)) || (pesoTiempoReal == (cantidadAPesar + 0.1))) {

        document.getElementById("Peso").style.backgroundColor = "Yellow";
        $("#btnGuardarFormato").attr("disabled", false);
    }
    else if(pesoTiempoReal < (cantidadAPesar - 0.1) || pesoTiempoReal > (cantidadAPesar + 0.1))
    {
        document.getElementById("Peso").style.backgroundColor = "Red";

        $("#btnGuardarFormato").attr("disabled", true);
    }

    
}

function ConsultarCantidadAPesar() {
   
    var cantidadAPesar = 0;
    var filter = {

        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOMATERIAL:$("#ListaMateriales").val()
    };

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultarCantidadAPesar", function (data) {

        if (data != null) {
            cantidadAPesar = (data.value);
            $("#CantidadAPesar").val((cantidadAPesar * a).toFixed(2));

        }



    }, errorHandler, JSON.stringify(filter))


}
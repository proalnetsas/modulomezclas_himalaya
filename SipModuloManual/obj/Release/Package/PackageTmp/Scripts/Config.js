﻿$(document).ready(function ()
{
    //Activa la cultura de kendo y llama la clase para activar botones de menu
    kendo.culture("es-CO");
    activeClass("ConfigurarMenu", "ParentMenu", "LMenu");
    
  

});

//Funcion para activar controles de kendo
function activeClass(id, content, parent)
{
    var ul = document.getElementById("side-menu");
    var items = ul.getElementsByTagName("li");
    for (var i = 0; i < items.length; ++i) {
        //Debemos remover la class active de todos los elementos li
        $(items[i].getElementsByTagName("a")).removeClass("active");
    }
    //Después de haber quitado la class active de todos los objetos de la lista se debe activar el id mandado por parametro
    //$("#" + parent).addClass("in");
    $("#" + parent).addClass("active");
    document.getElementById(content).style.display = "block";
    $("#" + id).addClass("active");
    
}



//Llama cualquier popup de kendo
function LlamarPopupPPR(popup) {
    var window = popup;

    window.data("kendoWindow").open()
    window.data("kendoWindow").center();
}

//Cierra cualquier popup de kendo
function CerrarPopup(popup) {
    var window = popup;
    window.data("kendoWindow").close();
}


//Configura el popup del ppr027 de mezclado y bodega
function ConfigurarPantallaInicial() {
    

    $("#FechaRecibido").kendoDatePicker({
        format: "dd-MMM-yyyy"
    });

    $("#FechaEntrega").kendoDatePicker({
        format: "dd-MMM-yyyy"
    });

    $("#FechaPesaje").kendoDatePicker({
        format: "dd-MMM-yyyy"
    });


    var window = $("#PopupNuevoPPR27");



    if (!window.data("kendoWindow")) {
        window.kendoWindow({
            width: "650",
            height: "620",
            title: "Formato PPR 027",

            modal: false,
            resizable: false,
            actions: [
                "Close"
            ],
            visible: false,
            open: function () {
                $(document.getElementsByClassName('PupUpModalMask')).show();
            },
            close: function (e) {
                $(document.getElementsByClassName('PupUpModalMask')).hide();
            }
        });
    }


    if ($('.PopUpModalMask').length == 0)
    {
        $('body').append('<div class="modalMask PupUpModalMask"></div>');
        $(document.getElementsByClassName('PupUpModalMask')).hide();
    }

    if ($('.PupUpModalLoading').length == 0) {


        $('body').append('<div class="modalMask PupUpModalLoading" > \
            <div class="input-group" style="position: fixed;top: 50%;left: 45%;">\
                <div class="col-lg-10 nopadding">\
                    <h3 style="padding-top: 0px;margin-top: 0px;color: white;">Generando Reporte</h3>\
                </div>\
                <div class="col-lg-2">\
                    <div class="loader"></div>\
                </div>\
            </div>\
        </div>'); 
        $(document.getElementsByClassName('PupUpModalLoading')).hide();
    }
    
}

//Configura Cualquier Popup de kendo mandando los 4 parametros
function ConfigurarPopup(popup, alto, ancho, titulo, modal) {
    var window = popup;
    if (!window.data("kendoWindow")) {
        window.kendoWindow({
            width: ancho,
            height: alto,
            title: titulo,

            modal: modal,
            resizable: false,
            actions: [
                "Close"
            ],
            visible: false,
            open: function () {
                $(document.getElementsByClassName('PupUpModalMask')).show();
            },
            close: function (e) {
                $(document.getElementsByClassName('PupUpModalMask')).hide();
            }
        });

    }
}


//Dispara el evento, ya que el check no lo toma por el dom 
$(document).on('ifChanged', '#IngresoPeso', function (e) {
    // Creo que el PopUp lo desvincula del DOM y lo vuelve a crear,por eso no quedaba asociado el evento... tocó así dinámico.
    
    if ($("#IngresoPeso").is(':checked'))
    {
        PararComunicacion();
        $("#Peso").attr("disabled", false);
    }
    else {
        ReactivarComunicacion();
        $("#Peso").attr("disabled", true);
    }
});


//Dispara el evento, ya que el check no lo toma por el dom 
$(document).on('ifChanged', '#IngresoPesoPpr', function (e) {
    // Creo que el PopUp lo desvincula del DOM y lo vuelve a crear,por eso no quedaba asociado el evento... tocó así dinámico.

    if ($("#IngresoPesoPpr").is(':checked')) {
        PararComunicacion();
        $("#PesoBruto").attr("disabled", false);
    }
    else {
        ReactivarComunicacion();
        $("#PesoBruto").attr("disabled", true);
    }
});
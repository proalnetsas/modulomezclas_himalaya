﻿
function consultarOrden() {

    $("#ErrorEditar").html("");

    if ($("#ordenProduccion").val() == "")
    {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion a Consultar", "red");
        return;
    }

    llenargridProductosConPm();
}

function consultarOrdenMezclado()
{

    $("#ErrorEditar").html("");

    if ($("#ordenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion a Consultar", "red");
        return;
    }

    llenargridMaterialesMezclado();


}

function consultaOrdenPPR() {
 

    $("#ErrorEditar").html("");

    if ($("#OrdenProduccion").val() == "")
    {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion a Consultar", "red");
        $("#CodProducto").val("");
        $("#Producto").val("");
        return;
    }

    
    var ordenProduccion = $("#OrdenProduccion").val();
   
    if (ordenProduccion == undefined)
    {
        ordenProduccion = $("#ordenProduccion").val();
    }

    //request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs(" + ordenProduccion + ")", function (data) {
    request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs('" + ordenProduccion + "')", function (data) {
        

        if ($("#gridControlProceso") != undefined)
        {
            if ($("#gridControlProceso").data("kendoGrid") != null)
            {
                $("#gridControlProceso").kendoGrid({dataSource: null})
            }
        }

        

        if (data.NOMBREPRODUCTO == null || data.CODIGOPRODUCTOS == null)
        {
            showModal("Sistema Cadivi", "La Orden a Consultar No Ha Sido Entregada En Bodega", "red");
            $("#CodProducto").val("");
            $("LoteProducto").val("");
            $("#Producto").val("");
            $("#TotalProduccion").val("");
            $("#TotalOperaciones").val("");
        }
        else
        {
            $("#CodProducto").val(data.CODIGOPRODUCTOS);
            $("#Producto").val(data.NOMBREPRODUCTO);
            $("#LoteProducto").val(data.LOTEPRODUCTO);
            $("#TotalProduccion").val(data.KILOGRAMOSODP);
            $("#TotalOperaciones").val((data.KILOGRAMOSODP / 205));
        }
        
    }, errorHandler);


    //setInterval(function () {
    //    consultarTemperaturaSipsada();
    //}, 3000);

}

function consultaOrdenTOC02() {


    $("#ErrorEditar").html("");

    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion a Consultar", "red");
        $("#CodProducto").val("");
        $("#Producto").val("");
        return;
    }


    var ordenProduccion = $("#OrdenProduccion").val();

    if (ordenProduccion == undefined) {
        ordenProduccion = $("#ordenProduccion").val();
    }

    //request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs(" + ordenProduccion + ")", function (data) {
    request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs('" + ordenProduccion + "')", function (data) {


        if ($("#gridControlProceso") != undefined)
        {
            if ($("#gridControlProceso").data("kendoGrid") != null) {
                $("#gridControlProceso").kendoGrid({ dataSource: null })
            }
        }



        if (data.NOMBREPRODUCTO == null || data.CODIGOPRODUCTOS == null) {
            showModal("Sistema Cadivi", "La Orden a Consultar No Ha Sido Entregada En Bodega", "red");
            $("#CodProducto").val("");
            $("LoteProducto").val("");
            $("#Producto").val("");
        }
        else {
            $("#CodProducto").val(data.CODIGOPRODUCTOS);
            $("#Producto").val(data.NOMBREPRODUCTO);
            $("#LoteProducto").val(data.LOTEPRODUCTO);
        }

    }, errorHandler);


    //setInterval(function () {
    //    consultarTemperaturaSipsada();
    //}, 5000);

}

function consultaOrdenPPRMezclado()
{


    $("#ErrorEditar").html("");

    if ($("#OrdenProduccion").val() == "")
    {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion a Consultar", "red");
        $("#codigoProducto").val("");
        $("#Producto").val("");
        return;
    }


    var ordenProduccion = $("#ordenProduccion").val();
  
    //request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs(" + ordenProduccion + ")", function (data) {
    request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs('" + ordenProduccion + "')", function (data) {

        
        if (data.NOMBREPRODUCTO == null || data.CODIGOPRODUCTOS == null)
        {
            showModal("Sistema Cadivi", "La Orden a Consultar No Ha Sido Entregada En Bodega", "red");
            $("#codigoProducto").val("");
            $("#nombreProducto").val("");
        }
        else {
            $("#codigoProducto").val(data.CODIGOPRODUCTOS);
            $("#nombreProducto").val(data.NOMBREPRODUCTO);
        }

    }, errorHandler);


}

function iniciarIngresoCondiciones() {

    var f = new Date();

     
    var grid = $("#gridCondiciones").data("kendoGrid");
    var lista = grid.dataSource.data();

    var dataCondiciones = new Array(lista.length);

    for (var i = 0; i < lista.length; i++) {
        dataCondiciones[i] = { nombre: lista[i].CONDICION, id: lista[i].ID };
    }



    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora = f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();


    $("#Hora").val(hora + ":" + min);


    var data = ["HORA1", "HORA2", "HORA3", "HORA4", "HORA5", "HORA6", "HORA7", "HORA8"];

    $("#ListaHora").kendoComboBox({
        dataSource: data
    });

    $("#ListaCondiciones").kendoComboBox({
        dataTextField: "nombre",
        dataValueField: "id",
        dataSource: dataCondiciones
    });
    



    LlamarPopupPPR($("#PopupNuevoCondicion"));
}



function iniciarIngresoPesos()
{

    var f = new Date();


    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora = f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();


    $("#Hora").val(hora + ":" + min);


    var data = ["HORA1", "HORA2", "HORA3", "HORA4", "HORA5", "HORA6", "HORA7", "HORA8", "HORA9", "HORA10", "HORA11", "HORA12", "HORA13", "HORA14", "HORA15", "HORA16", "HORA17", "HORA18", "HORA19", "HORA20", "HORA21", "HORA22", "HORA23", "HORA24"];

    $("#ListaHora").kendoComboBox({
        dataSource: data
    });

    LlamarPopupPPR($("#PopupNuevoPesos"));
}

function iniciarIngresoAtrib() {

    var f = new Date();


    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora = f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();


    $("#Hora1").val(hora + ":" + min);


    var data = ["HORA1", "HORA2", "HORA3", "HORA4", "HORA5", "HORA6", "HORA7", "HORA8", "HORA9", "HORA10", "HORA11", "HORA12", "HORA13", "HORA14", "HORA15", "HORA16", "HORA17", "HORA18", "HORA19", "HORA20", "HORA21", "HORA22", "HORA23", "HORA24"];

    $("#ListaHoraA").kendoComboBox({
        dataSource: data
    });

    LlamarPopupPPR($("#PopupNuevoAtrib"));
}

function consultaPPRBodega(e)
{
    $("#divaprobar").show();
    $("#divguardar").hide();


    cambiarcheck(1);
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    if (dataItem.NOMBREMATERIAL.includes("Pre-Mezc-") == true) {
        showModal("Sistema Cadivi", "<h2>Una Pre-Mezcla No Puede Ingresar Formato de Entrega</h2>", 'red');
    }
    else
    {
        

        //$("#NombreMaterial").attr("disabled", "disabled");
        //$("#Producto").attr("disabled", "disabled");
        $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
        $("#codMaterial").val(dataItem.CODIGOMATERIAL);
        $("#Producto").val(dataItem.NOMBREPRODUCTO);


        LimpiarDatosPPR();
        LlamarPopupPPR($("#PopupNuevoPPR27"));
    


        var filter = {
            CODIGOORDENPRODUCCION: $("#ordenProduccion").val(),
            MOMENTO: $('input[name="Status"]:checked').val(),
            CODIGOPRODUCTO: $("#codigoProducto").val(),
            CODIGOMATERIAL: dataItem.CODIGOMATERIAL,
        }

    

        request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/BuscaformatoBodega", function (data) {

            $("#NAnalisis").val(data.ANALISIS);
            $("#NRecipiente").val(data.RECIPIENTE);
            $("#Consecutivo").val(data.CONSECUTIVORECIPIENTE);
            $("#LoteProducto").val(data.LOTEPRODUCTO);
            $("#FechaPesaje").data("kendoDatePicker").value(data.FECHAPESAJE);
            $("#FechaEntrega").data("kendoDatePicker").value(data.FECHAENTREGA);
            $("#FechaRecibido").data("kendoDatePicker").value(data.FECHARECIBE);
            $("#Tara").val(data.TARA);
            $("#PesoBruto").val(data.PESOBRUTO);
            $("#PesoNeto").val(data.PESOTOTAL);
            $("#idOperarioEntrega").val(data.OPERARIOENTREGA);
            $("#OperarioEntrega").val(data.NOMBREOPERARIOENTREGA);
            $("#Observaciones").val(data.OBSERVACIONES);
            $("#LoteMaterial").val(data.LOTEMATERIAL);



        }, errorHandler, JSON.stringify(filter));

        request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/MostrarUsuario", function (data) {

            $("#OperarioRecibe").val(data.Nombre);
            $("#idOperarioRecibe").val(data.CodigoUsuario);
        }, errorHandler, null);
    }
}

function consultaPPREnBodega(dataItem) {
   
    
    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");
    $("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
    $("#codMaterial").val(dataItem.CODIGOMATERIAL);
    $("#Producto").val(dataItem.NOMBREPRODUCTO);
   


    LimpiarDatosPPR();
    LlamarPopupPPR($("#PopupNuevoPPR27"));



    var filter = {
        CODIGOORDENPRODUCCION: $("#ordenProduccion").val(),
        MOMENTO: $('input[name="Status"]:checked').val(),
        CODIGOPRODUCTO: $("#codProducto").val(),
        CODIGOMATERIAL: dataItem.CODIGOMATERIAL,
    }



    request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/BuscaformatoBodega", function (data) {

        $("#NAnalisis").val(data.ANALISIS);
        $("#NRecipiente").val(data.RECIPIENTE);
        $("#Consecutivo").val(data.CONSECUTIVORECIPIENTE);
        $("#LoteProducto").val(data.LOTEPRODUCTO);
        $("#FechaPesaje").data("kendoDatePicker").value(data.FECHAPESAJE);
        $("#FechaEntrega").data("kendoDatePicker").value(data.FECHAENTREGA);
        $("#FechaRecibido").data("kendoDatePicker").value(data.FECHARECIBE);
        $("#Tara").val(data.TARA);
        $("#PesoBruto").val(data.PESOBRUTO);
        $("#PesoNeto").val(data.PESOTOTAL);
        
        $("#idOperarioRecibe").val(data.OPERARIORECIBE);
        $("#idOperarioEntrega").val(data.OPERARIOENTREGA);
        $("#OperarioEntrega").val(data.NOMBREOPERARIOENTREGA);
        $("#OperarioRecibe").val(data.NOMBREOPERARIORECIBE);
        $("#Observaciones").val(data.OBSERVACIONES);
        $("#LoteMaterial").val(data.LOTEMATERIAL);

    }, errorHandler, JSON.stringify(filter))



}

function consultarOperacionActual(filtro)


{
    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CantidadOperacionesHechas", function (data) {


        if (data != null)
        {
            $("#OperacionActual").val(data.value);

        }

    }, errorHandler, JSON.stringify(filtro));

}

function consultarOperacionBase(filtro) {
    debugger;
    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultarOperacionBase", function (data) {


        if (data != null) {
            $("#OpBase").val(data.value);

        }

    }, errorHandler, JSON.stringify(filtro));
}

function consultarOperacionATrabajar(filtro) {
    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CantidadOperacionesHechas", function (data) {

        
        if (data != null) {
            $("#OperacionActual").val(data.value + 1);
        }

    }, errorHandler, JSON.stringify(filtro));

}

function consultarTemperaturaSipsada()
{

    ////request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs(" + ordenProduccion + ")", function (data) {
    //request("application/json", null, null, "POST", baseUrl + "FORMATOPPR01/ConsultarTemperaturas", function (data) {
    //    var a = $.parseJSON(data.value);
    //    var tempsup = parseFloat(a["Temperatura Superior"]);
    //    var tempinf = parseFloat( a["Temperatura Inferior"]);

    //    $("#Temperatura").html("Temperatura Superior Troquel :" + tempsup.toFixed(2) + ", Temperatura Inferior Troquel " + tempinf.toFixed(2));
    //}, errorHandler);


}

function consultaPPRLoteYNroAnalisis(dataItem) {


    //$("#NombreMaterial").attr("disabled", "disabled");
    //$("#Producto").attr("disabled", "disabled");
    //$("#NombreMaterial").val(dataItem.NOMBREMATERIAL);
    //$("#codMaterial").val(dataItem.CODIGOMATERIAL);
    //$("#Producto").val(dataItem.NOMBREPRODUCTO);



    //LimpiarDatosPPR();
    //LlamarPopupPPR($("#PopupNuevoPPR27"));



    var filter = {
        CODIGOORDENPRODUCCION: $("#ordenProduccion").val(),
        MOMENTO: 1,
        CODIGOPRODUCTO: $("#codProducto").val(),
        CODIGOMATERIAL: dataItem.CODIGOMATERIAL,
    }



    request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/BuscaformatoBodega", function (data) {

        debugger;
        if (data != null)
        {
            $("#NAnalisis").val(data.ANALISIS);
            //$("#NRecipiente").val(data.RECIPIENTE);
            //$("#Consecutivo").val(data.CONSECUTIVORECIPIENTE);
            $("#LoteProducto").val(data.LOTEPRODUCTO);
            //$("#FechaPesaje").data("kendoDatePicker").value(data.FECHAPESAJE);
            //$("#FechaEntrega").data("kendoDatePicker").value(data.FECHAENTREGA);
            //$("#FechaRecibido").data("kendoDatePicker").value(data.FECHARECIBE);
            //$("#Tara").val(data.TARA);
            //$("#PesoBruto").val(data.PESOBRUTO);
            //$("#PesoNeto").val(data.PESOTOTAL);
            //$("#idOperarioRecibe").val(data.OPERARIORECIBE);
            //$("#idOperarioEntrega").val(data.OPERARIOENTREGA);
            //$("#OperarioEntrega").val(data.NOMBREOPERARIOENTREGA);
            //$("#OperarioRecibe").val(data.NOMBREOPERARIORECIBE);
            //$("#Observaciones").val(data.OBSERVACIONES);
            $("#LoteMaterial").val(data.LOTEMATERIAL);
        }
    }, errorHandler, JSON.stringify(filter))



}

function consultaOrdenPPRHimalaya() {

    $("#gridMateriaPrima").hide();

    $("#ErrorEditar").html("");

    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Ingrese El Numero de Orden de Produccion a Consultar", "red");
        $("#CodProducto").val("");
        $("#Producto").val("");
        return;
    }


    var ordenProduccion = $("#OrdenProduccion").val();

    if (ordenProduccion == undefined) {
        ordenProduccion = $("#ordenProduccion").val();
    }

    var filter = {
        CODIGOORDENPRODUCCION: ordenProduccion
    }

    //request("application/json", null, null, "GET", baseUrl + "FORMATOPPRs(" + ordenProduccion + ")", function (data) {
    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultarDatosOPIniciarFabricacion", function (data) {


        if ($("#gridControlProceso") != undefined) {
            if ($("#gridControlProceso").data("kendoGrid") != null) {
                $("#gridControlProceso").kendoGrid({ dataSource: null })
            }
        }



        if (data.value == 0) {
            showModal("Sistema Cadivi", "La Orden a Consultar No Ha Sido Creada", "red");
            $("#CodProducto").val("");
            $("LoteProducto").val("");
            $("#Producto").val("");
            $("#TotalProduccion").val("");
        }
        else {
            $("#CodProducto").val(data.value[0]["CODIGOPRODUCTOS"]);
            $("#Producto").val(data.value[0]["NOMBREPRODUCTO"]);
            $("#TotalProduccion").val(data.value[0]["CANTIDAD"]);
        }

    }, errorHandler, JSON.stringify(filter))

   

    //setInterval(function () {
    //    consultarTemperaturaSipsada();
    //}, 3000);

}


function consultarOperacionActualHimalaya(filtro) {

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CantidadOperacionesHechas", function (data) {


        if (data != null) {
            $("#NumeroBatch").val(data.value);

        }

    }, errorHandler, JSON.stringify(filtro));

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ConsultaPesoBatch", function (data) {

        if (data != null) {
            a = data.value;
        }

    }, errorHandler, JSON.stringify(filtro))

}

function consultarOperacionATrabajarHimalaya(filtro) {
    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CantidadOperacionesHechas", function (data) {


        if (data != null) {
            $("#NumeroBatch").val(data.value + 1);
        }

    }, errorHandler, JSON.stringify(filtro));

}

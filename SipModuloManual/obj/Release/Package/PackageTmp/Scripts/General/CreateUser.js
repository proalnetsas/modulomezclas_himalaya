﻿var baseUrl = "../odata/";
var baseUrlPage = window.location.origin;

$(document).ready(function () {
    if (window.location.pathname === "/Home/Registrousser") {
        getUsuarios();
        getRoles();
    }
    
});

function getUsuarios() {


    request("application/json", null, null, "POST", baseUrl + "USUARIOS/GetPersonalSinUsuario", function (data) {

        if ($("#dropUsuarios").val() == null) {
            for (var i = 0; i < data.value.length; i++) {
                var id = data.value[i].CodigoPersonal;
                var name = data.value[i].NombrePersonal;

                $("#dropUsuarios").append("<option value='" + id + "'>" + name + "</option>");
            }
        }

    }, errorHandler, null);

}

function getRoles() {

    request("application/json", null, null, "POST", baseUrl + "USUARIOS/GetRolesParaUsuario", function (data) {

        if ($("#dropRoles").val() == null) {
            for (var i = 0; i < data.value.length; i++) {
                var id = data.value[i].IdRol;
                var name = data.value[i].Rol;

                $("#dropRoles").append("<option value='" + id + "'>" + name + "</option>");
            }
        }

    }, errorHandler, null);
}

function crearUsuarioXRol() {

    if ($("#txtUserName").val() == "") {

        showModal("Sistema Cadivi", "Ingrese un nombre de Usuario", "red")
        return;
    }
    if ($("#txtPassword").val() == "") {
        showModal("Sistema Cadivi", "Ingrese una contraseña", "red")
        return;
    }

    if ($("#txtConfirmPassword").val() == "") {
        showModal("Sistema Cadivi", "Por favor confirme su contraseña", "red")
        return;
    }


    if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
        showModal("Sistema Cadivi", "Las contraseñas no coninciden", "red")
        return;
    }

    var rol = {
        LOGIN: $("#txtUserName").val(),
        IDROL: parseInt($("#dropRoles").val()),
        PASSWORD: $("#txtPassword").val(),
        CODIGOPERSONAL: $("#dropUsuarios").val(),
        ROL: $("#dropRoles :selected").text()
    }

    request("application/json", null, null, "POST", baseUrl + "USUARIOS/CrearUsuarioXRol", function (data) {
        
        showModal("Sistema Cadivi", "Datos Almacenados Con Exito", 'blue')
        $("#txtUserName").val("");
        $("#txtPassword").val("");
        $("#txtConfirmPassword").val("");
        $("#dropUsuarios :selected").remove();
        getUsuarios();
        getRoles();

    }, errorHandler, JSON.stringify(rol))

}

function crearPersonal() {

    if ($("#txtCodPersonal").val() == "") {

        showModal("Sistema Cadivi", "Ingrese un Codigo de Personal", "red")
        return;
    }
    if ($("#txtCedula").val() == "") {
        showModal("Sistema Cadivi", "Ingrese una Cedula", "red")
        return;
    }


    var personal = {
        CODIGOPERSONAL: $("#txtCodPersonal").val(),
        CEDULA: $("#txtCedula").val(),
        NOMBRE: $("#txtNombre").val(),
        APELLIDOS: $("#txtApellidos").val(),
        SEXO: $("#dropSexo :selected").text(),
        FECHANACIMIENTO: $("#datepicker").val()

    }
    debugger;
    request("application/json", null, null, "POST", baseUrl + "USUARIOS/CrearPersonal", function (data) {

        showModal("Sistema Cadivi", "Datos Almacenados Con Exito", 'blue')
        $("#txtCodPersonal").val("");
        $("#txtCedula").val("");
        $("#txtNombre").val("");
        $("#txtApellidos").val("");
        $("#datepicker").val("");
        debugger;
    }, errorHandler, JSON.stringify(personal))

}


function getUsuarioAEditar(e) {

    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    $("#selectedUser").val(dataItem.Login);
    $("#selectedPassword").val(dataItem.Password);
    $("#txtConfirmPassword").val(dataItem.Password);
    $("#selectedPCode").val(dataItem.CodigoPersonal);

        request("application/json", null, null, "POST", baseUrl + "USUARIOS/GetRolesParaUsuario", function (data) {

            if ($("#selectedRole").val() == null) {

                for (var i = 0; i < data.value.length; i++) {
                    var id = data.value[i].IdRol;
                    var name = data.value[i].Rol;

                    $("#selectedRole").append("<option value='" + id + "'>" + name + "</option>");

                    if (name == dataItem.Rol) {

                        $("#selectedRole").get(0).selectedIndex = i;

                    }

                }
                
            }

        }, errorHandler, null);

    LlamarPopupPPR($("#PopupEditar"));

    if ($('#selectedRole').find(":selected").text() != dataItem.Rol) {

        $("#selectedRole").get(0).selectedIndex = $("#selectedRole option:contains(" + dataItem.Rol + ")").attr('selected', 'selected');

    }




}

function confirmDelete() {

    showConfirm('Eliminar Usuario', '<p><h2>¿Esta seguro que desea eliminar este usuario?</h2></p>', function (e) {
        e.sender.close(e.sender.element);
    }, function () {
        generateGridAdminUsers();
    });
}

function ActualizarUsuario(e) {

    if ($("#selectedUser").val() == "") {

        showModal("Sistema Cadivi", "Ingrese un nombre de Usuario", "red")
        return;
    }
    if ($("#selectedPassword").val() == "") {
        showModal("Sistema Cadivi", "Ingrese una contraseña", "red")
        return;
    }

    if ($("#txtConfirmPassword").val() == "") {
        showModal("Sistema Cadivi", "Por favor confirme su contraseña", "red")
        return;
    }

    if ($("#selectedPCode").val() == "") {
        showModal("Sistema Cadivi", "Por favor ingrese un Codigo de Personal", "red")
    }

    if ($("#selectedPassword").val() != $("#txtConfirmPassword").val()) {
        showModal("Sistema Cadivi", "Las contraseñas no coninciden", "red")
        return;
    }

    var usuario = {
        LOGIN: $("#selectedUser").val(),
        IDROL: parseInt($("#selectedRole").val()),
        PASSWORD: $("#selectedPassword").val(),
        CODIGOPERSONAL: $("#selectedPCode").val(),
        FECHACREACION: dataItem.Fecha_Creacion,
        ROL: $("#selectedRole :selected").text()
    }

    request("application/json", null, null, "POST", baseUrl + "USUARIOS/ActualizarUsuario", function (data) {

        CerrarPopup($("#PopupEditar"));
        showModal("Sistema Cadivi", "El usuario ha sido actualizado correctamente", "blue");

    }, errorHandler, JSON.stringify(usuario));

    generateGridAdminUsers();

}

function getPersonalAEditar(e) {

    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    var fecha = new Date(dataItem.Fecha_Nacimiento);

    $("#selectedCodPersonal").val(dataItem.CodigoPersonal);
    $("#selectedCedula").val(dataItem.Cedula);
    $("#selectedNombres").val(dataItem.Nombres);
    $("#selectedApellidos").val(dataItem.Apellidos);
    $("#selectedNacimiento").val(fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear());

    if (dataItem.Sexo == "M" || dataItem.Sexo == "Masculino") {
        $("#selectedSexo").val(1);
    }
    else {

        $("#selectedSexo").val(2);
    }

    LlamarPopupPPR($("#PopupEditar"));

}

function ActualizarPersonal(e) {

    if ($("#selectedNombres").val() == "") {
        showModal("Sistema Cadivi", "Ingrese un nombre para el personal", "red")
        return;
    }

    if ($("#selectedApellidos").val() == "") {
        showModal("Sistema Cadivi", "Ingrese los apellidos del personal", "red")
    }
    var fecha = $("#selectedNacimiento").val().split("/")
    var nacimiento = new Date(fecha[2], fecha[1] - 1, fecha[0])


    var personal = {
        CODIGOPERSONAL: $("#selectedCodPersonal").val(),
        CEDULA: $("#selectedCedula").val(),
        NOMBRES: $("#selectedNombres").val(),
        SEXO: $("#selectedSexo :selected").text(),
        APELLIDOS: $("#selectedApellidos").val(),
        FECHANACIMIENTO: nacimiento

    }

    request("application/json", null, null, "POST", baseUrl + "USUARIOS/ActualizarPersonal", function (data) {

        CerrarPopup($("#PopupEditar"));
        showModal("Sistema Cadivi", "El personal ha sido actualizado correctamente", "blue");

    }, errorHandler, JSON.stringify(personal));

    generateGridPersonal();

}

function eliminarPersonal(e)
{
    debugger;
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    var personal = {

        CODIGOPERSONAL: dataItem.CodigoPersonal,
        CEDULA: dataItem.Cedula,
        NOMBRES: dataItem.Nombres,
        SEXO: dataItem.Sexo,
        APELLIDOS: dataItem.Apellidos,
        FECHANACIMIENTO: dataItem.Fecha_Nacimiento
    }

    request("application/json", null, null, "POST", baseUrl + "USUARIOS/DeletePersonal", function (data) {

        showModal("Sistema Cadivi", "El usuario ha sido eliminado", "blue");

    }, errorHandler, JSON.stringify(personal));

    generateGridPersonal();

}

function eliminarUsuario(e) {

    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    var rol = {
        LOGIN: dataItem.Login,
        IDROL: dataItem.IdRol,
        PASSWORD: dataItem.Password,
        CODIGOPERSONAL: dataItem.CodigoPersonal,
        ROL: dataItem.Rol
    }


    request("application/json", null, null, "POST", baseUrl + "USUARIOS/DeleteUsuarioXRol", function (data) {

        showModal("Sistema Cadivi", "El usuario ha sido eliminado", "blue");
        
    }, errorHandler, JSON.stringify(rol));

    generateGridAdminUsers();
    
}

function errorHandler(jqXHR, exception) {
    debugger;
    var txt = '';
    if (jqXHR.responseJSON) {
        if (jqXHR.responseJSON["odata.error"]) {
            var error = '';
            if (jqXHR.responseJSON["odata.error"].message) {
                error = jqXHR.responseJSON["odata.error"].message.value;
            }
            if (jqXHR.responseJSON["odata.error"].innererror) {
                error = error + " " + jqXHR.responseJSON["odata.error"].innererror.message;
            }
            if (error != "")
                txt = error;
            else
                txt = 'Error no controlado.\n' + jqXHR.responseText;
        }
        else
            txt = 'Error no controlado.\n' + jqXHR.responseText;
    }
    else
        txt = 'Error no controlado.\n' + jqXHR.responseText;
    //}

    showModal("Sistema Cadivi", txt, "red");
}
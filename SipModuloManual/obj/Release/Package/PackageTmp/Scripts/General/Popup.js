﻿function showModal(Titulo, Message, Color) {
    
    var _old = $('.modalWrapper.modal');
    if (_old.length > 0) {
        _old.remove();
    }


    var div = document.createElement("div");
    document.body.appendChild(div);
    $(div).css("display", "none");
    $(div).html('<div class="modalWrapper modal"><div class="modalWrapperParent" style="background:'+ Color +'" align="center"><h2 class="panel-title" >' + Titulo + '</h2></div><div class="modalWrapperContent"><p style="font-size: 14px">' + Message + '</p><div align="right"><button onClick="evt_closeModal(this)" class="btn btn-modal">Aceptar</button></div></div></div>');
    $(div).show("slow");

    var h = $(div).find('.modalWrapperContent').outerHeight();
    h = parseInt(h) + 40;
    $(div).find('.modalWrapper.modal').height(h + 'px');
    

    return div.children[0].children[1].children[1].children[0];
}

function evt_closeModal(sender) {
    var parent = $(sender).parent().parent().parent().parent();
    $(parent).hide('slow', function () { $(parent).remove(); });
}

function showForm(Titulo, Cuerpo, onAccept, onCancel) {

    var _old = $('.modalWrapper.form');
    if (_old.length > 0) {
        _old.remove();
    }
    var div = document.createElement("div");
    document.body.appendChild(div);
    $(div).css("display", "none");
    $(div).html('<div class="modalWrapper form"><div class="modalWrapperParent" align="center"><h2 class="panel-title">' + Titulo + '</h2></div><div class="modalWrapperContent"><div>' + Cuerpo + '</div><div align="right"><button id="wrapContentFormAccept" class="btn btn-modal">Aceptar</button><button id="wrapContentFormCancel" style="margin-left:3px;" class="btn btn-modal">Cancelar</button></div></div><div>');
    $(div).show("slow");
    $(document.getElementById('wrapContentFormAccept')).click(function () {
        var data = { sender: { close: evt_closeModal, element: this } };
        onAccept(data);
    });
    $(document.getElementById('wrapContentFormCancel')).click(function () {
        evt_closeModal(document.getElementById('wrapContentFormCancel'));
        onCancel();
    });
    return div.children[0].children[1].children[1].children[0];
}

function showConfirm(Titulo, Message, onAccept, onCancel) {

    var _old = $('.modalWrapper.confirm');
    if (_old.length > 0) {
        _old.remove();
    }
    var div = document.createElement("div");
    document.body.appendChild(div);
    $(div).css("display", "none");
    $(div).html('<div class="modalWrapper confirm"><div class="modalWrapperParent" align="center"><h2 class="panel-title">' + Titulo + '</h2></div><div class="modalWrapperContent"><p>' + Message + '</p><div align="right"><button id="wrapContentConfirmAccept" class="btn btn-modal">Aceptar</button><button id="wrapContentConfirmCancel" style="margin-left:3px;" class="btn btn-modal">Cancelar</button></div></div><div>');
    $(div).show("slow");
    $(document.getElementById('wrapContentConfirmAccept')).click(function () {
        var data = { sender: { close: evt_closeModal, element: this } };
        onAccept(data);
    });
    $(document.getElementById('wrapContentConfirmCancel')).click(function () {
        evt_closeModal(document.getElementById('wrapContentConfirmCancel'));
        onCancel();
    });
    return div.children[0].children[1].children[1].children[0];
}
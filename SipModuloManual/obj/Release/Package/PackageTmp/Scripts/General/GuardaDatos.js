﻿function GuardarFormatoPPR()
{
    if( $("#Consecutivo").val() == "")
    {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Numero de Consecutivo del Recipiente", "red");
        return;
    }
    

    if( $("#LoteProducto").val()== "")
    {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Numero de Lote de Producto", "red");
        return;
    }

    if($("#Tara").val() == "" || $("#Tara").val() == "0")
    {   
        showModal("Sistema Cadivi", "Ingrese Un Valor de Tara Mayor a 0", "red");
        return;
    }

    if( $("#PesoBruto").val() == "" || $("#PesoBruto").val() == "0" )
    {
        showModal("Sistema Cadivi", "PorIngrese Un Valor de Peso Bruto Mayor a 0", "red");
        return;
    }

    if($("#PesoNeto").val() == "" || $("#PesoNeto").val() == "0")
    {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Peso Diferente a 0", "red");
        return;
    }
    

    var fechaPesaje = $("#FechaPesaje").data("kendoDatePicker").value();
    var fechaEntrega = $("#FechaEntrega").data("kendoDatePicker").value();
    var fechaRecibe = $("#FechaRecibido").data("kendoDatePicker").value();

    if ($("#idOperarioEntrega").val() == '')
        $("#idOperarioEntrega").val('9999');
    
    

    var formatoPPR =
    {
        ANALISIS: $("#NAnalisis").val(),
        RECIPIENTE: $("#NRecipiente").val(),
        CONSECUTIVORECIPIENTE: $("#Consecutivo").val(),
        CODIGOMATERIAL: $("#codMaterial").val(),
        LOTEPRODUCTO: $("#LoteProducto").val(),
        //FECHAPESAJE: $("#FechaPesaje").val(),
        FECHAPESAJE: kendo.toString(fechaPesaje, "yyyy-MM-dd"),
        TARA: $("#Tara").val(),
        PESOBRUTO: $("#PesoBruto").val(),
        PESOTOTAL: $("#PesoNeto").val(),
        OPERARIOENTREGA: $("#idOperarioEntrega").val(),
        FECHAENTREGA: kendo.toString(fechaEntrega, "yyyy-MM-dd"),
        OPERARIORECIBE: "9999",
        FECHARECIBE: kendo.toString(fechaRecibe, "yyyy-MM-dd"),
        OBSERVACIONES: $("#Observaciones").val(),
        MOMENTO: $('input[name="Status"]:checked').val(),
        IMPRESO: false,
        CODIGOORDENPRODUCCION: $("#ordenProduccion").val(),
        CODIGOPRODUCTO: $("#codProducto").val(),
        LOTEMATERIAL: $("#LoteMaterial").val(),
    }


    request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/CrearFormatoPPR", function (data) {

        CerrarPopup($("#PopupNuevoPPR27"));
        showModal("Sistema Cadivi", "Datos Guardados Con Exito", "blue");
        
        LimpiarDatosPPR();

        if ($("#gridOrdenProduccion").val() != undefined)
            llenargridMateriales();
        if ($("#gridOrdenProduccion").val() == undefined)
            consultarOrdenMezclado();

    }, errorHandler, JSON.stringify(formatoPPR))

    

}

function GuardarFormatoToc()
{

    $("#ErrorCrearToc").html("");


    if ($("#ListaMateriales").val() === "")
    {
        showModal("Sistema Cadivi", "Seleccione La Materia Prima", "red");
        return;
    }


    if ($("#Peso").val() == "" || $("#Peso").val() == "0")
    {
        showModal("Sistema Cadivi", "Ingrese un Peso Valido o Valida la Conexion a la Bascula", "red")
        return;
    }

    
    var formatoTOC = {
        LOTE: $("#LoteProducto").val(),
        CODIGOPRODUCTOS: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        CODIGOTURNO: "1",
        LOGIN: "9999",
        NROOPERACION: $("#OperacionActual").val(),
        ORDENPRODUCCIOBASE: $("#OpBase").val(),
        KILOGRAMOS: parseFloat($("#PesoKg").val()),
        HORA: $("#Hora").val(),
        MINUTOSMEZCLA: parseInt($("#MinutosMezcla").val()),
        BASE_SILO: $("#TipoEmpaque").is(':checked'),
        LAMINADAS: parseInt(0),
        CODMATERIAL: $("#ListaMateriales").val(),
        CANTIDAD: parseFloat($("#Peso").val()),
        PESOMANUAL: $("#IngresoPeso").is(':checked'),
        FECHA: $("#FechaIngreso").val()
    }


   

    $("#btnGuardarFormato").prop("disabled", true);

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearOperacionTrabajo", function (data) {
        showModal("Sistema Cadivi", "Datos Guardados Con Exito!", "blue")
        llenargridMateriaPrima();
        $("#Peso").val("0");
        $("#CodMaterial").val("");
        $("#ListaMateriales").data("kendoDropDownList").value("");
        $("#ErrorCrearToc").html("");
        $("#btnGuardarFormato").prop("disabled", false);




        var filter = {
            CODIGOPRODUCTO: $("#CodProducto").val(),
            CODIGOPUESTO: $("#MaqAsignar").val(),
            CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
        }


        llenarGridListaMateriales(filter);
    }, errorHandler, JSON.stringify(formatoTOC))


}

function GuardarFormatoPPR019(options) {

    
    var formatoPPR019 =
   {
       ID: String(options.ID),
       APLICA: Boolean(options.APLICA),
       RESPUESTA: Boolean(options.RESPUESTA),
       OBSERVACIONES: options.OBSERVACIONES,
    }

    return formatoPPR019;   
}

function crearNuevaOperacion() {
    
    var filter =
    {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }

    
    consultarOperacionATrabajar(filter);
    var f = new Date();


    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora = f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();


    $("#Hora").val(hora + ":" + min);

    //var ventanaconfirmacion = window.confirm("Desea Crear Una Nueva Operacion ?")
    //if (ventanaconfirmacion) {

    //}
    //else
    //    consultarOperacionActual(filter)



    showConfirm('¿Está Seguro?', '<p><h2>¿Esta Seguro Que Desea Crear Una Nueva Operacion ?</h2></p>', function (e) {
        e.sender.close(e.sender.element);
    }, function () {
        consultarOperacionActual(filter);
    });


}

function AprobarFormatoPPR() {
    var formatoPPR =
    {
        CODIGOMATERIAL: $("#codMaterial").val(),
        OPERARIORECIBE: $("#idOperarioRecibe").val(),
        MOMENTO: $('input[name="Status"]:checked').val(),
        CODIGOORDENPRODUCCION: $("#ordenProduccion").val(),
        CODIGOPRODUCTO: $("#codigoProducto").val(),
    }


    request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/AutorizarPPr", function (data) {
        showModal("Sistema Cadivi", "Datos Guardados Con Exito", 'blue');
        CerrarPopup($("#PopupNuevoPPR27"));
        LimpiarDatosPPR();
        consultarOrdenMezclado();
    }, errorHandler, JSON.stringify(formatoPPR))
}

function GuardarFormatoPPR01()
{
    if ($("#ListaHora").val() == "")
    {
        showModal("Sistema Cadivi", "Por Favor Seleccione la Hora de la Muestra", 'red');
        return;
    }
    
    if ($("#MuestraN1").val() == "") {

        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 1", 'red');
        return;
    }


    if ($("#MuestraN2").val() == "") {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 2", 'red');
        return;
    }

    if ($("#MuestraN3").val() == "") {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 3", 'red');
        return;
    }

    if ($("#MuestraN4").val() == "" ) {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 4", 'red');
        return;
    }

    if ($("#MuestraN5").val() == "") {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 5", 'red');
        return;
    }

    var grid = $("#gridFormatoppr01").data("kendoGrid");
    var d = grid.dataItem(grid.select());
  
    var formatoPPR =
    {
        MUESTRA1: $("#MuestraN1").val(),
        MUESTRA2: $("#MuestraN2").val(),
        MUESTRA3: $("#MuestraN3").val(),
        MUESTRA4: $("#MuestraN4").val(),
        MUESTRA5: $("#MuestraN5").val(),
        HORASEL: $("#ListaHora").val(),
        HORA: $("#Hora").val(),
        ID: d.ID
    }


    request("application/json", null, null, "POST", baseUrl + "FORMATOPPR01/EditarPesosPpr01", function (data) {
        showModal("Sistema Cadivi", "Datos Almacenados Con Exito", 'blue');
        CerrarPopup($("#PopupNuevoPesos"));
        ListarPesosAtributos(this);
        $("#MuestraN1").val("");
        $("#MuestraN2").val("");
        $("#MuestraN3").val("");
        $("#MuestraN4").val("");
        $("#MuestraN5").val("");
        HORASEL: $("#ListaHora").val("");


    }, errorHandler, JSON.stringify(formatoPPR))



}

function GuardarFormatoPPR01A() {

    if ($("#ListaHoraA").val() == "")
    {
        showModal("Sistema Cadivi", "Por Favor Seleccione la Hora de la Muestra", 'red');
        return;
    }

    if ($("#Envase").val() == "") {

        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 1", 'red');
        return;
    }


    if ($("#Embalaje").val() == "") {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 2", 'red');
        return;
    }

    if ($("#Envoltura").val() == "") {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 3", 'red');
        return;
    }

    if ($("#Proceso").val() == "") {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Muestra 4", 'red');
        return;
    }

    if ($("#ListaHoraA").val() == "") {
        showModal("Sistema Cadivi", "Por Favor Ingrese un Valor de Hora Selecionada", 'red');
        return;
    }


    var grid = $("#gridFormatoppr01").data("kendoGrid");
    var d = grid.dataItem(grid.select());

    var formatoPPR =
    {
        MUESTRA1: $("#Envase").val(),
        MUESTRA2: $("#Embalaje").val(),
        MUESTRA3: $("#Envoltura").val(),
        MUESTRA4: $("#Proceso").val(),
        HORASEL: $("#ListaHoraA").val(),
        HORA: $("#Hora1").val(),
        ID: d.ID
    }


    request("application/json", null, null, "POST", baseUrl + "FORMATOPPR01/EditarPesosPpr01A", function (data) {
        showModal("Sistema Cadivi", "Datos Almacenados Con Exito", 'blue');
        CerrarPopup($("#PopupNuevoAtrib"));
        LlenarGridAtributos(this);
        $("#Envase").val("");
        $("#Embalaje").val("");
        $("#Envoltura").val("");
        $("#ListaHora").data("kendoComboBox").value("");
        HORASEL: $("#ListaHora").val("");

    }, errorHandler, JSON.stringify(formatoPPR))



}

function GuardarDatosFormato(options) {

    var filter =
    {
        ID: parseInt(options.ID),
        LOTE: options.LOTE,
        OBSERVACIONES: options.OBSERVACIONES,
        PASTARECHAZADAFIN: options.PASTARECHAZADAFIN,
        PASTARECHAZADAINICIO: options.PASTARECHAZADAINICIO,
        PASTASPRODUCIDAS: options.PASTASPRODUCIDAS,
        PORCENTAJERECHAZADO: 0,//options.PORCENTAJERECHAZADO,
        CODIGOPRODUCTO: options.CODIGOPRODUCTOS,
        CODIGOPUESTO: options.CODIGOPUESTO,
        CODIGOORDENPRODUCCION: options.CODIGOORDENPRODUCCION,
        LOGIN: window.localStorage['usuario'],
        FECHA: $("#FechaIngreso").val(),
        CUMPLEGALGAS: options.CUMPLEGALGAS
    }

    return filter;
}

function ActualizarDatosFormatoDetalle() {
    //var filterdetalle = {
    //    ID: $("#ListaCondiciones").val(),
    //    VALOR: $("#ValorCondicion").val(),
    //    HORASEL: $("#ListaHora").val(),
    //    OBSERVACIONES: $("#Observaciones").val()
    //}

    var grid = $("#gridControlProceso").data("kendoGrid");
    var d = grid.dataItem(grid.select());


    debugger;
    var filterdetalles = {
        ID: d.ID,
        HORASEL: $("#ListaHora").val(),
        VELOCIDADRPM: $("#VELOCIDADrpm").val(),
        VELOCIDAD: $("#VELOCIDAD").val(),
        TEMPERATURAJABON: $("#TEMPERATURAJABON").val(),
        TEMPERATURACABEZAL: $("#TEMPERATURACABEZAL").val(),
        LONGITUD: $("#LONGITUD").val(),
        TMPTROQUELSUPERIOR: $("#TMPTROQUELSUPERIOR").val(),
        TMPTROQUELINFERIOR: $("#TMPTROQUELINFERIOR").val(),
        CICLOS: $("#CICLOS").val(),
        PRESIONSUPERIOR: $("#PRESIONSUPERIOR").val(),
        PRESIONINFERIOR: $("#PRESIONINFERIOR").val(),
        PRESIONEXPULSORES: $("#PRESIONEXPULSORES").val(),
        OBSERVACIONES: $("#Observaciones").val(),
        HORA: $("#Hora").val()
    }

 

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOC02DETALLE/EditarFormatoDetalle", function (data) {
        showModal("Sistema Cadivi", "Datos Almacenados Con Exito", 'blue');
        

        CerrarPopup($("#PopupNuevoCondicion"));
        $("#ListaHora").data("kendoComboBox").value("");
        $("#Observaciones").val("");
        $("#VELOCIDADrpm").val("");
        $("#VELOCIDAD").val("");
        $("#TEMPERATURAJABON").val("");
        $("#TEMPERATURACABEZAL").val("");
        $("#LONGITUD").val("");
        $("#TMPTROQUELSUPERIOR").val("");
        $("#TMPTROQUELINFERIOR").val("");
        $("#CICLOS").val("");
        $("#PRESIONSUPERIOR").val("");
        $("#PRESIONINFERIOR").val("");
        $("#PRESIONEXPULSORES").val("");
        LlenarGridCondiciones();


    }, errorHandler, JSON.stringify(filterdetalles))
    
}

function ActualizarFormatoPpr01PesosAtributos(options, pantalla) {

    var filteratributepeso =
    {
        ID: parseInt(options.ID),
        IDFORMATOPPR01: options.IDFORMATOPPR01,
        MUESTRA: options.MUESTRA,
        HORA1: parseFloat(options.HORA1),
        HORA2: parseFloat(options.HORA2),
        HORA3: parseFloat(options.HORA3),
        HORA4: parseFloat(options.HORA4),
        HORA5: parseFloat(options.HORA5),
        HORA6: parseFloat(options.HORA6),
        HORA7: parseFloat(options.HORA7),
        HORA8: parseFloat(options.HORA8),
        HORA9: parseFloat(options.HORA9),
        HORA10: parseFloat(options.HORA10),
        HORA11: parseFloat(options.HORA11),
        HORA12: parseFloat(options.HORA12),
        HORA13: parseFloat(options.HORA13),
        HORA14: parseFloat(options.HORA14),
        HORA15: parseFloat(options.HORA15),
        HORA16: parseFloat(options.HORA16),
        HORA17: parseFloat(options.HORA17),
        HORA18: parseFloat(options.HORA18),
        HORA19: parseFloat(options.HORA19),
        HORA20: parseFloat(options.HORA20),
        PANTALLA: pantalla
    }
    
    return filteratributepeso;
}

function GuardarDatosFormatoPPr01(otpions) {


    var filter = {
        CODIGOPRODUCTO: otpions.CODIGOPRODUCTO,
        CODIGOPUESTO: otpions.CODIGOPUESTO,
        CODIGOORDENPRODUCCION: otpions.CODIGOORDENPRODUCCION,
        LOTE: otpions.LOTE,
        LOGIN: window.localStorage['usuario'],
        ID: parseInt(otpions.ID),
        FECHA: otpions.FECHA,
        PRESENTACION: parseInt(otpions.PRESENTACION),
        PESOMINIMO: parseInt(otpions.PESOMINIMO),
    }

    return filter;
}

function crearNuevoBatch() {

    if ($("#PesoBatch").val() == "") {
        showModal("Sistema Mezclas", "Por Favor Ingrese un Peso para el Batch", 'red');
        return;
    }

    var filter =
    {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: "9999",
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
    }


    consultarOperacionATrabajarHimalaya(filter);
    var f = new Date();


    var hora, min;
    if (f.getHours() < 10)
        hora = "0" + f.getHours();
    else
        hora = f.getHours();

    if (f.getMinutes() < 10)
        min = "0" + f.getMinutes();
    else
        min = f.getMinutes();


    $("#Hora").val(hora + ":" + min);

    //var ventanaconfirmacion = window.confirm("Desea Crear Una Nueva Operacion ?")
    //if (ventanaconfirmacion) {

    //}
    //else
    //    consultarOperacionActual(filter)



    showConfirm('¿Está Seguro?', '<p><h2>¿Esta Seguro Que Desea Crear Un Nuevo Batch?</h2></p>', function (e) {
        e.sender.close(e.sender.element);
    }, function () {
        consultarOperacionActualHimalaya(filter);
    });
    a = parseFloat($("#PesoBatch").val());
    $("#PesoBatch").val("");
}


function GuardarFormatoTocHimalaya() {

    $("#ErrorCrearToc").html("");


    if ($("#ListaMateriales").val() === "") {
        showModal("Sistema Mezclas", "Seleccione La Materia Prima", "red");
        return;
    }


    if ($("#Peso").val() == "" || $("#Peso").val() == "0") {
        showModal("Sistema Mezclas", "Ingrese un Peso Valido o Valide la Conexion a la Bascula", "red")
        return;
    }


    var formatoTOC = {
        LOTE: $("#OrdenProduccion").val(),
        CODIGOPRODUCTOS: $("#CodProducto").val(),
        CODIGOPUESTO: "9999",
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        CODIGOTURNO: "1",
        LOGIN: "9999",
        NROOPERACION: $("#NumeroBatch").val(),
        ORDENPRODUCCIOBASE: $("#OrdenProduccion").val(),
        KILOGRAMOS: a,
        HORA: $("#Hora").val(),
        MINUTOSMEZCLA: parseInt(0),
        BASE_SILO: true,
        LAMINADAS: parseInt(0),
        CODMATERIAL: $("#ListaMateriales").val(),
        CANTIDAD: parseFloat($("#Peso").val()),
        PESOMANUAL: $("#IngresoPeso").is(':checked'),
        FECHA: $("#FechaIngreso").val()
    }

    $("#btnGuardarFormato").prop("disabled", true);

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearOperacionTrabajoHimalaya", function (data) {
        showModal("Sistema Cadivi", "Datos Guardados Con Exito!", "blue")
        llenargridMateriaPrimaHimalaya();
        $("#Peso").val("0");
        $("#CodMaterial").val("");
        $("#ListaMateriales").data("kendoDropDownList").value("");
        $("#ErrorCrearToc").html("");
        $("#btnGuardarFormato").prop("disabled", false);

        var filter = {
            CODIGOPRODUCTO: $("#CodProducto").val(),
            CODIGOPUESTO: "9999",
            CODIGOORDENPRODUCCION: $("#OrdenProduccion").val()
        }

        var filterProducto = {

            CODIGOPRODUCTO: $("#CodProducto").val(),
            NROOPERACION: $("#NumeroBatch").val()
        }

        LlenarGridCapturaHimalaya(filter);
        LlenarCombo($("#ListaMateriales"), filterProducto);
        $("#CantidadAPesar").val("0.00");
    }, errorHandler, JSON.stringify(formatoTOC))


}

//funcion que realiza la peticion para crear la grid dinamica
function llenarGridListaMaterialesHimalaya() {

    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());


    var grid2 = $("#gridToc01").data("kendoGrid");

    if (grid2 != null) {
        $("#gridToc01").data("kendoGrid").destroy();
        $('#gridToc01').empty();

    }


    var filter =
    {
        CODIGOPRODUCTO: d.CODIGOPRODUCTOS,
        CODIGOORDENPRODUCCION: d.CODIGOORDENPRODUCCION
    }

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearReporteDinamycTOCHimalaya", function (data) {

        if (data != null) {
            generateGridCapturaHimalaya(JSON.parse(data.value))
        }
    }, errorHandler, JSON.stringify(filter));


};
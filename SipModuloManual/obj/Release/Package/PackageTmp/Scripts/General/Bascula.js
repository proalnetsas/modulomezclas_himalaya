﻿var connection;

$(document).ready(function () {
    Bascula();
});



function PararComunicacion()
{
    connection.stop();
}

function ReactivarComunicacion()
{
    connection.start();
    connection.start();
}


function Bascula()
{

    //Se crea la conexion al puerto donde se expone el valor del peso
    connection = $.hubConnection("http://localhost:9992");
    //Se crea el clienthub
    proxyBascula = connection.createHubProxy("clientHub");
    //Se inicia la conexion
    connection.start({ logging: true })

    //se colaca en modo automatico el clienhub
    proxyBascula.on('data', function (dato) {

        if (dato != null) {

            var bruto = dato.Valor;

            if(($("#Tara").val()  != undefined))
            {
                if ($("#Tara").val() != "")
                {
                    var tara = $("#Tara").val();
                    var neto = parseFloat(bruto).toFixed(2) - parseFloat(tara);
                    $("#PesoNeto").val(neto.toFixed(2));
                }
                
                $("#PesoBruto").val(bruto);
            }

            if($("#Peso").val() != undefined)
            {
                $("#Peso").val(bruto/1000);
            }

            ///Validacion para pintar el campo peso
            var cantidadAPesar = parseFloat($("#CantidadAPesar").val());
            var pesoTiempoReal = parseFloat($("#Peso").val());


            if (pesoTiempoReal == cantidadAPesar) {

                document.getElementById("Peso").style.backgroundColor = "Green";
                $("#btnGuardarFormato").attr("disabled", false);
            }
            else if ((pesoTiempoReal > (cantidadAPesar - 0.1) && pesoTiempoReal < cantidadAPesar) || ((pesoTiempoReal < (cantidadAPesar + 0.1)) && pesoTiempoReal > cantidadAPesar) || (pesoTiempoReal == (cantidadAPesar - 0.1)) || (pesoTiempoReal == (cantidadAPesar + 0.1))) {

                document.getElementById("Peso").style.backgroundColor = "Yellow";
                $("#btnGuardarFormato").attr("disabled", false);
            }
            else if (pesoTiempoReal < (cantidadAPesar - 0.1) || pesoTiempoReal > (cantidadAPesar + 0.1)) {
                document.getElementById("Peso").style.backgroundColor = "Red";

                $("#btnGuardarFormato").attr("disabled", true);
            }

            
            //document.getElementById("PesoBruto").value = Bruto;
        }
    });
}
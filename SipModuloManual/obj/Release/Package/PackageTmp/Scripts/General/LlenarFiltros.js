﻿function ConsultaReporteppr027()
{
    var codorden, codproducto, fechadesde, fechahasta;

    
    if ($("#Selection").val() != "Vacio")
    {
        if ($("#itemfiltro").val() == "") {
            showModal("Sistema Cadivi", "Error Consultando: Ingrese El Item para Consultar", 'red');
            return;
        }
        else
        {
            if ($("#Selection").val() == "Orden") {
                codorden = $("#itemfiltro").val();
                codproducto = "";
            }

            if ($("#Selection").val() == "Producto") {
                codproducto = $("#itemfiltro").val();
                codorden = "";
            }

        }
    }
    else
    {
        codorden = "";
        codproducto = "";
    };


    fechadesde = $("#fechadesde").val();
    fechahasta = $("#fechahasta").val();



    var filter =
    {
        CODIGOORDENPRODUCCION: codorden,
        CODIGOPRODUCTO: codproducto,
        FECHADESDE: fechadesde,
        FECHAHASTA: fechahasta,
    }



    //if ($("#gridPpr027").data("kendoGrid") != undefined)
    //    $("#gridPpr027").data("kendoGrid").dataSource.data([]);


    request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/ReportePPR027", function (data) {

        var json = $.parseJSON(data.value);

        $("#gridPpr027").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success(json);

                    }
                },
                schema:
                {
                    model:
                    {
                        fields:
                        {
                            CODIGOORDENPRODUCCION: { type: "number" },
                            NOMBREPRODUCTO: { type: "string" },
                            ANALISIS: { type: "number" },
                            NOMBREMATERIAL: { type: "string" },
                            APROBADO: { type: "string" },
                            IMPRESO: { type: "string" },
                            OPERARIOENTREGA: { type: "string" },
                            FECHAENTREGA: { type: "string" },
                            OPERARIORECIBE: { type: "string" },
                            FECHARECIBE: { type: "string" },
                            FECHAPESAJE: { type: "string" },
                            TARA: { type: "string" },
                            PESOTOTAL: { type: "string" },
                            PESOBRUTO: { type: "string" }
                        }
                    },
                    data: function (data) {
                        return data;
                    },
                    total: function (data) {
                        var total = data.length;

                        return total;
                    }
                },
                pageSize: 15
            }),
            selectable: "row",
            height: 610,
            pageable: true,
            scrollable: true,
            columns: [
                { title: "ORDEN PRODUCCION", field: "CODIGOORDENPRODUCCION" },
                { title: "PRODUCTO", field: "NOMBREPRODUCTO" },
                { title: "ANALISIS", field: "ANALISIS"},
                { title: "MATERIAL", field: "NOMBREMATERIAL" },
                { title: "IMPRESO", field: "IMPRESO" },
                { title: "OPERARIO ENTREGA", field: "OPERARIOENTREGA" },

                {
                    title: "FECHAENTREGA", field: "FECHAENTREGA",  template: function (data) {
                        var today = new Date(parseInt(data.FECHAENTREGA.replace('/Date(', ''))).toISOString().slice(0, 10);
                        return (today);
                    }
                },
                { title: "OPERARIO RECIBE", field: "OPERARIORECIBE" },
                {
                    title: "FECHARECIBE", field: "FECHARECIBE",  template: function (data) {
                        var today = new Date(parseInt(data.FECHARECIBE.replace('/Date(', ''))).toISOString().slice(0, 10);
                        return (today);
                    }
                },
                {
                    title: "FECHAPESAJE", field: "FECHAPESAJE", template: function (data) {
                        var today = new Date(parseInt(data.FECHAPESAJE.replace('/Date(', ''))).toISOString().slice(0, 10);
                        return (today);
                    }
                },
                { title: "TARA", field: "TARA" },
                { title: "PESO TOTAL", field: "PESOTOTAL" },
                { title: "PESO BRUTO", field: "PESOBRUTO" }
            ]
            
        });



    }, errorHandler, JSON.stringify(filter));


}


function ConsultaReporteToc01()
{
    var codorden, codproducto, fechadesde, fechahasta;


    if ($("#Selection").val() != "Vacio") {
        if ($("#itemfiltro").val() == "") {
            showModal("Sistema Cadivi", "Error Consultando: Ingrese El Item para Consultar", 'red');
            return;
        }
        else {
            if ($("#Selection").val() == "Orden") {
                codorden = $("#itemfiltro").val();
                codproducto = "";
            }

            if ($("#Selection").val() == "Producto") {
                codproducto = $("#itemfiltro").val();
                codorden = "";
            }

        }
    }
    else {
        codorden = "";
        codproducto = "";
    };


    fechadesde = $("#fechadesde").val();
    fechahasta = $("#fechahasta").val();



    var filter =
    {
        CODIGOORDENPRODUCCION: codorden,
        CODIGOPRODUCTO: codproducto,
        FECHADESDE: fechadesde,
        FECHAHASTA: fechahasta,
    }

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/ReporteToc01Orden", function (data) {

        var json = $.parseJSON(data.value);

        $("#gridOrden").kendoGrid({

            dataSource: new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success(json);

                    }
                },
                schema:
                {
                    model:
                    {
                        fields:
                        {
                            CODIGOORDENPRODUCCION: { type: "string" },
                            CODIGOPRODUCTOS: { type: "string" },
                            NOMBREPRODUCTO: { type: "string" },
                        }
                    },
                    data: function (data) {
                        return data;
                    },
                    total: function (data) {
                        var total = data.length;

                        return total;
                    },
                },
                pageSize: 5,
            }),
            selectable: "row",
            change: llenarGridListaMaterialesReporteToc01,
            height: 210,
            pageable: true,
            scrollable: true,
        });



    }, errorHandler, JSON.stringify(filter));

}

function ConsultaReportePpr019()
{
    var codorden, codproducto, fechadesde, fechahasta;


    if ($("#Selection").val() != "Vacio") {
        if ($("#itemfiltro").val() == "") {
            showModal("Sistema Cadivi", "Error Consultando: Ingrese El Item para Consultar", 'red');
            return;
        }
        else {
            if ($("#Selection").val() == "Orden") {
                codorden = $("#itemfiltro").val();
                codproducto = "";
            }

            if ($("#Selection").val() == "Producto") {
                codproducto = $("#itemfiltro").val();
                codorden = "";
            }

        }
    }
    else {
        codorden = "";
        codproducto = "";
    };


    fechadesde = $("#fechadesde").val();
    fechahasta = $("#fechahasta").val();



    var filter =
    {
        CODIGOORDENPRODUCCION: codorden,
        CODIGOPRODUCTO: codproducto,
        FECHADESDE: fechadesde,
        FECHAHASTA: fechahasta
    };


    request("application/json", null, null, "POST", baseUrl + "FORMATOPPRCONTROLs/ReportePpr019", function (data) {

        var json = $.parseJSON(data.value);

        $("#gridOrden").kendoGrid({

            dataSource: new kendo.data.DataSource({
                transport: {
                    read: function (options) {

                        options.success(json);

                    }
                },
                schema:
                {
                    model:
                    {
                        fields:
                        {
                            CODIGOORDENPRODUCCION: { type: "string" },
                            CODIGOPRODUCTOS: { type: "string" },
                            PRODUCTO: { type: "string" },
                            LOTEPRODUCTO: { type: "string" },
                            CODIGOPRODUCTOANT: { type: "string" },
                            PRODUCTOANTERIOR: { type: "string" },
                            LOTEPRODUCTOANT: { type: "string" },
                            DESINFECTANTE: { type: "string" },
                        }
                    },
                    data: function (data) {
                        return data;
                    },
                    total: function (data) {
                        var total = data.length;

                        return total;
                    },
                },
                pageSize: 5,
            }),
            selectable: "row",
            change: ConsultaDetallePpr019,
            height: 210,
            pageable: true,
            scrollable: true,
        });



    }, errorHandler, JSON.stringify(filter));
    
}

function ConsultaReportePpr01() {

    var codorden, codproducto, fechadesde, fechahasta;


    if ($("#Selection").val() != "Vacio") {
        if ($("#itemfiltro").val() == "") {
            showModal("Sistema Cadivi", "Error Consultando: Ingrese El Item para Consultar", 'red');
            return;
        }
        else {
            if ($("#Selection").val() == "Orden") {
                codorden = $("#itemfiltro").val();
                codproducto = "";
            }

            if ($("#Selection").val() == "Producto") {
                codproducto = $("#itemfiltro").val();
                codorden = "";
            }

        }
    }
    else {
        codorden = "";
        codproducto = "";
    };


    fechadesde = $("#fechadesde").val();
    fechahasta = $("#fechahasta").val();


    var filter =
    {
        CODIGOORDENPRODUCCION: codorden,
        CODIGOPRODUCTO: codproducto,
        FECHADESDE: fechadesde,
        FECHAHASTA: fechahasta
    };
    
    $("#gridFormatoppr01").kendoGrid({
        dataSource: null
    });

    request("application/json", null, null, "POST", baseUrl + "FORMATOPPR01/ListaFormatoConsolidado", function (data) {

        var json = $.parseJSON(data.value);



        $("#gridFormatoppr01").kendoGrid({

            dataSource: new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success(json);

                    }
                },
                schema:
                {
                    model:
                    {
                        fields:
                        {
                            ID: { editable: false, type: "string" },
                            CODIGOORDENPRODUCCION: { editable: false, type: "string" },
                            CODIGOPRODUCTO: { editable: false, type: "string" },
                            CODIGOPUESTO: { editable: false, type: "string" },
                            FECHA: { editable: false, type: "date" },
                            LOTE: { editable: true, type: "string" },
                            PRESENTACION: { editable: true, type: "string" },
                            PESOMINIMO: { editable: true, type: "string" },

                        }
                    },
                    data: function (data) {
                        return data;
                    },
                    total: function (data) {
                        var total = data.length;

                        return total;
                    },
                },
                pageSize: 15,
            }),

            selectable: "row",
            height: 510,

            pageable: true,
            scrollable: true,
            columns: [
                { field: "ID", hidden: true },
                { title: "ORDEN PRODUCCION", field: "CODIGOORDENPRODUCCION" },
                { title: "PRODUCTO", field: "CODIGOPRODUCTO" },
                { title: "PUESTO", field: "CODIGOPUESTO" },
                { title: "FECHA", field: "FECHA", template: '#= kendo.toString(FECHA,"MM/dd/yyyy") #' },
                { title: "LOTE", field: "LOTE" },
                { title: "PRESENTACION", field: "PRESENTACION" },
                { title: "PESO MINIMO", field: "PESOMINIMO" },
                { title: "OPERARIO", field: "OPERARIO" },
                { title: "TURNO", field: "TURNO" }
            ],
        })



    }, errorHandler, JSON.stringify(filter));

}

function ConsultaDetallePpr019()
{
    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());

    var grid2 = $("#gridPpr019").data("kendoGrid");

    if (grid2 != null)
    {
        $("#gridPpr019").data("kendoGrid").destroy();
        $('#gridPpr019').empty();
    }

    var filter =
    {
        IDCONTROL: d.ID
    };

    request("application/json", null, null, "POST", baseUrl + "FORMATOPPRDESPEJELINEAs/ReporteDetallePpr019", function (data) {

        var json = $.parseJSON(data.value);
        $("#gridPpr019").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success(json);

                    }
                },
                schema:
                {
                    model:
                    {
                        fields:
                        {
                            CODIGOPREGUNTA: { type: "string" },
                            PREGUNTA: { type: "string" },
                        }
                    },
                    data: function (data) {
                        return data;
                    },
                    total: function (data) {
                        var total = data.length;

                        return total;
                    },
                },
                pageSize: 5,
            }),
            selectable: "row",
            height: 410,

            pageable: true,
            scrollable: true,
            
        });

    }, errorHandler, JSON.stringify(filter))

}

//funcion que realiza la peticion para crear la grid dinamica
function llenarGridListaMateriales() {

    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());


    var grid2 = $("#gridToc01").data("kendoGrid");

    if (grid2 != null)
    {
        $("#gridToc01").data("kendoGrid").destroy();
        $('#gridToc01').empty();

    }
   

    var filter =
    {
            CODIGOPRODUCTO: d.CODIGOPRODUCTOS,
            CODIGOPUESTO: '104200',
            CODIGOORDENPRODUCCION: d.CODIGOORDENPRODUCCION
    }

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearReporteDinamycTOC", function (data) {

        if (data != null)
        {
            generateGrid(JSON.parse(data.value))
        }
    }, errorHandler, JSON.stringify(filter));


};

//funcion  que se encarga de generar el modelo y la grid de manera dinamica
function generateGrid(gridData) {

    var model = generateModel(gridData[0], "NROOPERACION");

    var grid = $("#gridToc01").kendoGrid({
        dataSource: {
            data: gridData,
            height: 410,
            schema: {
                model: model,
                total: function (data) {
                    var total = data.length;

                    return total;
                },
            },
            pageSize: 10,
        },
        pageable: true,

    });



}

//funcion que realiza la peticion para crear la grid dinamica
function llenarGridListaMaterialesReporteToc01() {

    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());


    var grid2 = $("#gridToc01").data("kendoGrid");

    if (grid2 != null) {
        $("#gridToc01").data("kendoGrid").destroy();
        $('#gridToc01').empty();

    }


    var filter =
    {
        CODIGOPRODUCTO: d.CODIGOPRODUCTOS,
        CODIGOPUESTO: '104200',
        CODIGOORDENPRODUCCION: d.CODIGOORDENPRODUCCION
    }

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearReporteDinamycTOC", function (data) {
        if (data != null) {
            generateGridReporteToc01(JSON.parse(data.value))
        }
    }, errorHandler, JSON.stringify(filter));



};

//funcion  que se encarga de generar el modelo y la grid de manera dinamica
function generateGridReporteToc01(gridData) {
    
    if (gridData[0].NROOPERACION = "0") {
        var a = 1;
        for (var i = 0; i < gridData.length; i++) {

            gridData[i].NROOPERACION = "" + a + "";
            a++;
        };

    };

    var model = generateModelToc01(gridData[0], "NROOPERACION");


    var items = Object.keys(gridData[0]);
    var arrCol = [];
    var arrAgg = [];
    var sum = new Array(items.length);
    var producido = 0;

    for (var i = 0; i < sum.length; i++) {
        sum[i] = 0;
    }

    for (var gridIndex = 0; gridIndex < gridData.length; gridIndex++) {

        if (gridData[gridIndex].BAS9005 != undefined) {
            gridData[gridIndex].BAS9005 = 200;
        };
        if (gridData[gridIndex].BAS09005 != undefined) {
            gridData[gridIndex].BAS09005 = 200;
        }
        var val = Object.values(gridData[gridIndex]);

        for (var index = 0; index < items.length; index++) {



            if (items[index] != "NROOPERACION" && items[index] != "FECHA" && items[index] != "HORA") {

                sum[index] = parseFloat(sum[index]) + parseFloat(val[index]);

            }

            if (items[index] == "HORA") {

                sum[index] = parseFloat(sum[index]) + parseFloat(val[index]);

            }

            if (items[index] == "NROOPERACION" || items[index] == "FECHA") {

                sum[index] = parseFloat(sum[index]) + parseFloat(val[index]);

            }

            producido = producido + parseFloat(sum[index] * 0.001);
            $("#ProduccionFinal").val(producido.toFixed(2));
        }

    }

    for (var index = 0; index < items.length; index++) {


        if (items[index] != "NROOPERACION" && items[index] != "FECHA" && items[index] != "HORA") {

            arrCol.push({ field: items[index], aggregates: ["sum"], footerTemplate: "<div> #=kendo.toString(" + sum[index] + ",'n0')# </div>" + "<div>#=kendo.toString(" + (sum[index] * 0.001) + ", 'n1') # </div" });
            arrAgg.push({ field: items[index], aggregate: "sum" });
        }
        if (items[index] == "HORA") {

            arrCol.push({ field: items[index], aggregates: ["count"], footerTemplate: "<div>Total g</div><div>Total Kg</div> " });
            arrAgg.push({ field: items[index], aggregate: "count" });
        }

        if (items[index] == "NROOPERACION" || items[index] == "FECHA") {

            arrCol.push({ field: items[index], aggregates: ["count"], footerTemplate: "" });
            arrAgg.push({ field: items[index], aggregate: "count" });
        }

    };

    var grid = $("#gridToc01").kendoGrid({
        dataSource: {
            data: gridData,
            height: 410,
            schema: {
                model: model
            },
            pageSize: 5,
            aggregate: arrAgg
        },
        pageable: true,
        columns: arrCol
    });
}


function generateModelReporteToc01(gridData, id) {
    var model = {};
    model.id = id;
    var fields = {};

    for (var property in gridData) {
        var propType = typeof gridData[property];
        fields[property] = {
            validation: {
                required: false
            }
        };

    }
    model.fields = fields;

    return model;
}



function ExportarExcel()
{
    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());
    
    if (d != null)
    {
        $(document.getElementsByClassName('PupUpModalLoading')).show();
        var filter =
            {
                CODIGOPRODUCTO: d.CODIGOPRODUCTOS,
                CODIGOPUESTO: '104200',
                CODIGOORDENPRODUCCION: d.CODIGOORDENPRODUCCION
            }

        request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearReporteDinamycTOCExcel", function (data)
        {
            $(document.getElementsByClassName('PupUpModalLoading')).hide();
            window.location = 'http://192.168.10.13:2522/aspnet_client/reportes/' + data.value;
            //window.location = 'http://localhost:2522/Content/Reportes/'  + data.value;
        }, errorHandler, JSON.stringify(filter));
    }
    else
    {
        showModal("Sistema Cadivi", "Por Favor Seleccione una orden para exportar", "red");
        return;
    }
}

function ExportarPprExcel() {
    
    

    $(document.getElementsByClassName('PupUpModalLoading')).show();
    var grid = $("#gridPpr027").data("kendoGrid");
    //var d = grid.dataItem(grid.select());

   request("application/json", null, null, "POST", baseUrl + "FORMATOPPRs/ReportePPR027Excel", function (data) {
       $(document.getElementsByClassName('PupUpModalLoading')).hide();
       window.location = 'http://192.168.10.13:2522/aspnet_client/reportes/' + data.value;
        //window.location = 'http://localhost:2522/Content/Reportes/'  + data.value;
    }, errorHandler);
}

function ExportarExcelPPr019(reporte) {

    
    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());

    if (d != null) {

        $(document.getElementsByClassName('PupUpModalLoading')).show();

        var filter =
            {
                IDCONTROL: d.ID,
            }


        request("application/json", null, null, "POST", baseUrl + "FORMATOPPRDESPEJELINEAs/ReporteDetallePpr019Excel", function (data)
        {
            $(document.getElementsByClassName('PupUpModalLoading')).hide();
            window.location = 'http://192.168.10.13:2522/aspnet_client/reportes/' + data.value;
            //window.location = 'http://localhost:2522/Content/Reportes/' + data.value;
        }, errorHandler, JSON.stringify(filter));
    }
    else {
        showModal("Sistema Cadivi", "Por Favor Seleccione una orden para exportar", "red");
        return;
    }

}

function ExportarExcelPpr01(reporte) {
    

    var grid = $("#gridFormatoppr01").data("kendoGrid");
    var d = grid.dataItem(grid.select());
    
    if (d != null) {

        $(document.getElementsByClassName('PupUpModalLoading')).show();

        var filter =
            {
                IDPPR01: d.ID,
            }


        request("application/json", null, null, "POST", baseUrl + "FORMATOPPR01/ReporteDetallPpr01Excel", function (data)
        {
            $(document.getElementsByClassName('PupUpModalLoading')).hide();
            window.location = 'http://192.168.10.13:2522/aspnet_client/reportes/' + data.value;
            //window.location = 'http://localhost:2522/Content/Reportes/' + data.value;
        }, errorHandler, JSON.stringify(filter));
    }
    else
    {
        showModal("Sistema Cadivi", "Por Favor Seleccione una orden para exportar", "red");
        return;
    }
}

function ExportarExcelToc02(reporte) {

    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());

    if (d != null) {

        $(document.getElementsByClassName('PupUpModalLoading')).show();

        var filter =
            {
                IDTOC02: d.ID,
            }

        request("application/json", null, null, "POST", baseUrl + "FORMATOTOC02DETALLE/ReporteDetalleToc02Excel", function (data)
        {
            $(document.getElementsByClassName('PupUpModalLoading')).hide();
            window.location = 'http://192.168.10.13:2522/aspnet_client/reportes/' + data.value;
        }, errorHandler, JSON.stringify(filter));
    }
    else { 

        showModal("Sistema Cadivi", "Por Favor Seleccione una orden para exportar", "red");
        return;
    }
}

///funcion para generar el modelo de la grid dinamica
function generateModel(gridData, id) {
    var model = {};
    model.id = id;
    var fields = {};

    for (var property in gridData)
    {
        var propType = typeof gridData[property];
            fields[property] = {
                validation: {
                    required: false
                }
            };
      
    }
    model.fields = fields;

    return model;
}

function ConsultaReporteToc02() {

    var codorden, codproducto, fechadesde, fechahasta;


    if ($("#Selection").val() != "Vacio") {
        if ($("#itemfiltro").val() == "") {
            showModal("Sistema Cadivi", "Error Consultando: Ingrese El Item para Consultar", 'red');
            return;
        }
        else {
            if ($("#Selection").val() == "Orden") {
                codorden = $("#itemfiltro").val();
                codproducto = "";
            }

            if ($("#Selection").val() == "Producto") {
                codproducto = $("#itemfiltro").val();
                codorden = "";
            }

        }
    }
    else {
        codorden = "";
        codproducto = "";
    };


    fechadesde = $("#fechadesde").val();
    fechahasta = $("#fechahasta").val();



    var filter =
    {
        CODIGOORDENPRODUCCION: codorden,
        CODIGOPRODUCTO: codproducto,
        FECHADESDE: fechadesde,
        FECHAHASTA: fechahasta
    };


    request("application/json", null, null, "POST", baseUrl + "FORMATOTOC02/ReporteToc02", function (data) {

        var json = $.parseJSON(data.value);

        $("#gridOrden").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success(json);

                    }
                },
                schema:
                {
                    model:
                    {
                        fields:
                        {
                            CODIGOORDENPRODUCCION: { type: "string" },
                        }
                    },
                    data: function (data) {
                        return data;
                    },
                    total: function (data) {
                        var total = data.length;

                        return total;
                    },
                },
                pageSize: 5,
            }),
            selectable: "row",
            change: ConsultaDetalleToc02,
            height: 210,
            pageable: true,
            scrollable: true,
        });



    }, errorHandler, JSON.stringify(filter));

}

function ConsultaDetalleToc02()
{
    var grid = $("#gridOrden").data("kendoGrid");
    var d = grid.dataItem(grid.select());

    var grid2 = $("#gridPpr019").data("kendoGrid");

    if (grid2 != null) {
        $("#gridPpr019").data("kendoGrid").destroy();
        $('#gridPpr019').empty();
    }

    var filter =
    {
        IDTOC02: d.ID,
    }

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOC02DETALLE/ReporteDetalleToc02", function (data) {

        var json = $.parseJSON(data.value);

        $("#gridToc02").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success(json);

                    }
                },
                schema:
                {
                    model:
                    {
                        fields:
                        {
                        }
                    },
                    data: function (data) {
                        return data;
                    },
                    total: function (data) {
                        var total = data.length;

                        return total;
                    },
                },
                pageSize: 5,
            }),
            selectable: "row",
            height: 410,
            selectable: "row",
            pageable: true,
            scrollable: true,
            
        })



    }, errorHandler, JSON.stringify(filter))
}

function iniciarPpr01Reporte() {
    $("#FechaIngreso").val(new Date().toJSON().slice(0, 10));


    if ($("#OrdenProduccion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Orden de Produccion Para Iniciar El Proceso", 'red');
        return;
    }

    if ($("#CodProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Codigo Del Producto", 'red');
        return;
    }

    if ($("#LoteProducto").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Lote", 'red');
        return;
    }

    if ($("#Presentacion").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Numero de Presentacion", 'red');
        return;
    }


    if ($("#PesoMin").val() == "") {
        showModal("Sistema Cadivi", "Error Iniciando Proceso: Ingrese El Valor del Peso Minimo", 'red');
        return;
    }



    var filter = {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOPUESTO: $("#MaqAsignar").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        LOTE: $("#LoteProducto").val(),
        LOGIN: window.localStorage['usuario'],
        ID: 0,
        FECHA: $("#FechaIngreso").val(),
        PRESENTACION: parseInt($("#Presentacion").val()),
        PESOMINIMO: parseInt($("#PesoMin").val()),
    }


    LlenarGridControlPpr01Reporte(filter);
    

}


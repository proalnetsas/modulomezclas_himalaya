﻿var baseUrl = "../odata/";
var dateFields = [];
$(document).ready(function () {
    if (window.location.pathname === "/Home/AdminUsers") {
        generateGridAdminUsers();
    }

    if (window.location.pathname === "/Home/AdminPersonal") {
        generateGridPersonal();
    }


});

//Funcion Para llenar grid de usuarios en configuracion
function llenargridProductos() {

   var filter = 
    {
       CODIGOORDENPRODUCCION: $("#ordenProduccion").val()
    }
    
    $("#gridOrdenProduccion").kendoGrid({
        dataSource: new kendo.data.DataSource({
            type: 'odata',
            transport: {
                read:
                {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "MATERIALESPORPRODUCTOes/ListaProductos",
                    contentType: "application/json; charset=utf-8",
                },
                parameterMap: function (options, operation) {
                    return JSON.stringify(filter);
                },
            },
            schema: {
                model: {
                    fields: {
                        CODIGOPRODUCTOS: { editable: true, type: "string" },
                        NOMBREPRODUCTO:  { validation: { required: true },type:"string"},
                     }
                },
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data['odata.count'];
                },
            },
            

        }),
        selectable: "row",
        filterable: {
            mode: "row",
            extra: false
        },
       
        height: 610,
        selectable: "row",
        pageable: false,
        scrollable: true,
        sortable: true,
        change: llenargridMateriales,
        columns: [
            {
                title: "Codigo Producto",width:"40%", field: "CODIGOPRODUCTOS", filterable: {
                    cell: {
                        showOperators: "contains",
                    }
                }
            },
            {
                title: "Producto", field: "NOMBREPRODUCTO", filterable: false
            },
        ],
    
    });

};

//Funcion Para llenar grid de usuarios en configuracion
function llenargridProductosConPm() {

    var filter =
    {
        CODIGOORDENPRODUCCION: $("#ordenProduccion").val()
    }

    $("#gridOrdenProduccion").kendoGrid({
        dataSource: new kendo.data.DataSource({
            type: 'odata',
            transport: {
                read:
                {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "MATERIALESPORPRODUCTOes/ListaProductosYPm",
                    contentType: "application/json; charset=utf-8",
                },
                parameterMap: function (options, operation) {
                    return JSON.stringify(filter);
                },
            },
            schema: {
                model: {
                    fields: {
                        CODIGOPRODUCTOS: { editable: true, type: "string" },
                        NOMBREPRODUCTO: { validation: { required: true }, type: "string" },
                    }
                },
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data['odata.count'];
                },
            },


        }),
        selectable: "row",
        filterable: {
            mode: "row",
            extra: false
        },

        height: 610,
        selectable: "row",
        pageable: false,
        scrollable: true,
        sortable: true,
        change: llenargridMateriales,
        columns: [
            {
                title: "Codigo Producto", width: "40%", field: "CODIGOPRODUCTOS", filterable: {
                    cell: {
                        showOperators: "contains",
                    }
                }
            },
            {
                title: "Producto", field: "NOMBREPRODUCTO", filterable: false
            },
        ],

    });

};

//Funcion Para llenar la grid de las maquinas para relacionar con el usuario
function llenargridMateriales(e) {

    var grid = $("#gridOrdenProduccion").data("kendoGrid");
    var grid2 = $("#gridMateriales").data("kendoGrid");
    var d = grid.dataItem(grid.select());

    
    if (grid2 != null)
    {
        $("#gridMateriales").data("kendoGrid").destroy();
    }

    
    $("#codProducto").val(d.CODIGOPRODUCTOS);
    
    var filter = {
        
        CODIGOPRODUCTO: d.CODIGOPRODUCTOS,
        CODIGOORDENPRODUCCION: $("#ordenProduccion").val()
    }

    $("#gridMateriales").kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "MATERIALESPORPRODUCTOes/ListaMaterialesProductos",
                    contentType: "application/json; charset=utf-8",
                    
                },

                parameterMap: function (options, operation) {
                    return JSON.stringify(filter);
                }
            },

            schema: {
                model: {
                    fields: {
                        CODIGOMATERIAL: { editable: false, type: "string" },
                        NOMBREMATERIAL: { editable: false, type: "string" },
                        //NOMBREPRODUCTO: { editable: false, type: "string" },
                        IMPRESION: { editable: false, type: "number" },
                        APROBADO: { editable: false, type: "boolean" },
                        GUARDADO: { editable: false, type: "boolean" },
                    }
                },
                data: function (data) {
                    
                    return data.value;
                },
                total: function (data) {
                    var total = data['odata.count'];
                    return total;
                },
            },
          
        }),
        height: 610,
        selectable: "row",
        pageable: false,
        scrollable: true,
        sortable: true,
        columns: [
            { title: "COD MATERIAL", field: "CODIGOMATERIAL", width: "15%" },
            { title: "MATERIAL", field: "NOMBREMATERIAL", width: "25%" },
            //{
            //    title: "PRODUCTO", field: "NOMBREPRODUCTO", width: "40%"
            //},
            {
                title: "APROBADO", field: "APROBADO", template: function (data)
                {
                    if (data.APROBADO == 1)
                        return "SI";
                    else
                        return "NO";
                }

            },
            {
                title: "IMPRESO", field: "IMPRESION", template: function (data)
                {
                    if (data.IMPRESION == 1)
                        return "SI";
                    else
                        return "NO";
                }

            },
              {
                  title: "GUARDADO",field:"GUARDADO", template: function (data)
                  {
                      if (data.GUARDADO == false)
                          return "NO";
                      else
                          return "SI";
                  }

              },
            {
            command: { text: "Entrega", click: iniciarEntrega }, title: " "
            },

        ],
       
        

    });

}

//Llena la grid de la lista de materiales en el ppr027 de mezclado
function llenargridMaterialesMezclado() {

    var grid2 = $("#gridMateriales").data("kendoGrid");
    var id = undefined;

    if (grid2 != null) {
        $("#gridMateriales").data("kendoGrid").destroy();
    }


    var filter = {
        CODIGOPRODUCTO: $("#codigoProducto").val(),
        CODIGOORDENPRODUCCION: $("#ordenProduccion").val()
    }

    $("#gridMateriales").kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "MATERIALESPORPRODUCTOes/ListaMaterialesProductosMezclado2",
                    contentType: "application/json; charset=utf-8",

                },

                parameterMap: function (options, operation) {
                    return JSON.stringify(filter);
                }
            },

            schema: {
                model: {
                    fields: {
                        CODIGOMATERIAL: { editable: false, type: "string" },
                        NOMBREMATERIAL: { editable: false, type: "string" },
                        NOMBREPRODUCTO: { editable: false, type: "string" },
                    }
                },
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    var total = data['odata.count'];
                    return total;
                },
            },

        }),
        height: 450,
        selectable: "row",
        pageable: false,
        scrollable: true,
        sortable: true,
        columns: [
            { title: "CODIGO MATERIAL", field: "CODIGOMATERIAL" },
            { title: "MATERIAL", field: "NOMBREMATERIAL" },
            {
                title: "PRODUCTO", field: "NOMBREPRODUCTO"
            },
            {
                title: "APROBADO", field: "APROBADO", template: function (data)
                {
                    if(!data.APROBADO)
                        return "<div style='text-align:center'><a class='k-button k-grid-Recibido' onclik='consultaPPRBodega()' style='background: blue;color: white;'><span  >Sin Aprobar</span></a></div>"
                    else
                    {
                        return "<div style='text-align:center'><a class='k-button k-grid-Recibido' onclik='consultaPPRBodega()' style='background: gray;color: white;'><span  >Aprobado</span></a></div>"
                    }
                }
            },
             {
                 title: "PROCESADO", field: "PROCESADO", template: function (data) {
                     if (!data.PROCESADO)
                         return "<div style='text-align:center'><a onclik='iniciarProcesoMezclado' class='k-button k-grid-EnProceso'  style='background: red;color: white;'><span>Sin Procesar</span></a></div>"
                     else {
                         return "<div style='text-align:center'><a onclik='iniciarProcesoMezclado' class='k-button k-grid-EnProceso'  style='background: gray;color: white;' ><span>Procesado</span></a></div>"
                     }
                 }
             },
              {
                  title: "DEVUELTO", field: "DEVUELTO", template: function (data) {
                      if (!data.DEVUELTO)
                          return "<div style='text-align:center'><a onclik='iniciarDevolucion' class='k-button k-grid-Devolucion'  style='background: green;color: white;' ><span>Sin Devolucion</span></a></div>"
                      else {
                          return "<div style='text-align:center'><a onclik='iniciarDevolucion' class='k-button k-grid-Devolucion'  style='background: gray;color: white;'><span>Devolucion</span></a></div>"
                      }
                  }
              },
            {
                command: {
                    text: "Recibido", click: consultaPPRBodega, title: " ",
                }, hidden:'hidden'
            },
            {
                command: { text: "En Proceso", click: iniciarProcesoMezclado }, title: " ", hidden: 'hidden'
            },
            {
                command: { text: "Devolucion", click: iniciarDevolucion }, title: " ", hidden: 'hidden'
            },
        ],
        //dataBound: cambiarColoresBotones


    });

}


//llena la grid de control del toc 02 y envia los datos para guardarlos la primera vez
function LlenarGridControlProceso(filter)
{
    if(filter != undefined)
    {

        $("#gridControlProceso").kendoGrid({
            dataSource: ({
                type: 'odata',
                transport: {
                    read: {
                        dataType: 'json',
                        method: 'POST',
                        url: baseUrl + "FORMATOTOC02/CreateFormatoToc02",
                        contentType: "application/json; charset=utf-8",
                    },
                    update: {
                        dataType: "json",
                        method: 'POST',
                        url: baseUrl + "FORMATOTOC02/CreateFormatoToc02",
                        contentType: "application/json; charset=utf-8",
                    },

                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options)
                        {
                            return kendo.stringify(GuardarDatosFormato(options));
                        }
                        else
                            return JSON.stringify(filter);
                    }
                },

                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false, type: "number" },
                            CODIGOORDENPRODUCCION: { editable: false, type: "string" },
                            CODIGOPRODUCTOS: { editable: false, type: "string" },
                            CODIGOPUESTO: { editable: false, type: "string" },
                            LOTE: {editable: true, type: "string"},
                            FECHA: {editable:false, type:"string"},
                            PASTASPRODUCIDAS: { editable: true, type: "number" },
                            PASTARECHAZADAINICIO: { editable: true, type: "number" },
                            PASTARECHAZADAFIN: { editable: true, type: "number" },
                            PORCENTAJERECHAZADO: { editable: false, type: "string" },
                            CUMPLEGALGAS: { editable: true, type: "boolean" },
                            OBSERVACIONES: {editable: true, type: "string"}
                        }
                    },
                    data: function (data) {
                        return data.value;
                    },
             
                    total: function (data) {
                        var total = data['odata.count'];
                        return total;
                    },
                
                },

            }),
            batch: true,
            selectable: "row",
            scrollable: true,
            sortable: true,
            change: LlenarGridCondiciones,
            columns: [
                { title: "ID", field: "ID" , hidden: "true"},
                { title: "ORDEN P", field: "CODIGOORDENPRODUCCION", width:"150px" },
                { title: "# PRODUCTO", field: "CODIGOPRODUCTOS", width: "150px" },
                {title: "CODIGOPUESTO", field:"CODIGOPUESTO" , hidden:"true"},
                { title: "LOTE", field: "LOTE", width: "150px" },
                {
                    title: "FECHA", field: "FECHA", width: "150px", template: function (data) {
                        return new Date(data.FECHA).toJSON().slice(0, 10);
                    }
                },
                { title: "PASTAS PRODUCIDAS", width: "150px", field: "PASTASPRODUCIDAS" },
                { title: "PASTA RECHAZADA INICIO", width: "150px", field: "PASTARECHAZADAINICIO" },
                { title: "PASTA RECHAZADA FIN", width: "150px", field: "PASTARECHAZADAFIN" },
                {
                    title: "PORCENTAJE RECHAZADO", width: "150px", field: "PORCENTAJERECHAZADO", template: function (data) {
                        return parseFloat(data.PORCENTAJERECHAZADO).toFixed(2) + '%';
                    }
                },
                {
                    title: "CUMPLE GALGAS", width: "150px", field: "CUMPLEGALGAS", template: function (data) {
                        if (data.CUMPLEGALGAS)
                            return "Si";
                        else
                            return "No";
                    }
                },
                { title: "OBSERVACIONES", width: "150px", field: "OBSERVACIONES" },
                { command: [{ name: "edit", text: { edit: "Editar", update: "Editar", cancel: "Cancelar" } }], title: "Editar", width: "130px" },
             ],
            editable: "inline"


    });

    }
} 

//llena los datos de las condiciones del toc 02, cada que se selecciona el valor en el control
function LlenarGridCondiciones() {

    var grid = $("#gridControlProceso").data("kendoGrid");
    var grid2 = $("#gridCondiciones").data("kendoGrid");
    var d = grid.dataItem(grid.select());
    $("#btnEditar").css("visibility", "visible");

    if (grid2 != null) {
        $("#gridCondiciones").data("kendoGrid").destroy();
    
    }

    var filter =
    {
        CODIGOPUESTO: d.CODIGOPUESTO,
        CODIGOCONDICION: d.ID
    }

    $("#gridCondiciones").kendoGrid({
        dataSource: {
            type: "odata",
            transport: {
                read: {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "FORMATOTOC02DETALLE/ListaFormato",
                    contentType: "application/json; charset=utf-8",
                },
                update: {
                    datatype: "json",
                    method: 'POST',
                    url: baseUrl + "FORMATOTOC02DETALLE/EditarFormatoDetalle",
                    contentType: "application/json; charset=utf-8",
                },
                parameterMap: function (options, operation) {
                    
                    if (operation !== "read" && options) {
                        return kendo.stringify(ActualizarDatosFormatoDetalle(options));
                    }
                    else {
                        return JSON.stringify(filter);
                    }
                },

            },
            schema: {

                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "string" },
                        CONDICION: { editable: false, type: "string" },
                        ESPECIFICACION: { editable: false, type: "string" },
                        IEP: { editable: false, type: "string" },
                        HORA1: { editable: true, type: "string" },
                        HORA2: { editable: true, type: "string" },
                        HORA3: { editable: true, type: "string" },
                        HORA4: { editable: true, type: "string" },
                        HORA5: { editable: true, type: "string" },
                        HORA6: { editable: true, type: "string" },
                        HORA7: { editable: true, type: "string" },
                        HORA8: { editable: true, type: "string" },
                        OBSERVACIONES: { editable: true, type: "string" },
                        IDTOC: { editable: false, type: "string" }
                    }
                },
                data: function (data) {
                    
                     return data.value;
                },
                total: function (data) {
                    var total = data['odata.count'];
                    return total;
                },
            },
        },
        batch: true,
        height: 400,
        filterable: true,
        sortable: true,
        pageable: false,
        columns: [
        { title: "Codigo", field: "ID" },
        { title: "CONDICION", field: "CONDICION", width: "200px" },
        {
            title: "ESPECIFICACION", field: "ESPECIFICACION", width: "150px"
        },
        { title: "IEP", field: "IEP", width: "80px" },
        { title: "HORA1", field: "HORA1", width: "100px" },
        { title: "HORA2", field: "HORA2", width: "100px" },
        { title: "HORA3", field: "HORA3", width: "100px" },
        { title: "HORA4", field: "HORA4", width: "100px" },
        { title: "HORA5", field: "HORA5", width: "100px" },
        { title: "HORA6", field: "HORA6", width: "100px" },
        { title: "HORA7", field: "HORA7", width: "100px" },
        { title: "HORA8", field: "HORA8", width: "100px" },
        { title: "OBSERVACIONES", field: "OBSERVACIONES", width: "150px" },
        { title: "IDTOC", field: "IDTOC", hidden: "true" },
        //{ command: [{ name: "edit", text: { edit: "Editar", update: "Editar", cancel: "Cancelar" } }], width: "130px" },
        ],
        //editable: "inline"

    });



}


//llena la grid de control del ppr 01 y envia los datos para guardarlos la primera vez
function LlenarGridControlPpr01(filter) {

   
    if (filter != undefined) {

        $("#gridFormatoppr01").kendoGrid({
            dataSource: ({
                type: 'odata',
                transport: {
                    read: {
                        dataType: 'json',
                        method: 'POST',
                        url: baseUrl + "FORMATOPPR01/CreateFormatoPPR01",
                        contentType: "application/json; charset=utf-8",
                    },
                    update: {
                        dataType: "json",
                        method: 'POST',
                        url: baseUrl + "FORMATOPPR01/CreateFormatoPPR01",
                        contentType: "application/json; charset=utf-8",
                    },

                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options) {
                            return kendo.stringify(GuardarDatosFormatoPPr01(options));
                        }
                        else
                            return JSON.stringify(filter);
                    }
                },

                schema: {
                    model: {
                        id: "ID",
                        fields:
                        {
                            ID: { editable: false, type: "string" },
                            CODIGOORDENPRODUCCION: { editable: false, type: "string" },
                            CODIGOPRODUCTO: { editable: false, type: "string" },
                            CODIGOPUESTO: { editable: false, type: "string" },
                            FECHA: { editable: false, type: "string" },
                            LOTE: { editable: true, type: "string" },
                            PRESENTACION: { editable: true, type: "number" },
                            PESOMINIMO: { editable: true, type: "number" },
                        }
                    },
                    data: function (data) {
                        return data.value;
                    },

                    total: function (data) {
                        var total = data['odata.count'];
                        return total;
                    },

                },

            }),
            batch: true,
            selectable: "row",
            scrollable: true,
            sortable: true,
            change: ListarPesosAtributos,
            columns: [
                { title: "ID", field: "ID" },
                { title: "CODIGOORDENPRODUCCION", field: "CODIGOORDENPRODUCCION" },
                { title: "CODIGOPRODUCTOS", field: "CODIGOPRODUCTO" },
                { title: "CODIGOPUESTO", field: "CODIGOPUESTO", hidden: "true" },
                {
                    title: "FECHA", field: "FECHA", template: function (data) {
                        return new Date(data.FECHA).toJSON().slice(0, 10);
                    }
                },
                { title: "LOTE", field: "LOTE" },
                
                { title: "PRESENTACION", field: "PRESENTACION" },
                { title: "PESOMINIMO", field: "PESOMINIMO" },
                { command: [{ name: "edit", text: { edit: "Editar", update: "Editar", cancel: "Cancelar" } }], title: "Editar", width: "130px" },
            ],
            editable: "inline"


        });

    }
}

function ListarPesosAtributos(e) {
    LlenarGridPesos();
    LlenarGridAtributos();

    $("#btnEditar").css("visibility", "visible");
    $("#btnEditarA").css("visibility", "visible");
}

function LlenarGridPesos() {
    var grid = $("#gridFormatoppr01").data("kendoGrid");
    var grid2 = $("#gridPesos").data("kendoGrid");
    var d = grid.dataItem(grid.select());


    if (grid2 != null) {
        $("#gridPesos").data("kendoGrid").destroy();
    }

    var filter =
    {
        CODIGOPUESTO: d.CODIGOPUESTO,
        CODIGOPPR: d.ID
    }

    $("#gridPesos").kendoGrid({
        dataSource: ({
            type: 'odata',
            transport: {
                read: {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "FORMATOPPR01/ListarPesosPPR01",
                    contentType: "application/json; charset=utf-8",

                },
                //update: {
                //    dataType: "json",
                //    method: 'POST',
                //    url: baseUrl + "FORMATOPPR01/ActualizarFormatoAtributo",
                //    contentType: "application/json; charset=utf-8",
                //},

                parameterMap: function (options, operation) {
                    if (operation != "read" && options) {
                        return kendo.stringify(ActualizarFormatoPpr01PesosAtributos(options, "Pesos"));
                    }
                    else
                        return JSON.stringify(filter);
                }
            },

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "string" },
                        MUESTRA: { editable: false, type: "string" },
                        HORA1: { editable: true, type: "string" },
                        HORA2: { editable: true, type: "string" },
                        HORA3: { editable: true, type: "string" },
                        HORA4: { editable: true, type: "string" },
                        HORA5: { editable: true, type: "string" },
                        HORA6: { editable: true, type: "string" },
                        HORA7: { editable: true, type: "string" },
                        HORA8: { editable: true, type: "string" },
                        HORA9: { editable: true, type: "string" },
                        HORA10: { editable: true, type: "string" },
                        HORA11: { editable: true, type: "string" },
                        HORA12: { editable: true, type: "string" },
                        HORA13: { editable: true, type: "string" },
                        HORA14: { editable: true, type: "string" },
                        HORA15: { editable: true, type: "string" },
                        HORA16: { editable: true, type: "string" },
                        HORA17: { editable: true, type: "string" },
                        HORA18: { editable: true, type: "string" },
                        HORA19: { editable: true, type: "string" },
                        HORA20: { editable: true, type: "string" },
                        HORA21: { editable: true, type: "string" },
                        HORA22: { editable: true, type: "string" },
                        HORA23: { editable: true, type: "string" },
                        HORA24: { editable: true, type: "string" },
                        IDFORMATOPPR01: { editable: false, type: "string" }
                    }
                },
                

                data: function (data) {
                    return data["value"];
                },
                total: function (data) {
                    var total = data['odata.count'];
                    return total;
                },

            },
            //aggregate: [{ field: "HORA1", aggregate: "average" }],
        }),
        batch: true,
        selectable: "row",
        scrollable: true,
        sortable: true,
        columns: [
            { title: "ID", field: "ID" },
            { title: "MUESTRA", field: "MUESTRA", width:"300px" },
            //{
            //    title: "HORA1", field: "HORA1", width: "100px", aggregates: ['average'],template: "#= kendo.toString(HORA1, 'c0') #",
            //    footerTemplate: "#= average # "
            //},
            { title: "HORA1", field: "HORA1", width: "100px" },
            { title: "HORA2", field: "HORA2", width: "100px" },
            { title: "HORA3", field: "HORA3", width: "100px" },
            { title: "HORA4", field: "HORA4", width: "100px" },
            { title: "HORA5", field: "HORA5", width: "100px" },
            { title: "HORA6", field: "HORA6", width: "100px" },
            { title: "HORA7", field: "HORA7", width: "100px" },
            { title: "HORA8", field: "HORA8", width: "100px" },
            { title: "HORA9", field: "HORA9", width: "100px" },
            { title: "HORA10", field: "HORA10", width: "100px" },
            { title: "HORA11", field: "HORA11", width: "100px" },
            { title: "HORA12", field: "HORA12", width: "100px" },
            { title: "HORA13", field: "HORA13", width: "100px" },
            { title: "HORA14", field: "HORA14", width: "100px" },
            { title: "HORA15", field: "HORA15", width: "100px" },
            { title: "HORA16", field: "HORA16", width: "100px" },
            { title: "HORA17", field: "HORA17", width: "100px" },
            { title: "HORA18", field: "HORA18", width: "100px" },
            { title: "HORA19", field: "HORA19", width: "100px" },
            { title: "HORA20", field: "HORA20", width: "100px" },
            { title: "HORA21", field: "HORA21", width: "100px" },
            { title: "HORA22", field: "HORA22", width: "100px" },
            { title: "HORA23", field: "HORA23", width: "100px" },
            { title: "HORA24", field: "HORA24", width: "100px" },
            { title: "IDFORMATOPPR01", field: "IDFORMATOPPR01", hidden:"true" },
        ],
        editable: "inline"

    });

}


function LlenarGridAtributos() {
    var grid = $("#gridFormatoppr01").data("kendoGrid");
    var grid2 = $("#gridAtributos").data("kendoGrid");
    var d = grid.dataItem(grid.select());


    if (grid2 != null) {
        $("#gridAtributos").data("kendoGrid").destroy();
    }

    var filter =
    {
        CODIGOPUESTO: d.CODIGOPUESTO,
        CODIGOPPR: d.ID
    }

    $("#gridAtributos").kendoGrid({
     
        dataSource: ({
            type: 'odata',
            transport: {
                read: {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "FORMATOPPR01/ListarAtributosPPR01",
                    contentType: "application/json; charset=utf-8",

                },
                update: {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "FORMATOPPR01/ActualizarFormatoAtributo",
                    contentType: "application/json; charset=utf-8",
                },

                parameterMap: function (options, operation) {
                    if (operation != "read" && options) {
                        return kendo.stringify(ActualizarFormatoPpr01PesosAtributos(options, "Atributos"));
                    }
                    else
                        return JSON.stringify(filter);
                }
            },

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "string" },
                        MUESTRA: { editable: false, type: "string" },
                        HORA1: { editable: true, type: "string" },
                        HORA2: { editable: true, type: "string" },
                        HORA3: { editable: true, type: "string" },
                        HORA4: { editable: true, type: "string" },
                        HORA5: { editable: true, type: "string" },
                        HORA6: { editable: true, type: "string" },
                        HORA7: { editable: true, type: "string" },
                        HORA8: { editable: true, type: "string" },
                        HORA9: { editable: true, type: "string" },
                        HORA10: { editable: true, type: "string" },
                        HORA11: { editable: true, type: "string" },
                        HORA12: { editable: true, type: "string" },
                        HORA13: { editable: true, type: "string" },
                        HORA14: { editable: true, type: "string" },
                        HORA15: { editable: true, type: "string" },
                        HORA16: { editable: true, type: "string" },
                        HORA17: { editable: true, type: "string" },
                        HORA18: { editable: true, type: "string" },
                        HORA19: { editable: true, type: "string" },
                        HORA20: { editable: true, type: "string" },
                        HORA21: { editable: true, type: "string" },
                        HORA22: { editable: true, type: "string" },
                        HORA23: { editable: true, type: "string" },
                        HORA24: { editable: true, type: "string" },
                        IDFORMATOPPR01: { editable: false, type: "string" }
                    }
                },

                data: function (data) {
                    return data["value"];
                },
                total: function (data) {
                    var total = data['odata.count'];
                    return total;
                },

            },

        }),
        height: '300px',
        batch: true,
        selectable: "row",
        scrollable: true,
        sortable: true,
        columns: [
            { title: "ID", field: "ID" },
            { title: "CALIFICACION", field: "MUESTRA", width:"300px" },
              { title: "HORA1", field: "HORA1", width: "100px" },
            { title: "HORA2", field: "HORA2", width: "100px" },
            { title: "HORA3", field: "HORA3", width: "100px" },
            { title: "HORA4", field: "HORA4", width: "100px" },
            { title: "HORA5", field: "HORA5", width: "100px" },
            { title: "HORA6", field: "HORA6", width: "100px" },
            { title: "HORA7", field: "HORA7", width: "100px" },
            { title: "HORA8", field: "HORA8", width: "100px" },
            { title: "HORA9", field: "HORA9", width: "100px" },
            { title: "HORA10", field: "HORA10", width: "100px" },
            { title: "HORA11", field: "HORA11", width: "100px" },
            { title: "HORA12", field: "HORA12", width: "100px" },
            { title: "HORA13", field: "HORA13", width: "100px" },
            { title: "HORA14", field: "HORA14", width: "100px" },
            { title: "HORA15", field: "HORA15", width: "100px" },
            { title: "HORA16", field: "HORA16", width: "100px" },
            { title: "HORA17", field: "HORA17", width: "100px" },
            { title: "HORA18", field: "HORA18", width: "100px" },
            { title: "HORA19", field: "HORA19", width: "100px" },
            { title: "HORA20", field: "HORA20", width: "100px" },
            { title: "HORA21", field: "HORA21", width: "100px" },
            { title: "HORA22", field: "HORA22", width: "100px" },
            { title: "HORA23", field: "HORA23", width: "100px" },
            { title: "HORA24", field: "HORA24", width: "100px" },
            { title: "IDFORMATOPPR01", field: "IDFORMATOPPR01", hidden: "true" },
        ],
        
    });

}
//Llena la grid de la lista 
function llenargridMateriaPrima() {

        var filter =
        {
            CODIGOPRODUCTO: $("#CodProducto").val(),
            CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
            NROOPERACION: $("#OperacionActual").val(),
            CODIGOPUESTO: $("#MaqAsignar").val(),

        }

        $("#gridMateriaPrimaOperacion").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        dataType: "json",
                        method: 'POST',
                        url: baseUrl + "FORMATOTOCs/ConsultarDatosOperacion",
                        contentType: "application/json; charset=utf-8",

                    },

                    parameterMap: function (options, operation) {
                        return JSON.stringify(filter);
                    }
                },

                schema: {
                    model: {
                        fields: {
                            NROOPERACION:{type: "string"},
                            CODIGOMATERIAL: {  type: "string" },
                            NOMBREMATERIAL: {  type: "string" },
                            NOMBREPRODUCTO: { type: "string" },
                        }
                    },
                    data: function (data) {

                        return data.value;
                    },
                    total: function (data) {
                        var total = data['odata.count'];
                        return total;
                    },
                },

            }),
            height: 450,
            selectable: "row",
            pageable: false,
            scrollable: true,
            sortable: true,
            columns: [
                { title: "Nro Operacion", field: "NroOperacion" , width: "10%" },
                //{ title: "Cod Material", field: "CodigoMaterial" },
                {
                    title: "Material", field: "Material"
                },
                { title: "Cantidad", field: "Cantidad", width: "15%" }
           
            ],



        });

    }

////funcion que llena la lista de despeje de linea
function llenargridDespejeLinea() {

        var filter =
        {
            CODIGOPRODUCTO: $("#ListaProductoActual").val(),
            CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
            CODIGOPRODUCTOANT: $("#ListaProductoAnterior").val(),
            CODIGOPUESTO: $("#MaqAsignar").val(),
            LOTEPRODUCTO: $("#LoteProducto").val(),
            LOTEPRODUCTOANT: $("#LoteProductoAnt").val(),
            FECHA: $("#FechaIngreso").val().toString("yyyy-MM-dd"),
            TURNO:"1", 
            DESINFECTANTE: $("#Desinfectante").val(),
        }


        $("#gridDespejeLinea").kendoGrid({
            dataSource: {
                type: "odata",
                transport: {
                    //read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
                    read:{
                        dataType: "json",
                        method: 'POST',
                        url: baseUrl + "FORMATOPPRCONTROLs/CrearFormatoControlProceso",
                        contentType: "application/json; charset=utf-8",
                    },
                    update:{
                        datatype: "json",
                        method: 'POST',
                        url: baseUrl + "FORMATOPPRDESPEJELINEAs/EditarRespuestaDespejelinea",
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (options, operation) {
                        
                        if (operation !== "read" && options) {
                            return kendo.stringify(GuardarFormatoPPR019(options));
                        }
                        else {
                            return JSON.stringify(filter);
                        }
                    },

                },
               schema: {
                    
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false, type: "string" },
                            CODIGOPREGUNTA: {type: "string", editable: false},      
                            PREGUNTA: { type: "string", editable:false },
                            RESPUESTA: { type: "boolean", editable: true },
                            APLICA: { type: "boolean", editable: true },
                            OBSERVACIONES: { type: "string", editable:true },
                        }
                    },
                    data: function (data) {
                        return data.value;
                    },
                    total: function (data) {
                        var total = data['odata.count'];
                        return total;
                    },
                },
            },
            batch: true,
            height: 550,
            filterable: true,
            sortable: true,
            pageable: true,
            columns: [
            { title: "Codigo", field: "ID", hidden: true },
            { title: "Cod Pregunta", field: "CODIGOPREGUNTA", hidden: true },
            { title: "Pregunta", field: "PREGUNTA", width:'50%' },
            {
                title: "Respuesta", field: "RESPUESTA", template: function (data)
                {
                    if (data.RESPUESTA == true) {return "Si";}
                    else {return "No";}
                }
            },
            {
                title: "Aplica", field: "APLICA", template: function (data) {
                    if (data.APLICA == true) { return "Si"; }
                    else {return "No";}
                }
            },
            { title: "Observaciones", field: "OBSERVACIONES" },
                { command: [{ name: "edit", text: { edit: "Editar", update: "Editar", cancel: "Cancelar" } }], width: "130px" },
            ],
                editable: "inline"
            
        });
    
    
    }


//funcion  que se encarga de generar el modelo y la grid de manera dinamica
function generateGrid(gridData) {


    var model = generateModel(gridData[0], "NROOPERACION");

    var grid = $("#gridMateriaPrima").kendoGrid({
        dataSource: {
            data: gridData,
            height: 410,
            schema: {
                model: model,
                total: function (data) {
                    var total = data.length;

                    return total;
                },
            },
            pageSize: 12,
        },
        pageable: true,

    });
}
        
    
//funcion que realiza la peticion para crear la grid dinamica
function llenarGridListaMateriales(filter) {


        request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearReporteDinamycTOC", function (data) {
            if (data != null) {
                generateGrid(JSON.parse(data.value))
            }
        }, errorHandler, JSON.stringify(filter));


  
};
//Funciones para llenar grid de Toc01
function LlenarGridToc01(filter) {

    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearReporteDinamycTOC", function (data) {
        if (data != null) {
            generateGridToc01(JSON.parse(data.value))
        }
    }, errorHandler, JSON.stringify(filter));

};

function generateGridToc01(gridData) {




    if (gridData[0].NROOPERACION = "0") {
        var a = 1;
        for (var i = 0; i < gridData.length; i++) {

            gridData[i].NROOPERACION = "" + a + "";
            a++;
        };

    };


    var model = generateModelToc01(gridData[0], "NROOPERACION");


    var items = Object.keys(gridData[0]);
    var arrCol = [];
    var arrAgg = [];
    var sum = new Array(items.length);
    var producido = 0;

    for (var i = 0; i < sum.length; i++) {
        sum[i] = 0;
    }

    for (var gridIndex = 0; gridIndex < gridData.length; gridIndex++) {

        if (gridData[gridIndex].BAS9005 != undefined) {
            gridData[gridIndex].BAS9005 = 200;
        };
        if (gridData[gridIndex].BAS09005 != undefined) {
            gridData[gridIndex].BAS09005 = 200;
        };
        var val = Object.values(gridData[gridIndex]);

        for (var index = 0; index < items.length; index++) {

            
            if (items[index] != "NROOPERACION" && items[index] != "FECHA" && items[index] != "HORA") {

                sum[index] = parseFloat(sum[index]) + parseFloat(val[index]);

            }

            if (items[index] == "HORA") {

                //sum[index] = parseFloat(sum[index]) + parseFloat(val[index]);
                continue;

            }

            if (items[index] == "NROOPERACION" || items[index] == "FECHA") {

                //sum[index] = parseFloat(sum[index]) + parseFloat(val[index]);
                continue;
            }

            
        }

    }

    for (var index = 0; index < items.length; index++) {

        debugger;
        if (items[index] != "NROOPERACION" && items[index] != "FECHA" && items[index] != "HORA" && items[index] != "BAS9005" && items[index] != "BAS09005") {

            arrCol.push({ field: items[index], aggregates: ["sum"], footerTemplate: "<div> #=kendo.toString(" + sum[index] + ",'n0')# </div>" + "<div>#=kendo.toString(" + (sum[index] * 0.001) + ", 'n1') # </div" });
            arrAgg.push({ field: items[index], aggregate: "sum" });
 
            producido = producido + parseFloat(sum[index] * 0.001);
            $("#ProduccionFinal").val(producido.toFixed(2));
        }
        if (items[index] == "BAS9005" || items[index] == "BAS09005")
        {
            arrCol.push({ field: items[index], aggregates: ["sum"], footerTemplate: "<div>&nbsp</div>" + "<div>#=kendo.toString(" + (sum[index].toFixed(0)) + ", 'n1') # </div" });
            arrAgg.push({ field: items[index], aggregate: "sum" });

            producido = producido + parseFloat(sum[index]);
            $("#ProduccionFinal").val(producido.toFixed(2));
        }

        if (items[index] == "HORA") {

            arrCol.push({ field: items[index], aggregates: ["count"], footerTemplate: "<div>Total g</div><div>Total Kg</div> " });
            arrAgg.push({ field: items[index], aggregate: "count" });
        }

        if (items[index] == "NROOPERACION" || items[index] == "FECHA") {

            arrCol.push({ field: items[index], aggregates: ["count"], footerTemplate: "" });
            arrAgg.push({ field: items[index], aggregate: "count" });
        }

    };

    var grid = $("#gridMateriaPrima").kendoGrid({
        dataSource: {
            data: gridData,
            height: 410,
            schema: {
                model: model
            },
            pageSize: 12,
            aggregate: arrAgg
        },
        pageable: true,
        columns: arrCol
    });
}

function generateModelToc01(gridData, id) {
    var model = {};
    model.id = id;
    var fields = {};

    for (var property in gridData) {
        var propType = typeof gridData[property];
        if (propType == "number") {
            fields[property] = {
                type: "number",
                validation: {
                    required: true
                },
            };
        } else if (propType == "boolean") {
            fields[property] = {
                type: "boolean",
                validation: {
                    required: true
                }
            };
        }
        else if (propType == "string") {
            var parsedDate = kendo.parseDate(gridData[property]);
           
            fields[property] = {
                validation: {
                    required: true
                }

            }
        }
        else {
            fields[property] = {
                    validation: {
                        required: true
                    }
            };
        }

    }
    
    model.fields = fields;

    return model;

}

///funcion para generar el modelo de la grid dinamica
function generateModel(gridData,id) {
        var model = {};
        model.id = id;
        var fields = {};
    
        for (var property in gridData)
        {
            var propType = typeof gridData[property];
            if (propType == "number") {
                fields[property] = {
                    type: "number",
                    validation: {
                        required: true
                    }
                };
            } else if (propType == "boolean") {
                fields[property] = {
                    type: "boolean",
                    validation: {
                        required: true
                    }
                };
            } else if (propType == "string")
            {
                var parsedDate = kendo.parseDate(gridData[property]);
                //if (parsedDate)
                //{
                //    fields[property] = {
                //        type: "date",
                //        validation: {
                //            required: true
                //        }
                //    };
                //    dateFields.push(property);
                //} else {
                fields[property] = {
                    validation: {
                        required: true
                    }
                    //};
                }
            } else {
                fields[property] = {
                    validation: {
                        required: true
                    }
                };
            }

        }
   
    model.fields = fields;

    return model;

    }

//funcion para llenar el combo de productos 
function LlenarComboProductos(combo, filter) {
        combo.kendoDropDownList({
            optionLabel: "Seleccione el Producto...",
            dataTextField: "NOMBREPRODUCTO",
            dataValueField: "CODIGOPRODUCTOS",
            dataSource: {
                transport: {
                    read: {
                        method: 'GET',
                        url: baseUrl + "PRODUCTOS",
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (options, operation) {

                        if (filter != "")
                            return JSON.stringify(filter);
                    }
                },
                schema: {
                    data: function (data) {
                        return data.value;
                    },
                    total: function (data) {
                        return data["odata.count"];
                    },
                },
            },
        });
}

//Funcion para llenar el combo de los materiales 
function LlenarCombo(combo, filter) {

    combo.kendoDropDownList({
        optionLabel: "Seleccione el Material...",
        dataTextField: "NOMBREMATERIAL",
        dataValueField: "CODIGOMATERIAL",
        dataSource: {
            transport: {
                read: {
                    method: 'POST',
                    url: baseUrl + "MATERIALESPORPRODUCTOes/ListaMaterialesProductosMezclado",
                    contentType: "application/json; charset=utf-8",
                },
                parameterMap: function (options, operation) {

                    if (filter != "")
                        return JSON.stringify(filter);
                }
            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
            },
        },
    });
}

function generateGridPersonal() {

    var filter =
    {
        LOGIN: $("#gridPersonal")
    }

    $("#gridPersonal").kendoGrid({
        dataSource: new kendo.data.DataSource({
            type: 'odata',
            transport: {
                read:
                {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "USUARIOS/GetAdminPersonal",
                    contentType: "application/json; charset=utf-8",
                },
                parameterMap: function (options, operation) {
                    return JSON.stringify(filter);
                },
            },
            schema: {
                model: {
                    fields: {
                        CodigoPersonal: { validation: { required: true }, editable: false, type: "string", nullable: false },
                        Cedula: { validation: { required: true }, editable: false, type: "string", nullable: false },
                        Nombres: { validation: { required: true }, editable: false, type: "string", nullable: false },
                        Apellidos: { validation: { required: true }, editable: false, type: "string", nullable: false },
                        Sexo: { validation: { required: true }, editable: false, type: "string", nullable: false },
                        Fecha_Nacimiento: { editable: false, type: "date", nullable: false }
                    }
                },
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    var total = data.value.length;
                    return total;
                },
            },
            pageSize: 10,
        }),
        selectable: "row",
        sortable: true,
        pageable: true,
        columns: [
            { field: "CodigoPersonal", title: "CodPersonal", width: "150px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { field: "Cedula", title: "Cedula", width: "150px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { field: "Nombres", title: "Nombres", width: "190px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { field: "Apellidos", title: "Apellidos", width: "190px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { field: "Sexo", title: "Sexo", width: "80px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { field: "Fecha_Nacimiento", title: "Nacimiento", format: '{0:dd-MMM-yyyy}',  width: "180px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { command: [{ name: "edit", text: "Editar", click: getPersonalAEditar }, { name: "delete", text: "Eliminar", click: eliminarPersonal }] }
        ],
    });
}


function generateGridAdminUsers() {

        var filter =
        {
            LOGIN: $("#gridUsuarios")
        }

    $("#gridUsuarios").kendoGrid({
            dataSource: new kendo.data.DataSource({
                type: 'odata',
                transport: {
                    read:
                    {
                        dataType: "json",
                        method: 'POST',
                        url: baseUrl + "USUARIOS/GetAdminUsuarios",
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (options, operation) {
                        return JSON.stringify(filter);
                    },
                },
                schema: {
                    model: {
                        fields: {
                            Login: { validation: { required: true }, editable: false, type: "string", nullable: false },
                            Password: { validation: { required: true }, editable: false, type: "string", nullable: false },
                            Fecha_Creacion: { editable: false, type: "date", nullable: false},
                            CodigoPersonal: { validation: { required: true }, editable: false, type: "string", nullable: false },
                            Rol: { validation: { required: true }, editable: false, type: "string", nullable: false },
                            IdRol: { editable: false, type: "int", nullable: false }
                        }
                    },
                    data: function (data) {
                        return data.value;
                    },
                    total: function (data) {
                        var total = data.value.length;
                        return total;
                    },
                },
                pageSize: 10,
            }),
            selectable: "row",
            sortable: true,
            pageable: true,
        columns: [
            { field: "Login", title: "Usuario", width: "160px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" }},
            { field: "Password", title: "Contraseña", width: "160px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" }},
            { field: "Fecha_Creacion", title: "Fecha de creacion", format: '{0:dd-MMM-yyyy}', width: "200px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" }},
            { field: "CodigoPersonal", title: "Codigo de personal", width: "220px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { field: "Rol", title: "Rol del usuario", width: "180px", attributes: { style: "text-align: center;" }, headerAttributes: { style: "text-align: center; font-size:150%" } },
            { command: [{ name: "edit", text: "Editar", click: getUsuarioAEditar }, { name: "delete", text: "Eliminar", click: eliminarUsuario }] }
        ],
    });

}

//Funciones para llenar grid de CapturaInformacion Himalaya
function LlenarGridCapturaHimalaya(filter) {



    request("application/json", null, null, "POST", baseUrl + "FORMATOTOCs/CrearReporteDinamycTOCHimalaya", function (data) {
        if (data != null) {
            generateGridCapturaHimalaya(JSON.parse(data.value))
        }
    }, errorHandler, JSON.stringify(filter));

};

function generateGridCapturaHimalaya(gridData) {



    if (gridData[0].NROOPERACION = "0") {
        $("#gridMateriaPrima").show();
        var a = 1;
        for (var i = 0; i < gridData.length; i++) {

            gridData[i].NROOPERACION = "" + a + "";
            a++;
        };

    };


    var model = generateModelCapturaHimalaya(gridData[0], "NROOPERACION");


    var items = Object.keys(gridData[0]);
    var arrCol = [];
    var arrAgg = [];
    var sum = new Array(items.length);
    var TotalProducido = 0;


    for (var i = 0; i < gridData.length; i++) {

        if (gridData[i].TOTALBATCH != "0") {

            TotalProducido = TotalProducido + parseInt(gridData[i].TOTALBATCH)
        }
    }


    for (var i = 0; i < sum.length; i++) {
        sum[i] = 0;
    }

    for (var gridIndex = 0; gridIndex < gridData.length; gridIndex++) {

        var val = Object.values(gridData[gridIndex]);

        for (var index = 0; index < items.length; index++) {


            if (items[index] == "") {

                sum[index] = parseFloat(sum[index]) + parseFloat(val[index]);

            }

        }

    }

    $("#TotalProducido").val(TotalProducido);

    document.getElementById("TotalAProducir").value = (parseFloat(document.getElementById("TotalProduccion").value) - (document.getElementById("TotalProducido").value))


    var grid = $("#gridMateriaPrima").kendoGrid({
        dataSource: {
            data: gridData,
            height: 410,
            schema: {
                model: model
            },
            pageSize: 12,
        },
        pageable: true,
    });
}

function generateModelCapturaHimalaya(gridData, id) {

    var model = {};
    model.id = id;
    var fields = {};

    for (var property in gridData) {
        var propType = typeof gridData[property];
        if (propType == "number") {
            fields[property] = {
                type: "number",
                validation: {
                    required: true
                },
            };
        } else if (propType == "boolean") {
            fields[property] = {
                type: "boolean",
                validation: {
                    required: true
                }
            };
        }
        else if (propType == "string") {
            var parsedDate = kendo.parseDate(gridData[property]);

            fields[property] = {
                validation: {
                    required: true
                }

            };
        }
        else {
            fields[property] = {
                validation: {
                    required: true
                }
            };
        }

    }

    model.fields = fields;

    return model;

}

//Llena la grid de la lista 
function llenargridMateriaPrimaHimalaya() {


    var filter =
    {
        CODIGOPRODUCTO: $("#CodProducto").val(),
        CODIGOORDENPRODUCCION: $("#OrdenProduccion").val(),
        NROOPERACION: $("#NumeroBatch").val()

    }
    var TotalPesado = 0;

    $("#gridMateriaPrimaOperacion").kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    method: 'POST',
                    url: baseUrl + "FORMATOTOCs/ConsultarDatosOperacionHimalaya",
                    contentType: "application/json; charset=utf-8",

                },

                parameterMap: function (options, operation) {
                    return JSON.stringify(filter);
                }
            },

            schema: {
                model: {
                    fields: {
                        NROOPERACION: { type: "string" },
                        CODIGOMATERIAL: { type: "string" },
                        NOMBREMATERIAL: { type: "string" },
                        NOMBREPRODUCTO: { type: "string" },
                    }
                },
                data: function (data) {

                    for (var i = 0; i < data.value.length; i++) {

                        TotalPesado = TotalPesado + data.value[i].Cantidad
                    }
                    $("#TotalPesadoBatch").val(TotalPesado)
                    return data.value;
                },
                total: function (data) {
                    var total = data['odata.count'];
                    return total;
                },
            },

        }),
        height: 450,
        selectable: "row",
        pageable: false,
        scrollable: true,
        sortable: true,
        columns: [
            { title: "Nro Operacion", field: "NroOperacion", width: "10%" },
            //{ title: "Cod Material", field: "CodigoMaterial" },
            {
                title: "Material", field: "Material"
            },
            { title: "Cantidad", field: "Cantidad", width: "15%" }

        ],



    });

}
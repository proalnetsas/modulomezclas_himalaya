﻿//calls the keep alive handler every 300,000 miliseconds, which is 5 minutes
var keepAlive = {
    refresh: function () {
        $.get('/keep-alive.ashx');
        setTimeout(keepAlive.refresh, 300000);
    }
}; $(document).ready(keepAlive.refresh());
﻿using SipModuloManual.Controllers;
using SipModuloManualModelo.General;
using SipModuloManualModelo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SipModuloManual.Models.Security
{
    public class UsersSecurity
    {
        public static string UserLogin { get; set; }

        public UsersSecurity()
        {
            
        }

        public void ValidacionLogin(String user, String pass, ref Boolean Result)
        {
            ContextEntities db = new ContextEntities();
            try
            {
                USUARIOS u = db.USUARIOS.Where(x => x.LOGIN == user).FirstOrDefault();
               
                if (u != null)
                {
                    if (u.PASSWORD == pass)
                    {
                        if (u.ESTADO != 1)
                            throw new Exception("El usuario se encuentra inactivo, por favor contacte al administrador del sistema.");

                        if (Generales.SesionTrabajo == null)
                            Generales.SesionTrabajo = new SesionDeTrabajoActual();



                        Generales.SesionTrabajo.FechaInicioSesion = DateTime.Now;
                        Generales.SesionTrabajo.NombreUsuario = u.PERSONAL.NOMBRES + ' ' + u.PERSONAL.APELLIDOS;
                        Generales.SesionTrabajo.CodigoUsuario = u.LOGIN;
                        UserLogin = u.LOGIN;
                        Generales.UltimoMovimientoSistema = DateTime.Now;
                        List<USUARIOSXROL> roles = db.USUARIOSXROL.Where(x => x.LOGIN == u.LOGIN && x.ROLES.ESTADO == true)
                            .OrderBy(r => r.ROLES.ROL).ToList();

                        if (roles != null && roles.Count > 0)
                        {
                            Generales.SesionTrabajo.Roles = roles.Select(x => x.IDROL).ToList();
                            if (roles.Count == 1)
                            {
                                Generales.SesionTrabajo.RolActual = roles.Select(x => x.IDROL).FirstOrDefault();

                                Result = true;
                            }
                        }
                        else
                        {
                            throw new Exception("El usuario no tiene perfiles asignados, por favor solicite al administrador del sistema los perfiles para poder ingresar");
                        }
                    }
               }
                else
                    throw new Exception("Usuario y/o contraseña erroneas, por favor intentelo nuevamente.");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                db.Dispose();
            }
        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.General
{
    public class SesionDeTrabajoActual
    {
        public String NombreUsuario { get; set; }
        public String CodigoUsuario { get; set; }
        public List<int> Roles { get; set; }
        public int RolActual { get; set; }
        public Double NumeroConexion { get; set; }
        public DateTime FechaInicioSesion { get; set; }
        public Object OpcionesPorUsuario { get; set; }
    }

    public class PermisosPorUsuario
    {
        public int CodigoOpcion { get; set; }
        public String Url { get; set; }
        public Boolean Estado { get; set; }
    }
}

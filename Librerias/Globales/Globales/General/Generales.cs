﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SipModuloManualModelo.General
{
    public class Generales
    {
        public SesionDeTrabajoActual SesionTrabajoActual { get; set; }
        public static SesionDeTrabajoActual SesionTrabajo
        {
            get
            {
                return (SesionDeTrabajoActual)HttpContext.Current.Session["SesionTrabajo"];
            }
            set
            {
                HttpContext.Current.Session["SesionTrabajo"] = value;
            }
        }

        public static Boolean SesionBloqueada
        {
            get
            {
                return Convert.ToBoolean(HttpContext.Current.Cache["SesionBloqueada"]);
            }
            set
            {
                HttpContext.Current.Cache["SesionBloqueada"] = value;
            }
        }

        public static DateTime UltimoMovimientoSistema
        {
            get
            {
                return Convert.ToDateTime(HttpContext.Current.Cache["UltimoMovimientoSistema"]);
            }
            set
            {
                HttpContext.Current.Cache["UltimoMovimientoSistema"] = value;
            }
        }

        public static void RedireccionarUrl(String Controlador, String Accion)
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            if (appUrl != "/") appUrl += "/";

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            HttpContext.Current.Response.RedirectPermanent(baseUrl + Controlador + "/" + Accion);
        }

        public static string RetornaTurno()
        {
            string CodigoTurno = String.Empty;

            if (DateTime.Now.Hour >= 14 && (DateTime.Now.Hour <= 21 && DateTime.Now.Minute <= 59))
                CodigoTurno = "2";
            else if (DateTime.Now.Hour >= 6 && (DateTime.Now.Hour <= 13 && DateTime.Now.Minute <= 59))
                CodigoTurno = "1";
            else
                CodigoTurno = "3";


            return CodigoTurno;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globales
{
    public class SipSada
    {
        SqlConnection conectar; //objetos para conectar a la BD
        private SqlDataAdapter adaptador; /*adaptacion con una base de Datos*/
        private DataTable tabla; /* Representa una tabla de datos en memoria*/

        public SipSada()
        {

        }

        public Dictionary<string, double> ConsultarVariables()
        {
            Dictionary<string, double> Temperaturas = new Dictionary<string, double>();
            DataTable resulta = llena("SELECT TOP 2  VA.NombreVariable,V.VALOR FROM VALOR V INNER JOIN VARIABLE VA ON V.IdVariable=VA.IdVariable ORDER BY V.FechayHora DESC");

            if (resulta != null)
            { 
                foreach (DataRow row in resulta.Rows)
                {
                    Temperaturas.Add(row[0].ToString(), Convert.ToDouble(row[1].ToString()));
                }
            }

            return Temperaturas;
        }


        public void iniciarconexion()
        {
            try
            {
                conectar = new SqlConnection();//asignamos a la variable sqlconexion el objeto de conexión
                conectar.ConnectionString = "Data Source=192.168.10.13,1475;Initial Catalog=SIPSADA;User ID=sa;PASSWORD=DK1dk1DK1";//en ConnectionString le ponemos la ruta de la base de datos
                conectar.Open();//abrimos la conexion 

            }
            catch(Exception Ex) //manejo de errores en tiempo de ejecucion
            {

            }

        }

        public void CerrarConexion() {
            conectar.Close();

        }

        public DataTable llena(String str) //solo recibe un select
        {
            iniciarconexion();
            try
            {
                adaptador = new SqlDataAdapter(str, conectar);
                tabla = new DataTable();
                adaptador.Fill(tabla); //llena datatable
                return tabla; //si hay error retorna nulo nada
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                CerrarConexion();
            }
        }


    }
}

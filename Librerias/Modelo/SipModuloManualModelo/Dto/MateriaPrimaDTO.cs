﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class MateriaPrimaDTO
    {
        public int NroOperacion { get; set; }
        public string CodigoMaterial { get; set; }
        public string Material { get; set; }
        public double Cantidad { get; set; }
        public double? CantidadAPesar { get; set; }
    }
}

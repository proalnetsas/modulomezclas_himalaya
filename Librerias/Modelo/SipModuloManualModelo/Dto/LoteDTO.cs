﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class LoteDTO
    {
        public string Lote { get; set; }
        public float Cantidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class ProgramacionDTO
    {
        public string CodigoOrdenProduccion { get; set; }
        public string CodigoProducto { get; set; }
        public string NombreProducto { get; set; }
        public double PesoProgramado { get; set; }
        public double PorcentajeAvance { get; set; }
        public DateTime? FechaInicioProducto { get; set; }
    }
}

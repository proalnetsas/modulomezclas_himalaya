﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class NuevoBatchDTO
    { 
        public string CodigoMaterial { get; set; }
        public string NombreMaterial { get; set; }
        public double? CantidadAPesar { get; set; }
        public double CantidadPesada { get; set; }
    }
}

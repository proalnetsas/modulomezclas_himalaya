﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class PersonalDTO
    {
        public string CodigoPersonal { set; get; }
        public string NombrePersonal { set; get; }
    }
}

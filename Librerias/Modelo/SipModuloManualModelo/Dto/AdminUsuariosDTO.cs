﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class AdminUsuariosDTO
    {
        public string Login { get; set; }
        public string Password{ get; set; }
        public DateTime? Fecha_Creacion { get; set; }
        public string CodigoPersonal { get; set; }
        public int IdRol { get; set; }
        public string Rol { get; set; }

    }
}

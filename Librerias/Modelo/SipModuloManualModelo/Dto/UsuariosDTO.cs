﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class UsuariosDTO
    {
        public string Nombre { get; set; }
        public string CodigoUsuario { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class MaterialesDTO
    {
        public string CODIGOMATERIAL { get; set; }
        public string NOMBREMATERIAL { get; set; }

        public string NOMBREPRODUCTO { get; set; }
        

        public int IMPRESION { get; set; }

        public bool? APROBADO { get; set; }

        public bool? GUARDADO { get; set; }

        public bool? PROCESADO { get; set; }

        public bool? DEVUELTO { get; set; }

     }
}

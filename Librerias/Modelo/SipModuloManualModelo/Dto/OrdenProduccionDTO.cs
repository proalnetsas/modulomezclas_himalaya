﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class OrdenProduccionDTO
    {
        public string CODIGOORDENPRODUCCION { get; set; }
        public string CODIGOPRODUCTOS { get; set; }
        public string NOMBREPRODUCTO { get; set; }
        public double? CANTIDAD { get; set; }
        public double? KILOGRAMOSODP { get; set; }
        public string LOTE { get; set; }
        public string LOTEPRODUCTO { get; set; }
    }
}

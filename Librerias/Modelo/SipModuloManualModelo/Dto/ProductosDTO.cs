﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class ProductosDTO
    {
        public string CODIGOPRODUCTOS { get; set; }
        public string NOMBREPRODUCTO { get; set; }
    }
}

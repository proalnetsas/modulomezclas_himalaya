﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SipModuloManualModelo.Dto
{
    public class RolesDTO
    {
        public int IdRol { get; set; }
        public string Rol { set; get; }
    }
}

﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SipModuloManualModelo.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ContextEntities : DbContext
    {
        public ContextEntities()
            : base("name=ContextEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<MATERIALES> MATERIALES { get; set; }
        public virtual DbSet<MATERIALESPORPRODUCTO> MATERIALESPORPRODUCTO { get; set; }
        public virtual DbSet<ORDENES_DE_PRODUCCION> ORDENES_DE_PRODUCCION { get; set; }
        public virtual DbSet<ORDENPRODUCCIONXPRODUCTO> ORDENPRODUCCIONXPRODUCTO { get; set; }
        public virtual DbSet<PARTESXPRODUCTOXPROCESO> PARTESXPRODUCTOXPROCESO { get; set; }
        public virtual DbSet<PROCESO> PROCESO { get; set; }
        public virtual DbSet<PRODUCTOPORPROCESO> PRODUCTOPORPROCESO { get; set; }
        public virtual DbSet<PRODUCTOS> PRODUCTOS { get; set; }
        public virtual DbSet<PUESTOTRABAJOSEGUNPRODUCTO> PUESTOTRABAJOSEGUNPRODUCTO { get; set; }
        public virtual DbSet<ROLES> ROLES { get; set; }
        public virtual DbSet<USUARIOSXROL> USUARIOSXROL { get; set; }
        public virtual DbSet<USUARIOS> USUARIOS { get; set; }
        public virtual DbSet<PERSONAL> PERSONAL { get; set; }
        public virtual DbSet<FORMATOTOC> FORMATOTOC { get; set; }
        public virtual DbSet<INT_SAP_CARGUE> INT_SAP_CARGUE { get; set; }
        public virtual DbSet<INT_SAP_SUBIDA> INT_SAP_SUBIDA { get; set; }
        public virtual DbSet<PARAMETROS> PARAMETROS { get; set; }
    }
}

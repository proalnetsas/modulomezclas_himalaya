﻿using GeneradorReportes;
using SipModuloManualModelo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace GeneradorService
{
    public partial class GeneradorService : ServiceBase
    {
        public Timer aTimer;
        
        object updateLock = new object();
        private ContextEntities db = new ContextEntities();
        private CrearArchivo crea = new CrearArchivo();

        public GeneradorService()
        {
            InitializeComponent();
            eventLog1.Source = "GenerarExcelService";
            eventLog1.Log = "Application";
            if (!System.Diagnostics.EventLog.SourceExists("GenerarExcelService"))
            {
                System.Diagnostics.EventLog.CreateEventSource("GenerarExcelService", "");
            }
        }

        protected override void OnStart(string[] args)
        {
            System.Threading.TimerCallback sincronizacionCallback = new System.Threading.TimerCallback(OnTimedEvent);
            aTimer = new System.Threading.Timer(sincronizacionCallback, null, 30000, 1000);
            eventLog1.Source = "GenerarExcelService";
        }

        private void OnTimedEvent(object objeto)
        {
            try
            {

                if (Monitor.TryEnter(updateLock))
                {
                    try
                    {
                        generarReportes();
                    }
                    finally
                    {
                        Monitor.Exit(updateLock);
                    }
                }
                else
                {
                    // previous timer tick took too long.
                    // so do nothing this time through.
                }

                
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                EventLog.WriteEntry("Error de captura: " + error, EventLogEntryType.Error);
            }
            finally
            {
                
            }
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("GenerarExcelService detenido!!");
        }


        void generarReportes()
        {
            List<GENERADORREPORTES> gene = db.GENERADORREPORTES.Where(c => c.GENERADO == false).ToList();

            foreach (GENERADORREPORTES gen in gene)
            {
                if (gen.REPORTE == "PPR027")
                {
                    crea.GenerarPPR027(gen.RUTA);
                    gen.GENERADO = true;
                    db.SaveChanges();
                }

                if (gen.REPORTE == "TOCO1")
                {
                    crea.GenerarToc01(gen.RUTA, gen.PARAMETRO);
                    gen.GENERADO = true;
                    db.SaveChanges();
                }

                if (gen.REPORTE == "TOC02")
                {
                    crea.GenerarToc02(gen.RUTA, Convert.ToInt32(gen.PARAMETRO));
                    gen.GENERADO = true;
                    db.SaveChanges();
                }

                if (gen.REPORTE == "PPR019")
                {
                    crea.GenerarPpr019(gen.RUTA, Convert.ToInt32(gen.PARAMETRO));
                    gen.GENERADO = true;
                    db.SaveChanges();
                }

                if (gen.REPORTE == "PPR01")
                {
                    crea.GenerarPPr01(gen.RUTA, Convert.ToInt32(gen.PARAMETRO));
                    gen.GENERADO = true;
                    db.SaveChanges();
                }


            }
        }
    }
}

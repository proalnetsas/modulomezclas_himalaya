﻿using InterfazHimalaya.ServiceReferenceHimalaya;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazHimalaya
{
    public class Comunication
    {
        ServiceReferenceHimalaya.WSSincronizacion cliente;
        public Comunication()
        {
        }

        public List<ResultRead> StartRead(string CodItem, string CodBodega) {
            try
            {
                List<ResultRead> read = new List<ResultRead>();
                string XMlSend = CreateDataXML(CodItem, CodBodega);
                if (XMlSend != string.Empty)
                {
                    read = SendXML(XMlSend);
                    return read;
                }
                else
                    return read;
                
            }
            catch (Exception ex) {
                throw new Exception("Error Validando Datos en SAP " + ex.Message);
            }
        
        }


        public string CreateDataXML(string CodItem, string Codbodega)
        {
            string XMLTransport = string.Empty;
            try
            {

                XMLTransport = "<?xml version=\"1.0\" standalone =\"yes\" ?>";
                XMLTransport += "<ObjetosIntegracion BaseDatos=\"HBT_AGR_HIMA\" > ";
                XMLTransport += "<SAPBO>";
                XMLTransport += "<SAP Objeto=\"StockBodegaxItem\" Operacion =\"Read\" > ";
                XMLTransport += "  <Propiedad_Encabezado> ";
                XMLTransport += "<NombreColumna>lcbodega</NombreColumna>";
                XMLTransport += "<ValorColumna>" + Codbodega + "</ValorColumna>";
                XMLTransport += "</Propiedad_Encabezado>";
                XMLTransport += " <Propiedad_Encabezado> ";
                XMLTransport += "<NombreColumna>lcitem</NombreColumna>";
                XMLTransport += "<ValorColumna>" + CodItem + "</ValorColumna>";
                XMLTransport += "</Propiedad_Encabezado>";
                XMLTransport += "</SAP>";
                XMLTransport += "</SAPBO>";
                XMLTransport += "</ObjetosIntegracion>";
                return XMLTransport;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Creando Xml para Validar Datos en SAP " + ex.Message);
            }
        }


        public List<ResultRead> SendXML(string xml)
        {
            String JsonResulta;
            List<ResultRead> result = new List<ResultRead>();
            try
            {
                cliente = new ServiceReferenceHimalaya.WSSincronizacionClient();
                JsonResulta = cliente.ConsultarDatosSAP(xml);
                result = JsonConvert.DeserializeObject<List<ResultRead>>(JsonResulta);

                return result;
            }
            catch (Exception exception)
            {
                throw new Exception(string.Concat("Error Conectandose a WEbService Himalaya ", exception.Message));
            }

        }
    }
}

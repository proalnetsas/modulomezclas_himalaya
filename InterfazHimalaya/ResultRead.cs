﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazHimalaya
{
    public class ResultRead
    {
        public string CodItem { get; set; }
        public string Lote { get; set; }
        public float Qt { get; set; }
        public string Almacen { get; set; }
    }
}
